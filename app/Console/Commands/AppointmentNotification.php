<?php

namespace App\Console\Commands;

use App\Models\AppointmentSchedule;
use App\Models\EmergencyContact;
use App\Models\Notification;
use App\OutSourceRequest;
use DateTime;
use Illuminate\Console\Command;

class AppointmentNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sent Notifcation To Realtor the notification will start in 15 minutes interval within 1 hour ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {


        $appointmentSchedule = AppointmentSchedule::all();

        if (!empty($appointmentSchedule) && count($appointmentSchedule) > 0) {
            foreach ($appointmentSchedule as $notification) {
                $time2 = new DateTime('' . $notification->start_date . '' . $notification->start_time . '');
                $time1 = new DateTime();
                $timediff = $time1->diff($time2);

                echo $timediff->m . "</n>";

                if ($timediff->m >= 0 && $timediff->m <= 15) {

                    $this->emergencyContacts($notification->id);

                    //Realtor Notification
                    $this->notification('15', $notification->id, $notification->realtor_id);

                    // Send SMS as Well
                    // $this->sendSms($notification->realtor->user->phone_number, 'hello');


                } elseif ($timediff->m > 15 && $timediff->m <= 30) {

                    //Realtor Notification
                    $this->notification('30', $notification->id, $notification->realtor_id);

                    // Send SMS as Well
                    // $this->sendSms($notification->realtor->user->phone_number, 'hello');

                } elseif ($timediff->m > 30 && $timediff->m <= 45) {

                    //Realtor Notification
                    $this->notification('45', $notification->id, $notification->realtor_id);

                    // Send SMS as Well
                    // $this->sendSms($notification->realtor->user->phone_number, 'hello');

                } elseif ($timediff->m > 45 && $timediff->m <= 60) {

                    //Realtor Notification
                    $this->notification('60', $notification->id, $notification->realtor_id);

                    // Send SMS as Well
                    // $this->sendSms($notification->realtor->user->phone_number, 'hello');
                }
            }
        }


    }

    public function emergencyContacts($appointmentId)
    {
        $emergencyContacts = EmergencyContact::where('realtor_id', $appointmentId)->get();
        if (!empty($emergencyContacts) && count($emergencyContacts) > 0) {
            //Send Sms to EmergencyContacts
            foreach ($emergencyContacts as $contact) {
            // $this->sendSms($contact->mobile_number, 'hello');
            }
        }
    }

    public function notification($minutes, $appointmentId, $realtorId)
    {
        $notification = new Notification();
        $notification->type = 'appointment';
        $notification->message = 'Your Appointment will Start within ' . $minutes . ' minutes.Your Appointment Id :: ' . $appointmentId . '';
        $notification->sms_sent = true;
        $notification->type_id = $realtorId;
        $notification->save();
    }
}
