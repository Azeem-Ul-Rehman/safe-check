<?php

namespace App\Http\Controllers\Api;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Resources\UserResource;

use App\Mail\OTPCodeEmail;
use App\Models\Setting;
use App\Models\Subscription;
use App\PasswordReset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\UserRole;
use App\Mail\ForgotPasswordRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    public $successStatus = 200;

    public function otp(Request $request)
    {
        $details = ['email' => $request->email];

        $validator = Validator::make($details, [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $otp_code = null;
            $user = User::where('email', $request->email)->first();

            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
            } else {
                $otp_code = $user->otp_code;
            }

            $user->update([
                'otp_code' => $otp_code,
            ]);


            $data = [
                'email' => $user->email,
                'code' => $otp_code,
            ];
            $this->verificationEmail($user,$otp_code);
            return response()->json(['message' => 'OTP code has been sent on given number.', 'data' => $data], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }
    public function verificationEmail($user,$otp_code)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your OTP Code: '.$otp_code,
            'from' => config('app.admin'),
            'subject'=>'Reset Password'

        ];
        Mail::to($user->email)->send(new OTPCodeEmail($details));
    }
    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {


            $user = User::where([
                'email' => $request->get('email'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {

                $user->otp_status = 'verified';
                $user->save();

                return response()->json(['status' => true, 'message' => 'OTP Code verified successfully.'], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'Provided OTP code is not correct.'], 200);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }
    public function forgotPasswordRequest(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = User::where('email', $request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(["status" => 'error', "message" => 'Your account is not associated with this email address.'], 404);
            }

            $user->password = bcrypt($request->password);
            $user->save();

            return response()->json(['status' => 'success', "message" => 'Your password has been changes successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'password' => 'required'
        ], [
            'email.required' => 'Email is required',
            'password.required' => 'Password is required',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {


            $user = Auth::user();
//            if (!$user->activated) {
//                auth()->logout();
//                return response()->json(['message' => 'You need to confirm your account. We have sent you an activation email.'], 401);
//            } else {

            if ($user->status == 'suspended') {
                auth()->logout();
                return response()->json(['message' => 'Your account is suspended.'], 401);
            }
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            $success['token'] = $user->createToken('AppName')->accessToken;
            $success['data'] = new UserResource($user);
            $success['settings'] = Setting::get();
            $success['remaining_check'] = !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0;
            return response()->json(['success' => $success], $this->successStatus);
//            }
        } else {
            return response()->json(['message' => 'Invalid Password'], 401);
        }
    }
    public function register(Request $request)
    {
        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);


        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            'phone_number' => 'required|unique:users,phone_number',
            'home_number' => 'required|unique:users,emergency_number',
            'pin_number' => 'required',
            'terms-and-conditions' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name field  is required.',
            'last_name.required' => 'Last name field is required.',
            'username.required' => 'Username field is required.',
            'phone_number.required' => 'Phone Number field  is required.',
            'home_number.required' => 'Home Number field  is required.',
            'pin_number.required' => 'Pin Number field  is required.',
            'email.required' => 'Email field  is required.',
            'terms-and-conditions.required' => 'Please Accept.',

        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            if ($request->has('image')) {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $profile_image = $name;
            } else {
                $profile_image = 'default.png';
            }
            $role = Role::find(2);

            $user = User::create([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'username' => $request->get('username'),
                'role_id' => $role->id,
                'phone_number' => $request->get('phone_number'),
                'emergency_number' => $request->get('home_number'),
                'user_type' => $role->name,
                'status' => 'verified',
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'profile_pic' => $profile_image,
            ]);

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);


            $realtor = new Realtor();
            $realtor->user_id = $user->id;
            $realtor->pin_number = $request->get('pin_number');
            $realtor->save();


            $packages = Package::all();
            $textPage = 'register';
            Auth::login($user);
            $messageShow = false;

            $details = [
                'greeting' => 'Hi',
                'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
                'body' => 'Thank you for becoming a member of SafeChex! Someone from our team will  contact you shortly to complete your registration process.',
                'thanks' => 'Thank you for using SafeChex ',
                'from' => 'regmedapp@gmail.com'

            ];


            //Realtor Notification
            $notification = new Notification();
            $notification->type = 'realtor';
            $notification->message = 'Thank you for becoming a member of SafeChex! Someone from our team will  contact you shortly to complete your registration process.';
            $notification->email_sent = true;
            $notification->type_id = $realtor->id;
            $notification->save();


            Mail::to($request->email)->send(new EventEmail($details));

            Auth::login($user);
            $success['token'] = $user->createToken('AppName')->accessToken;
            $success['data'] = new UserResource($user);
            $success['settings'] = Setting::get();
            $success['packages'] = $packages;
            $success['packages'] = $packages;
            $success['textPage'] = $textPage;
            $success['messageShow'] = $messageShow;

            return response()->json(['success' => $success], $this->successStatus);

        } else {
            return response()->json(['message' => 'Something Went Wrong'], 401);
        }





    }
    public function logout()
    {
        try {
            $user = Auth::user();

            foreach ($user->tokens as $token) {
                $token->revoke();
                $token->delete();
            }

            return response()->json(['status' => true], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

        return response()->json(null, 204);
    }
    public function getUser()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


}
