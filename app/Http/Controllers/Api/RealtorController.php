<?php

namespace App\Http\Controllers\Api;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Controller;
use App\Http\Resources\AppointmentResoruce;
use App\Http\Resources\UserResource;
use App\Models\Appointment;
use App\Models\AppointmentSchedule;
use App\Models\BackgroundCheck;
use App\Models\City;
use App\Models\EmergencyContact;
use App\Models\Notification;
use App\Models\Package;
use App\Models\ShortLink;
use App\Models\State;
use App\Models\Subscription;
use App\Models\SubscriptionHistory;
use App\Traits\GeneralHelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mockery\Matcher\Not;

class RealtorController extends Controller
{
    use GeneralHelperTrait;

    public function createAppointment(Request $request)
    {


        $user = Auth::user();


        $validator = Validator::make($request->all(), [
//            'cl_id' => 'required',
            'cl_first_name' => 'required|string',
            'cl_last_name' => 'required|string',
//            'cl_doc_id_number' => 'required',
//            'cl_doc_id_state' => 'required',
//            'cl_email' => 'required|email',
////            'cl_mobile_number' => 'required|numeric|digits_between:11,12',
//            'cl_mobile_number' => 'required|numeric',
            'property_address_one' => 'required',
            'property_address_two' => 'nullable',
            'property_city' => 'required',
            'property_state' => 'required',
            'property_zip_code' => 'required|numeric',
            'pin_number' => 'required',
            'start_date' => 'required',
//            'cl_dob' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'emergency_contacts' => 'required',
//            'image' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {


            $timeZone = $this->timeZoneSet($request->timeZoneOffset);
            $emergenciesContact = json_decode($request->emergency_contacts, true);
            $appointment = new Appointment();

            $appointment->cl_id = rand();
            $appointment->cl_first_name = $request->cl_first_name;
            $appointment->cl_last_name = $request->cl_last_name;
//            $appointment->cl_doc_id_number = $request->cl_doc_id_number;
//            $appointment->cl_doc_id_state = $request->cl_doc_id_state;
//            $appointment->cl_dob = $request->cl_dob;
//            $appointment->cl_email = $request->cl_email;
//            $appointment->cl_mobile_number = $request->cl_mobile_number;
            $appointment->property_address_one = $request->property_address_one;
            $appointment->property_address_two = $request->property_address_two;
            $appointment->property_city = $request->property_city;
            $appointment->property_state = $request->property_state;
            $appointment->property_zip_code = $request->property_zip_code;

//            if (isset($request->image) && !is_null($request->image) && $request->hasFile('image')) {
//                $image = $request->file('image');
//                $name = $image->getClientOriginalName();
//                $destinationPath = public_path('/uploads/appointmentImage');
//                $imagePath = $destinationPath . "/" . $name;
//                $image->move($destinationPath, $name);
//                $profile_image = $name;
//            } else {
//                $profile_image = 'default.png';
//            }
//            $appointment->image = $profile_image;
            $appointment->save();


            $appointmentSchedule = new AppointmentSchedule();
            $appointmentSchedule->realtor_id = $user->realtor->id;
            $appointmentSchedule->appointment_id = $appointment->id;
            $appointmentSchedule->pin_number = $request->pin_number;
            $appointmentSchedule->start_date = date('Y-m-d', strtotime($request->start_date));
            $appointmentSchedule->start_time = date("H:i", strtotime($request->start_time));
            $appointmentSchedule->end_time = date("H:i", strtotime($request->end_time));
            $appointmentSchedule->time_zone = $timeZone;
            $appointmentSchedule->local_police_number = $request->local_police_number;
            $appointmentSchedule->background_check = $request->background_check == 1 ? true : false;
            $appointmentSchedule->save();

            foreach ($emergenciesContact as $contact) {


                EmergencyContact::create([
                    'realtor_id' => $appointmentSchedule->id,
                    'name' => $contact['name'],
                    'mobile_number' => $contact['mobile_number'],
                ]);
                // Send SMS to Emergency Contact
                // $this->sendSms($contact['mobile_number'], 'hello');
            }


            // Send SMS to Appointment Person for the user
            // $this->sendSms($request->cl_mobile_number, 'hello');


            //Realtor Notification
            $notification = new Notification();
            $notification->type = 'appointment';
            $notification->data_id = $appointmentSchedule->id;
            $notification->message = 'Your Appointment has been created Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '';
            $notification->sms_sent = true;
            $notification->type_id = \auth()->user()->realtor->id;
            $notification->save();
            // Send SMS as Well
            // $this->sendSms(\auth()->user()->phone_number, 'hello');


//            if ($request->background_check == 1) {
//                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
//                if (!is_null($checkRecordBg)) {
//                    $backgroundCheck = $checkRecordBg;
//                } else {
//                    $backgroundCheck = new BackgroundCheck();
//                }
//
//                $backgroundCheck->appointment_schedule_id = $appointmentSchedule->id;
//                $backgroundCheck->first_name = $request->final_firstname;
//                $backgroundCheck->last_name = $request->final_lastname;
//                $backgroundCheck->ssn = $request->final_ssn;
//                $backgroundCheck->dob = $request->final_dob;
//                $backgroundCheck->city = $request->final_city;
//                $backgroundCheck->state = $request->final_state;
//                $backgroundCheck->save();
//            }
            // $this->sendSms($user->phone_number, 'hello');

//            $code = $this->createShortUrlLink($user->id, $appointmentSchedule->id);
//            $message = 'Please click on provided link to complete your appointment process https://safechex.com/code/' . $code;
//            $response = $this->sendSms("+17" . substr($user->phone_number, 1), $message);

            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            return response()->json([
                'status' => true,
                'message' => 'Appointment added successfully.',
                'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $appointmentSchedule->id)->first()),
//                'data' => AppointmentResoruce::collection(AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->get()),
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getAppointments()
    {

        try {

            $user = Auth::user();
            $date = today()->format('Y-m-d');
            $appointmentsScheduleUpcomming = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->whereStatus('upcoming')->whereNull('actual_start_time')->get();
            $appointmentsScheduleCurrent = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->whereStatus('current')->whereNotNull('actual_start_time')->whereNull('actual_end_time')->get();

            $currentAppointments = AppointmentResoruce::collection($appointmentsScheduleCurrent);
            $upcommingAppointments = AppointmentResoruce::collection($appointmentsScheduleUpcomming);
            $subscription = Subscription::where('realtor_id', $user->realtor->id)->with('package')->first();

            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();


            return response()->json([
                'status' => true,
                'message' => 'Appointment get successfully.',
                'current' => $currentAppointments,
                'upcomming' => $upcommingAppointments,
                'subscription' => $subscription,
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function getAppointmentDetail($appointment_schedule_id)
    {

        try {

            $user = Auth::user();
            $appointment = new AppointmentResoruce(AppointmentSchedule::where('id', $appointment_schedule_id)->first());
            $subscription = Subscription::where('realtor_id', $user->realtor->id)->with('package')->first();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();


            return response()->json([
                'status' => true,
                'message' => 'Appointment get successfully.',
                'appointment' => $appointment,
                'subscription' => $subscription,
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateAppointment(Request $request)
    {


        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
//            'cl_id' => 'required',
            'cl_first_name' => 'required|string',
            'cl_last_name' => 'required|string',
//            'cl_doc_id_number' => 'required',
//            'cl_doc_id_state' => 'required',
//            'cl_email' => 'required|email',
//            'cl_dob' => 'required|email',
////            'cl_mobile_number' => 'required|numeric|digits_between:11,12',
//            'cl_mobile_number' => 'required|numeric',
            'property_address_one' => 'required',
            'property_address_two' => 'nullable',
            'property_city' => 'required',
            'property_state' => 'required',
            'property_zip_code' => 'required|numeric',
            'pin_number' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'emergency_contacts' => 'required',
//            'image' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $timeZone = $this->timeZoneSet($request->timeZoneOffset);
            $emergenciesContact = json_decode($request->emergency_contacts, true);

            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $appointmentSchedule->pin_number = $request->pin_number;
            $appointmentSchedule->start_date = date('Y-m-d', strtotime($request->start_date));
            $appointmentSchedule->start_time = date("H:i", strtotime($request->start_time));
            $appointmentSchedule->end_time = date("H:i", strtotime($request->end_time));
            $appointmentSchedule->local_police_number = $request->local_police_number;
            $appointmentSchedule->time_zone = $timeZone;
            $appointmentSchedule->background_check = $request->background_check == 1 ? true : false;
            $appointmentSchedule->save();

            $appointment = Appointment::where('id', $appointmentSchedule->appointment_id)->first();
//            $appointment->cl_id = $request->cl_id;
            $appointment->cl_first_name = $request->cl_first_name;
            $appointment->cl_last_name = $request->cl_last_name;
//            $appointment->cl_doc_id_number = $request->cl_doc_id_number;
//            $appointment->cl_dob = $request->cl_dob;
//            $appointment->cl_doc_id_state = $request->cl_doc_id_state;
//            $appointment->cl_email = $request->cl_email;
//            $appointment->cl_mobile_number = $request->cl_mobile_number;
            $appointment->property_address_one = $request->property_address_one;
            $appointment->property_address_two = $request->property_address_two;
            $appointment->property_city = $request->property_city;
            $appointment->property_state = $request->property_state;
            $appointment->property_zip_code = $request->property_zip_code;

            $appointment->save();


            $appointmentSchedule->emergencyContacts->delete();
            foreach ($emergenciesContact as $contact) {
                EmergencyContact::create([
                    'realtor_id' => $appointmentSchedule->id,
                    'name' => $contact['name'],
                    'mobile_number' => $contact['mobile_number'],

                ]);
                // Send SMS to Emergency Contact
                // $this->sendSms($contact['mobile_number'], 'hello');
            }


            // Send SMS to Appointment Person for the user
            // $this->sendSms($request->cl_mobile_number, 'hello');


            //Realtor Notification
            $notification = new Notification();
            $notification->type = 'appointment';
            $notification->data_id = $appointmentSchedule->id;
            $notification->message = 'Your Appointment has been updated Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '';
            $notification->sms_sent = true;
            $notification->type_id = \auth()->user()->realtor->id;
            $notification->save();

            // Send SMS as Well
            // $this->sendSms(\auth()->user()->phone_number, 'hello');

//            if ($request->background_check == "1") {
//
//                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
//                if (!is_null($checkRecordBg)) {
//                    $backgroundCheck = $checkRecordBg;
//                } else {
//                    $backgroundCheck = new BackgroundCheck();
//                }
//                $backgroundCheck->first_name = $request->final_firstname;
//                $backgroundCheck->last_name = $request->final_lastname;
//                $backgroundCheck->ssn = $request->final_ssn;
//                $backgroundCheck->dob = $request->final_dob;
//                $backgroundCheck->city = $request->final_city;
//                $backgroundCheck->state = $request->final_state;
//                $backgroundCheck->save();
//            }


            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            return response()->json([
                'status' => true,
                'message' => 'Appointment updated successfully.',
//                'data' => AppointmentResoruce::collection(AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->get()),
                'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $appointmentSchedule->id)->first()),
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function timeIncreament(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $endTime = date("H:i", strtotime('+15 minutes', strtotime($appointmentSchedule->end_time)));
            $appointmentSchedule->end_time = $endTime;
            $appointmentSchedule->save();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            return response()->json([
                'status' => true,
                'message' => 'Appointment Time Updated successfully.',
                'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $request->appointment_schedule_id)->first()),
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function checkIn(Request $request)
    {


        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
            'timeZoneOffset' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $checkifCurrent = AppointmentSchedule::where('realtor_id', $user->realtor->id)->where('status', 'current')->get();

            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();
            if (!empty($checkifCurrent) && count($checkifCurrent) > 0) {
                return response()->json([
                    'status' => false,
                    'message' => 'You cannot checkin in multiple Appointment',
                    'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
                ], 200);
            } else {

                $timeZone = $this->timeZoneSet($request->timeZoneOffset);
                $actual_start_time = date('H:i', strtotime(Carbon::parse(date('H:i'))->setTimezone($timeZone)));
                $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
                $appointmentSchedule->actual_start_time = $actual_start_time;
                $appointmentSchedule->status = 'current';
                $appointmentSchedule->save();
                $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

                return response()->json([
                    'status' => true,
                    'message' => 'Appointment started successfully.',
                    'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $request->appointment_schedule_id)->first()),
                    'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
                ], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }

    public function checkOut(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
            'timeZoneOffset' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $timeZone = $this->timeZoneSet($request->timeZoneOffset);
            $actual_end_time = date('H:i', strtotime(Carbon::parse(date('H:i'))->setTimezone($timeZone)));
            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $appointmentSchedule->actual_end_time = $actual_end_time;
            $appointmentSchedule->status = 'past';
            $appointmentSchedule->save();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            return response()->json([
                'status' => true,
                'message' => 'Appointment ended successfully.',
                'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $request->appointment_schedule_id)->first()),
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function panicButton(Request $request)
    {
        try {

            $this->emergencyContacts($request->appointment_schedule_id);

            return response()->json(['status' => true, 'message' => 'Messages Send successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function backgroundCheck(Request $request)
    {
        try {
            $user = Auth::user();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            if (!is_null($subscriptionRemaining) && $subscriptionRemaining->remaining_checks > 0) {

                $subscriptionRemaining->remaining_checks = $subscriptionRemaining->remaining_checks - 1;
                $subscriptionRemaining->save();
                $data = $this->xmlRequest('John', 'Doe', '01/01/1901');


                if (count($data['searchResults']['CriminalSearch']['Result']) > 0) {
                    $collection = $data['searchResults']['CriminalSearch']['Result'];
                    $resultArr = $this->filterByDow($collection, 'STEVE', 'DUNN', 'FL', 'SPRING HILL');

                }
                return response()->json(['status' => true, 'message' => 'User Passes Background Check', 'statusCheck' => 'Passed', 'data' => $resultArr], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'No Background Check Available', 'statusCheck' => 'Empty'], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function appointmentHistory()
    {
        try {
            $user = Auth::user();
            $date = today()->format('Y-m-d');
            $appointmentsSchedulePast = AppointmentSchedule::where('realtor_id', $user->realtor->id)->whereIn('status', ['past', 'cancel'])->orderBy('id', 'desc')->get();
            $pastAppointments = AppointmentResoruce::collection($appointmentsSchedulePast);
            $subscription = Subscription::where('realtor_id', $user->realtor->id)->get();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();


            return response()->json([
                'status' => true,
                'message' => 'Appointment History get successfully.',
                'past' => $pastAppointments,
                'subscription' => $subscription,
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function notifications()
    {
        try {
            $user = Auth::user();
            $notifications = Notification::where('type_id', $user->realtor->id)->get();


            return response()->json([
                'status' => true,
                'message' => 'Notifications Get Successfully.',
                'data' => $notifications,
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function deleteNotification($id)
    {
        try {
            $user = Auth::user();
            Notification::where('id', $id)->delete();
            $notifications = Notification::where('type_id', $user->realtor->id)->get();

            return response()->json([
                'status' => true,
                'message' => 'Notifications Deleted Successfully.',
                'data' => $notifications,
            ], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function emergencyContacts($appointmentId)
    {
        $emergencyContacts = EmergencyContact::where('realtor_id', $appointmentId)->get();
        if (!empty($emergencyContacts) && count($emergencyContacts) > 0) {
            //Send Sms to EmergencyContacts
            foreach ($emergencyContacts as $contact) {
//                $this->sendSms($contact->mobile_number, 'hello');
            }

        }
    }

    function filterByDow($items, $firstname, $lastname, $state, $city)
    {
        foreach ($items as $v) {
            if ($v['lastname'] == $lastname && $v['firstname'] == $firstname && $v['state'] == $state && $v['city'] == $city) {
                $result[] = $v;
            }
        }
        return $result;

    }

    public function cancelAppointment($appointment_schedule_id)
    {
        $user = Auth::user();
        try {

            $appointmentSchedule = AppointmentSchedule::where('id', $appointment_schedule_id)->first();
            $appointmentSchedule->status = 'cancel';
            $appointmentSchedule->save();
            $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

            return response()->json([
                'status' => true,
                'message' => 'Appointment cancel successfully.',
                'data' => new AppointmentResoruce(AppointmentSchedule::where('id', $appointment_schedule_id)->first()),
                'remaining_check' => !is_null($subscriptionRemaining) ? $subscriptionRemaining->remaining_checks : 0
            ], 200);


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function packageSubscribe(Request $request)
    {
        $auth_user = Auth::user();
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'package_id' => 'required',
        ], [
            'user_id.required' => 'User ID is required',
            'package_id.required' => 'Package ID is required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {
            $packageData = Package::where('id', $request->package_id)->first();
            $old_subscription = Subscription::where('realtor_id', $auth_user->realtor->id)->where('package_id', '!=', 1)->where('status', 'paid')->first();
            $new_subscription = Subscription::where('realtor_id', $auth_user->realtor->id)->where('package_id', '!=', 1)->where('status', 'unpaid')->first();

            if (isset($new_subscription)) {
                $new_subscription->status = 'paid';
                $new_subscription->start_date = date('Y-m-d');
                $new_subscription->background_checks = $packageData->background_checks;
//                $new_subscription->remaining_checks = $new_subscription->remaining_checks + $packageData->background_checks;

                if (!is_null($old_subscription)) {
                    if (date('Y-m-d') > $old_subscription->expiry_date) {
                    } else {
                        $new_subscription->remaining_checks = isset($old_subscription) ? $old_subscription->remaining_checks + $new_subscription->remaining_checks : 0;
                    }
                } else {
                    $new_subscription->remaining_checks = (int)$new_subscription->addon_checks + (int)$new_subscription->background_checks;
                }
                $new_subscription->expiry_date = date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))));
                $new_subscription->save();
            } else {

                $subscription = Subscription::create([
                    'realtor_id' => $auth_user->realtor->id,
                    'package_id' => $packageData->id,
                    'price' => (int)$packageData->price,
                    'background_checks' => (int)$packageData->background_checks,
                    'remaining_checks' => (int)$packageData->background_checks,
                    'addon_checks' => 0,
                    'status' => 'paid',
                    'start_date' => date('Y-m-d'),
                    'expiry_date' => date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))))
                ]);

                if (isset($old_subscription)) {
                    $subscriptionHistory = new SubscriptionHistory();
                    $subscriptionHistory->realtor_id = $old_subscription->realtor_id;
                    $subscriptionHistory->package_id = $old_subscription->package_id;
                    $subscriptionHistory->remaining_checks = $old_subscription->remaining_checks;
                    $subscriptionHistory->addon_checks = $old_subscription->addon_checks;
                    $subscriptionHistory->price = $old_subscription->price;
                    $subscriptionHistory->background_checks = $old_subscription->background_checks;
                    $subscriptionHistory->start_date = $old_subscription->start_date;
                    $subscriptionHistory->expiry_date = $old_subscription->expiry_date;
                    $subscriptionHistory->save();
                    $old_subscription->delete();
                }
                return response()->json([
                    'status' => true,
                    'message' => 'Package subscribe successfully.',
                ], 200);
            }
        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function createShortUrlLink($user_id, $appointment_schedule_id)
    {
        $appUrl = 'https://safechex.com/realtor/appointment/background/check/' . $appointment_schedule_id . '/' . $user_id;
        $code = substr(str_shuffle("0123456789abcdefghijklmnopqrstvwxyz"), 0, 6);
        ShortLink::create([
            'link' => $appUrl,
            'code' => $code
        ]);
        return $code;
    }

}
