<?php

namespace App\Http\Controllers\Auth;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Controller;
use App\Mail\ContactUsEmail;
use App\Mail\ForgotPasswordRequest;
use App\Mail\OTPCodeEmail;
use App\Models\User;
use App\PasswordReset;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    public function otp(Request $request)
    {
        $details = ['email' => $request->email];

        $validator = Validator::make($details, [
            'email' => 'required|exists:users,email',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {
            $otp_code = null;
            $user = User::where('email', $request->email)->first();
            if (is_null($user->otp_code)) {
                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
            } else {
                $otp_code = $user->otp_code;
            }
            $user->update([
                'otp_code' => $otp_code,
            ]);


            $data = [
                'otp' => $otp_code,
                'email' => $user->email,
            ];
            $this->verificationEmail($user,$otp_code);

            return response()->json(['message' => 'OTP code has been sent on given email address.', 'data' => $data], 201);

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {


            $user = User::where([
                'email' => $request->get('email'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {
                $user->otp_status = 'verified';
                $user->save();

                return response()->json(['status' => 'success', 'message' => ''], 200);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Provided OTP code is not correct.'], 401);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function ForgotPasswordRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            $user = User::whereEmail($request->email)->first();

            if (is_null($user) || is_null($user->email) || empty($user->email)) {
                return response()->json(["status" => 'error', "message" => 'Your account is not associated with this email address.'], 404);
            }
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json(['status' => 'success', "message" => 'Your password has been changes successfully.'], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }
    }

    public function verificationEmail($user,$otp_code)
    {
        $details = [
            'greeting' => 'Hi ' . $user->first_name . ' ' . $user->last_name,
            'body' => 'Your OTP Code: '.$otp_code,
            'from' => config('app.admin'),
            'subject'=>'Reset Password'

        ];
        Mail::to($user->email)->send(new OTPCodeEmail($details));
    }

}
