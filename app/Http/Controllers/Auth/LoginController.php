<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Models\Activation;
use App\VerifyUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {


        return view('auth.login');
    }

    public function username()
    {
        return 'email';
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->status == 'suspended') {
            Auth::logout();
            return redirect()->route('index')->with([
                'flash_status' => 'error',
                'flash_message' => 'Your Account has been suspended.Please Contact Support.'
            ]);
        } else {

//            if (!$user->activated) {
//
//                auth()->logout();
//                return redirect()->route('login')->with([
//                    'flash_status' => 'warning',
//                    'flash_message' => 'You need to confirm your account. We have sent you an activation email.'
//
//                ]);
//            } else {
                if ($user->hasRole('realtor')) {
                    return redirect()->route('realtor.dashboard.index');
                }
                if ($user->hasRole('admin')) {
                    return redirect()->route('admin.dashboard.index');
                }


                Auth::logout();

                return redirect()->route('login')->with([
                    'flash_status' => 'error',
                    'flash_message' => 'You are Unauthorized for this login.'
                ]);
//            }


        }


    }

    public function activation($token)
    {

        $user = Auth::check();
        $activation = Activation::where('token', $token)->first();
        if (isset($activation)) {
            $user = $activation->user;
            if (!$user->activated) {
                $user = $activation->user;
                $user->activated = true;
                $user->save();
                $status = "Your e-mail is verified. You can now login.";
            } else {
                $status = "Your e-mail is already verified. You can now login.";
            }
        } else {
            return redirect('/login')->with([
                'flash_status' => 'warning',
                'flash_message' => 'Sorry your email cannot be identified.'
            ]);
        }

        return redirect('/login')->with([
            'flash_status' => 'success',
            'flash_message' => $status
        ]);
    }

}
