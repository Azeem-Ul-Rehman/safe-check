<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;

use App\Mail\EventEmail;
use App\Models\Notification;
use App\Models\Package;
use App\Models\Realtor;
use App\Models\Role;
use App\Models\UserRole;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Mail\VerifyMail;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Helpers\SendSms;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'username' => 'required|string',
            'phone_number' => 'required|unique:users,phone_number',
            'home_number' => 'required|unique:users,emergency_number',
            'pin_number' => 'required',
            'terms-and-conditions' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
        ], [
            'first_name.required' => 'First name field  is required.',
            'last_name.required' => 'Last name field is required.',
            'username.required' => 'Username field is required.',
            'phone_number.required' => 'Phone Number field  is required.',
            'home_number.required' => 'Home Number field  is required.',
            'pin_number.required' => 'Pin Number field  is required.',
            'email.required' => 'Email field  is required.',
            'terms-and-conditions.required' => 'Please Accept.',
        ]);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }
        $role = Role::find(2);

        $user = User::create([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'username' => $request->get('username'),
            'role_id' => $role->id,
            'phone_number' => $request->get('phone_number'),
            'emergency_number' => $request->get('home_number'),
            'user_type' => $role->name,
            'status' => 'verified',
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'profile_pic' => $profile_image,
        ]);

        UserRole::create([
            'user_id' => $user->id,
            'role_id' => $role->id
        ]);


        $realtor = new Realtor();
        $realtor->user_id = $user->id;
        $realtor->pin_number = $request->get('pin_number');
        $realtor->save();


        $packages = Package::all();
        $textPage = 'register';
        Auth::login($user);
        $messageShow = false;

        $details = [
            'greeting' => 'Hi',
            'name' => $request->get('first_name') . ' ' . $request->get('last_name'),
            'body' => 'Thank you for becoming a member of SafeChex! Someone from our team will  contact you shortly to complete your registration process.',
            'thanks' => 'Thank you for using SafeChex ',
            'from' => 'regmedapp@gmail.com'

        ];


        //Realtor Notification
        $notification = new Notification();
        $notification->type = 'realtor';
        $notification->message = 'Thank you for becoming a member of SafeChex! Someone from our team will  contact you shortly to complete your registration process.';
        $notification->email_sent = true;
        $notification->type_id = $realtor->id;
        $notification->save();


        Mail::to($request->email)->send(new EventEmail($details));


        return view('frontend.pages.subscription', compact('user', 'packages', 'textPage', 'messageShow'));


//        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//        $charactersLength = strlen($characters);
//        $randomString = '';
//        for ($i = 0; $i < 8; $i++) {
//            $randomString .= $characters[rand(0, $charactersLength - 1)];
//        }
//
//        $activation = $user->activation()->create([
//            'token' => $randomString
//        ]);
//        Mail::to($user->email)->send(new VerifyMail($user, $randomString));
//
//
////        $remove_zero = ltrim($user->phone_number, '0');
////        $add_number = '92';
////        $phone_number = $add_number . $remove_zero;
//        $phone_number = $user->phone_number;
//
//        $message = 'Thank you for becoming a member of SafeCheck! Someone from our team will be calling you shortly to complete your registration process. Thank you!';
//        //  event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
//
//
//        $admin_phone_number = User::where('user_type', 'admin')->first()->phone_number;
//        $message1 = 'A new realtor ' . ucfirst($request->get('first_name')) . ' ' . ucfirst($request->get('last_name')) . ' has registered. Please call customer to complete verification process.';
//        //  event(new SendReferralCodeWithPhone($user_name = '', $admin_phone_number, $message1));


//
//        $response['status'] = 'success';
//        $response['data'] = [
//            'flash_status' => 'success',
//            'flash_message' => 'Dear Customer,Thank you for signing up at safechex'
//        ];


//        session()->put('user', 'register');


//        return redirect()->route('login')->with(['flash_status' => 'success', 'flash_message' => 'Dear Customer,Thank you for signing up at safechex']);

    }


}
