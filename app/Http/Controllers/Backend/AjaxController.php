<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\City;
use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Subscription;


class AjaxController extends Controller
{
    use GeneralHelperTrait;

    public function example(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
        ], [

        ]);

        if (!$validator->fails()) {


            $response['status'] = 'success';
            $response['data'] = [

            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function updateAddOnChecks(Request $request)
    {
        $response = array('status' => '', 'message' => "");

        $getSubscription = Subscription::with('package')->where('id', $request->Id)->first();
        if (isset($getSubscription)) {
            $total_add_on = $request->newAddOn;
            $total_price = ($total_add_on * $getSubscription->package->addon_price) + $getSubscription->package->price;

            $getSubscription->addon_checks = $total_add_on;
            $getSubscription->price = $total_price;
            $getSubscription->save();
        }

        $response['status'] = 'success';
        $response['message'] = 'Addon Checks Updated sucessfully.';
        return $response;
    }

    public function stateCities(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'state_id' => 'required|exists:states,id'
        ], [
            'state_id.required' => "State is Required.",
            'state_id.exists' => "Invalid State Selected."
        ]);

        if (!$validator->fails()) {
            $cities = City::where('state_id', $request->state_id)->orderBy('name', 'asc')->get();

            $response['status'] = 'success';
            $response['data'] = [
                'cities' => $cities,
            ];
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }

    public function xmlRequestBackgroundCheck(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $user = Auth::user();
        $subscriptionRemaining = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->first();

        if (!is_null($subscriptionRemaining) && $subscriptionRemaining->remaining_checks > 0) {

            $subscriptionRemaining->remaining_checks = $subscriptionRemaining->remaining_checks - 1;
            $subscriptionRemaining->save();


            $response['status'] = 'success';
            $response['data'] = [
                'backgroundCheckResponse' => $this->xmlRequest($request->first_name, $request->first_name, date('m/d/Y', strtotime($request->dob))),
            ];
        } else {

            $response['status'] = 'error';
            $response['message'] = 'No Background Check Available';
            $response['data'] = [
                'backgroundCheckResponse' => [],
            ];
        }


        return $response;


    }


}
