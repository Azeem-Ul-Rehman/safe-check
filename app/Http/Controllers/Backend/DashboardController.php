<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $users_count = User::all()->count();
        $staff_users = User::where('user_type', 'realtor')->get()->toArray();

        return view('backend.dashboard.index', compact('users_count', 'staff_users'));
    }
}
