<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Package;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $packages = Package::orderBy('id', 'DESC')->get();

        return view('backend.packages.index', compact('packages'));
    }

    public function create()
    {
        return view('backend.packages.create');

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'background_checks' => 'required|numeric',
            'addon_price' => 'required|numeric',
        ]);
        Package::create($request->all());
        return redirect()->route('admin.packages.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Package created successfully.'
            ]);

    }

    public function edit($id)
    {
        $package = Package::find($id);
        return view('backend.packages.edit', compact('package'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'background_checks' => 'required|numeric',
            'addon_price' => 'required|numeric',
        ]);
        Package::find($id)->update($request->all());
        return redirect()->route('admin.packages.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Package updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $package = Package::findOrFail($id);
        if (!empty($package->realtors) || !empty($package->realtorSubcriptionHistory)) {
            return redirect()->route('admin.packages.index')
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'Someone using this package you cannot delete.'
                ]);
        } else {
            $package->delete();

            return redirect()->route('admin.packages.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Package has been deleted'
                ]);
        }

    }

}
