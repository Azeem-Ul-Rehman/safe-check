<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Imports\StatesImport;
use App\Models\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class StateController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $areas = State::orderBy('id', 'DESC')->get();

        return view('backend.state.index', compact('areas'));
    }

    public function create()
    {
        return view('backend.state.create');

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ], [
        ]);
        State::create($request->all());
        return redirect()->route('admin.states.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'State created successfully.'
            ]);

    }

    public function edit($id)
    {
        $area = State::find($id);
        return view('backend.state.edit', compact('area'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ], [
        ]);
        State::find($id)->update($request->all());
        return redirect()->route('admin.states.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'State updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $area = State::findOrFail($id);
        $area->delete();

        return redirect()->route('admin.states.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'State has been deleted'
            ]);
    }

    public function importFile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:xls,xlsx'
        ]);
        if (!$validator->fails()) {

            Excel::import(new StatesImport(), $request->file('file'));

            $status = 'success';
            $message = 'State File Imported Successfully.';
        } else {
            $status = 'error';
            $message = 'File Format is not correct or Incorrect Data.';
        }

        return redirect()->route('admin.states.index')
            ->with([
                'flash_status' => $status,
                'flash_message' => $message
            ]);
    }
}
