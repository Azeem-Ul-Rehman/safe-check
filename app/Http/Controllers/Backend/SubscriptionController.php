<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $subscriptions = Subscription::orderBy('id', 'DESC')->get();

        return view('backend.subscriptions.index', compact('subscriptions'));
    }

    public function create()
    {
        return view('backend.subscriptions.create');

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric',
            'background_checks' => 'required|numeric',
        ]);
        Subscription::create($request->all());
        return redirect()->route('admin.subscriptions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Subscription created successfully.'
            ]);

    }

    public function edit($id)
    {
        $subscription = Subscription::find($id);
        return view('backend.subscriptions.edit', compact('subscription'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'price' => 'required|numeric',
            'background_checks' => 'required|numeric',
        ]);


        $subscription = Subscription::find($id);

        $subscription->price = $request->price;
        $subscription->background_checks = $request->background_checks;
        $subscription->save();
        return redirect()->route('admin.subscriptions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Subscription updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $subscription = Subscription::findOrFail($id);
        $subscription->delete();

        return redirect()->route('admin.subscriptions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Subscription has been deleted'
            ]);
    }
}
