<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\TermsAndCondition;
use Illuminate\Http\Request;

class TermsAndConditionsController extends Controller
{
    public function index()
    {
        $terms = TermsAndCondition::first();
        return view('backend.terms_and_conditions.create', compact('terms'));
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'description' => 'required',
        ], [
            'description.required' => 'Description is required.'
        ]);
        $terms = TermsAndCondition::first();
        if (!is_null($terms)) {
            $terms->update([
                'description' => $request->description
            ]);
        } else {
            TermsAndCondition::create([
                'description' => $request->description
            ]);
        }


        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Terms and Conditions description updated successfully.'
        ]);
    }
}
