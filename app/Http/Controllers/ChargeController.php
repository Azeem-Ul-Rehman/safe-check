<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Subscription;
use App\Models\SubscriptionHistory;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Nikolag\Square\Facades\Square;
use Nikolag\Square\Models\Customer;

class ChargeController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function createCustomer($user)
    {

        $customerArr = [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'company_name' => 'SAFECHEX',
            'nickname' => $user->username,
            'email' => $user->email,
            'phone' => $user->phone_number,
            'note' => $user->id,
        ];
        $customer = new Customer($customerArr);
        $customer->save();

        return $customer;
    }


    public function charge(Request $request)
    {

        $merchant = Auth::user();

        $transaction = Square::charge([
            'amount' => (int)str_replace(" ", "", $request->amount),
            'source_id' => str_replace(" ", "", $request->nonce),
            'location_id' => 'QQVQ8P7F912TK',
            'currency' => 'USD',
        ]);

        return response()->json(compact('transaction'));
    }


    public function chargeWithMerchant(User $merchant)
    {

        $transaction = Square::setMerchant($merchant)->charge([
            'amount' => 500,
            'source_id' => 'cnon:card-nonce-ok',
            'location_id' => 'QQVQ8P7F912TK',
            'currency' => 'USD',
        ]);

        return response()->json(compact('transaction'));
    }


    public function chargeWithCustomer(Customer $customer)
    {
        $transaction = Square::setCustomer($customer)->charge([
            'amount' => 500,
            'source_id' => 'cnon:card-nonce-ok',
            'location_id' => 'QQVQ8P7F912TK',
            'currency' => 'USD',
        ]);
        return response()->json(compact('transaction'));
    }


    public function chargeWithMerchantAndCustomer(Request $request)
    {

        $merchant = User::where('user_type', 'admin')->first();
        $customer = $this->createCustomer(Auth::user());

        $transaction = Square::setMerchant($merchant)
            ->setCustomer($customer)->charge([
                'amount' => (int)str_replace(" ", "", $request->amount),
                'source_id' => str_replace(" ", "", $request->nonce),
                'location_id' => 'QQVQ8P7F912TK',
                'currency' => 'USD',
            ]);


        if ($transaction->status == 'PAID') {

            if ($request->type == 'add_more_checks') {

                $updateSubscription = Subscription::where('id', $request->subscription_id)->first();
                $updateSubscription->remaining_checks = $updateSubscription->remaining_checks + $request->add_checks;
                $updateSubscription->save();
                return redirect()->route('index')->with(['flash_status' => 'success', 'flash_message' => 'Payment Successfull.More Checks Added Successfully.']);

            } else {
                $auth_user = Auth::user();
                $old_subscription = Subscription::where('realtor_id', $auth_user->realtor->id)->where('package_id', '!=', 1)->where('status', 'paid')->first();
                $new_subscription = Subscription::where('realtor_id', $auth_user->realtor->id)->where('package_id', '!=', 1)->where('status', 'unpaid')->first();
                if (isset($new_subscription)) {


                    if (!is_null($old_subscription)) {
                        if (date('Y-m-d') > $old_subscription->expiry_date) {
                        } else {
                            $new_subscription->remaining_checks = isset($old_subscription) ? $old_subscription->remaining_checks + $new_subscription->remaining_checks : 0;
                        }
                    } else {
                        $new_subscription->remaining_checks = (int)$new_subscription->addon_checks + (int)$new_subscription->background_checks;
                    }


                    $new_subscription->status = 'paid';
                    $new_subscription->start_date = date('Y-m-d');
                    $new_subscription->expiry_date = date('Y-m-d', strtotime("+1 months", strtotime(date('Y-m-d'))));
                    $new_subscription->save();

                    if (isset($old_subscription)) {
                        $subscriptionHistory = new SubscriptionHistory();
                        $subscriptionHistory->realtor_id = $old_subscription->realtor_id;
                        $subscriptionHistory->package_id = $old_subscription->package_id;
                        $subscriptionHistory->remaining_checks = $old_subscription->remaining_checks;
                        $subscriptionHistory->addon_checks = $old_subscription->addon_checks;
                        $subscriptionHistory->price = $old_subscription->price;
                        $subscriptionHistory->background_checks = $old_subscription->background_checks;
                        $subscriptionHistory->start_date = $old_subscription->start_date;
                        $subscriptionHistory->expiry_date = $old_subscription->expiry_date;
                        $subscriptionHistory->save();
                        $old_subscription->delete();
                    }

                    return redirect()->route('index')->with(['flash_status' => 'success', 'flash_message' => 'Payment Successfull.Thank you for using ours service.']);
                }

            }


        } else {
            return redirect()->route('index')->with(['flash_status' => 'warning', 'flash_message' => 'Payment not successfull.Please try Again']);

        }

    }


    public function subscribe(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'realtor_id' => 'required',
            'package_id' => 'required',
        ], [

            'realtor_id.required' => 'Realtor is required',
            'package_id.required' => 'Package is required',
        ]);

        if ($validator->fails()) {
            return back()->with([
                'flash_status' => 'error',
                'flash_message' => 'Please Select package to subscribe.'
            ]);
        }



        $package = Package::where('id', $request->package_id)->first();
        $subscription = Subscription::where('realtor_id', $request->realtor_id)->where('package_id', '!=', 1)->where('status', 'unpaid')->first();

        $getfreesubscription = Subscription::where('realtor_id', $request->realtor_id)->where('package_id', '=', 1)->first();

//
        if (!is_null($getfreesubscription)) {

        } else {
            if ($package->id != 1) {
                $packageData = Package::where('id', 1)->first();
                $subscription = Subscription::create([
                    'realtor_id' => $request->realtor_id,
                    'package_id' => $packageData->id,
                    'price' => $packageData->price,
                    'background_checks' => $packageData->background_checks,
                    'remaining_checks' => 0,
                    'addon_checks' => 0,
                    'status' => 'paid',
                ]);
            }
        }

        if (!is_null($subscription)) {
            $subscription->realtor_id = $request->realtor_id;
            $subscription->package_id = $package->id;
            $subscription->price = $request->total;
            $subscription->background_checks = $package->background_checks;
            $subscription->remaining_checks = $package->background_checks + (isset($request->add_more_check_count[$package->id]) ? $request->add_more_check_count[$package->id] : 0);
            $subscription->addon_checks = (isset($request->add_more_check_count[$package->id])) ? $request->add_more_check_count[$package->id] : 0;
            $subscription->status = (strtolower($package->name) == 'free') ? 'paid' : 'unpaid';
            $subscription->save();
        } else {
            $subscription = Subscription::create([
                'realtor_id' => $request->realtor_id,
                'package_id' => $package->id,
                'price' => $request->total,
                'background_checks' => $package->background_checks,
                'remaining_checks' => $package->background_checks + (isset($request->add_more_check_count[$package->id]) ? $request->add_more_check_count[$package->id] : 0),
                'addon_checks' => (isset($request->add_more_check_count[$package->id]) ? $request->add_more_check_count[$package->id] : 0),
                'status' => (strtolower($package->name) == 'free') ? 'paid' : 'unpaid',
            ]);
        }
        if (strtolower($package->name) == 'free') {
            if ($request->text_page == 'register') {
                $message = 'Dear Customer,Thank you for signing up at safechex.';
            } else {
                $message = 'Dear Customer,Thank you for subscribing.';
            }

            return redirect()->route('index')->with(['flash_status' => 'success', 'flash_message' => $message]);
        } else {
            if ($request->text_page == 'register') {
                $message = 'Dear Customer,Thank you for signing up at safechex and please pay for using our service.';
            } else {
                $message = 'Dear Customer,Thank you for subscribing.Please pay for using our service';
            }


            return view('frontend.pages.payment', compact('subscription', 'package'))->with(['flash_status' => 'success', 'flash_message' => $message]);
//            return redirect()->route('payment')->with(['flash_status' => 'success', 'flash_message' => $message, 'package' => $package, 'subscription' => $subscription]);
        }


    }

    public function finalPayment()
    {
//        try {
        $subscription = \App\Models\Subscription::where('realtor_id', auth()->user()->realtor->id)->where('status', 'unpaid')->first();


        if (is_null($subscription)) {
            return redirect()->back()->with(['flash_status' => "error", 'flash_message' => "You are not able to access this page."]);
        }
        $package = \App\Models\Package::where('id', $subscription->package_id)->first();
        return view('frontend.pages.payment', compact('subscription', 'package'));
//        } catch (\Exception $ex) {
//            return redirect()->back()->with(['flash_status' => "error", 'flash_message' => "You are not able to access this page."]);
//        }
    }
}
