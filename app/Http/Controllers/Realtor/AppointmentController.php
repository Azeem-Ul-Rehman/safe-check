<?php

namespace App\Http\Controllers\Realtor;

use App\Http\Controllers\Controller;
use App\Http\Resources\AppointmentResoruce;
use App\Models\Appointment;
use App\Models\AppointmentSchedule;
use App\Models\BackgroundCheck;
use App\Models\City;
use App\Models\EmergencyContact;
use App\Models\Notification;
use App\Models\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;
use Google_Service_Calendar_Events;
use Illuminate\Support\Facades\DB;
use App\Traits\GeneralHelperTrait;


class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    use GeneralHelperTrait;

    protected $client;

    public function __construct()
    {

        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $client->addScope(Google_Service_Calendar::CALENDAR);
        $client->setScopes(Google_Service_Calendar::CALENDAR_EVENTS);

        $guzzleClient = new \GuzzleHttp\Client(array('curl' => array(CURLOPT_SSL_VERIFYPEER => false)));
        $client->setHttpClient($guzzleClient);
        $this->client = $client;
    }


    public function index()
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';

            $results = $service->events->listEvents($calendarId);
            $user = Auth::user();
            $appointments = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->with(['appointment', 'emergencyContacts'])->get();
            return view('realtor.appointments.index', compact('appointments'));

        } else {
            return redirect()->route('oauthCallback');
        }

    }

    public function oauth()
    {

        session_start();

        $rurl = action('Realtor\AppointmentController@oauth');

        $this->client->setRedirectUri($rurl);

        if (!isset($_GET['code'])) {
            $auth_url = $this->client->createAuthUrl();
            $filtered_url = filter_var($auth_url, FILTER_SANITIZE_URL);
            return redirect($filtered_url);
        } else {

            if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
                $user = Auth::user();
                $appointments = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->with(['appointment', 'emergencyContacts'])->get();


                return view('realtor.appointments.index', compact('appointments'));
            } else {
                $this->client->authenticate($_GET['code']);
                $_SESSION['access_token'] = $this->client->getAccessToken();
                $user = Auth::user();
                $appointments = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->with(['appointment', 'emergencyContacts'])->get();


                return view('realtor.appointments.index', compact('appointments'));
            }

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::orderBy('name', 'asc')->get();
        return view('realtor.appointments.create', compact('states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();


        $startTime = $request->start_time_hour . ":" . $request->start_time_min;
        $endTime = $request->end_time_hour . ":" . $request->end_time_min;


        session_start();
        $timeZoneData = $this->timeZoneSet($request->timeZoneOffset);
        $dtz = new \DateTimeZone($timeZoneData);//Request TimeZone from Frontend
        $time_in_sofia = new \DateTime('now', $dtz);
        $offset = $dtz->getOffset($time_in_sofia) / 3600;
//        if ($offset > 9) {
//            $timeZone = '00-' . $offset . ':00';
//        } else {
//            $timeZone = '00-0' . $offset . ':00';
//        }
        $timeZone = '00-0' . '0' . ':00';
        $startDateTime = date('Y-m-d', strtotime($request->start_date)) . 'T' . date("H:i", strtotime($startTime)) . ':' . $timeZone;
        $endDateTime = date('Y-m-d', strtotime($request->start_date)) . 'T' . date("H:i", strtotime($endTime)) . ':' . $timeZone;

        $appointment = new Appointment();

        $appointment->cl_id = rand();
        $appointment->cl_first_name = $request->cl_first_name;
        $appointment->cl_last_name = $request->cl_last_name;
        $appointment->cl_dob = $request->cl_dob;
        $appointment->cl_doc_id_number = $request->cl_doc_id_number;
        $appointment->cl_doc_id_state = $request->cl_doc_id_state;
        $appointment->cl_email = $request->cl_email;
        $appointment->cl_mobile_number = $request->cl_mobile_number;
        $appointment->property_address_one = $request->property_address_one;
        $appointment->property_address_two = $request->property_address_two;
        $appointment->property_city = $request->property_city;
        $appointment->property_state = $request->property_state;
        $appointment->property_zip_code = $request->property_zip_code;
        if ($request->has('upload_doc')) {
            $image = $request->file('upload_doc');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/appointmentImage');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }
        $appointment->image = $profile_image;
        if ($request->background_check == "1") {
            $appointment->bg_status = 'accepted';
        }
        $appointment->save();


        $appointmentSchedule = new AppointmentSchedule();
        $appointmentSchedule->realtor_id = $user->realtor->id;
        $appointmentSchedule->appointment_id = $appointment->id;
        $appointmentSchedule->pin_number = $request->pin_number;
        $appointmentSchedule->start_date = date('Y-m-d', strtotime($request->start_date));
        $appointmentSchedule->start_time = date("H:i", strtotime($startTime));
        $appointmentSchedule->end_time = date("H:i", strtotime($endTime));
        $appointmentSchedule->time_zone = $timeZoneData;
        $appointmentSchedule->local_police_number = $request->local_police_number;
        $appointmentSchedule->background_check = $request->background_check == 1 ? true : false;
        $appointmentSchedule->save();

        $a = 0;
        foreach ($request->name as $name) {
            EmergencyContact::create([
                'realtor_id' => $appointmentSchedule->id,
                'name' => $name,
                'mobile_number' => $request->mobile_number[$a],
            ]);
            $a++;

            // Send SMS to Emergency Contact
            // $this->sendSms($contact['mobile_number'], 'hello');

        }

        // Send SMS to Appointment Person for the user
        // $this->sendSms($request->cl_mobile_number, 'hello');


        //Realtor Notification
        $notification = new Notification();
        $notification->type = 'appointment';
        $notification->data_id = $appointmentSchedule->id;
        $notification->message = 'Your Appointment has been created Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '';
        $notification->sms_sent = true;
        $notification->type_id = \auth()->user()->realtor->id;
        $notification->save();
        // Send SMS as Well
        // $this->sendSms(\auth()->user()->phone_number, 'hello');


        if ($request->background_check == 1) {


            if (!is_null($request->final_firstname)) {
                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
                if (!is_null($checkRecordBg)) {
                    $backgroundCheck = $checkRecordBg;
                } else {
                    $backgroundCheck = new BackgroundCheck();
                }

                $backgroundCheck->appointment_schedule_id = $appointmentSchedule->id;
                $backgroundCheck->first_name = $request->final_firstname;
                $backgroundCheck->last_name = $request->final_lastname;
                $backgroundCheck->ssn = $request->final_ssn;
                $backgroundCheck->dob = $request->final_dob;
                $backgroundCheck->city = $request->final_city;
                $backgroundCheck->state = $request->final_state;
                $backgroundCheck->save();
            }

        }


        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $calendarId = 'primary';
            $event = new Google_Service_Calendar_Event([
                'summary' => 'New Appointment',
                'description' => 'Your Appointment has been created Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '',
                'start' => ['dateTime' => $startDateTime],
                'end' => ['dateTime' => $endDateTime],
                'reminders' => ['useDefault' => true],
            ]);


            $results = $service->events->insert($calendarId, $event);


        }


        return redirect(route('realtor.appointments.index'))->with(['flash_status' => "success", 'flash_message' => "Appointment Added Successfully!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getAppoitment = AppointmentSchedule::with(['appointment', 'emergencyContacts', 'backgroundCheck'])->where('id', $id)->first();

        $backgroundCheck = BackgroundCheck::where('ssn', $getAppoitment->appointment->cl_doc_id_number)->first();
        $states = State::orderBy('name', 'asc')->get();
        $cities = DB::table('cities')->where('id', (int)$getAppoitment->appointment->property_city)->get();


        return view('realtor.appointments.edit')->with([
            'getAppoitment' => $getAppoitment,
            'states' => $states,
            'cities' => $cities,
            'backgroundCheck' => $backgroundCheck
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $startTime = $request->start_time_hour . ":" . $request->start_time_min . "";
        $endTime = $request->end_time_hour . ":" . $request->end_time_min . "";
        $timeZoneData = $this->timeZoneSet($request->timeZoneOffset);

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
            'cl_id' => 'required',
            'cl_first_name' => 'required|string',
            'cl_last_name' => 'required|string',
            'cl_dob' => 'required',
            'cl_doc_id_number' => 'required',
            'cl_doc_id_state' => 'required',
            'cl_email' => 'required|email',
            'cl_mobile_number' => 'required|numeric|digits_between:11,12',
            'property_address_one' => 'required',
            'property_address_two' => 'nullable',
            'property_city' => 'required',
            'property_state' => 'required',
            'property_zip_code' => 'required|numeric',
            'pin_number' => 'required',
            'start_date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'emergency_contacts' => 'required',
        ]);
        $emergenciesContact = $request->emergency_contacts;
//        dd(date("H:i", strtotime($startTime)));

        $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
        $appointmentSchedule->pin_number = (!is_null($appointmentSchedule->pin_number)) ? $appointmentSchedule->pin_number : $user->realtor->pin_number;
        $appointmentSchedule->start_date = date('Y-m-d', strtotime($request->start_date));
        $appointmentSchedule->start_time = date("H:i", strtotime($startTime));
        $appointmentSchedule->end_time = date("H:i", strtotime($endTime));
        $appointmentSchedule->time_zone = $timeZoneData;
        $appointmentSchedule->local_police_number = $request->local_police_number;
        $appointmentSchedule->background_check = $request->background_check == 1 ? true : false;
        $appointmentSchedule->save();

        $appointment = Appointment::where('id', $appointmentSchedule->appointment_id)->first();
        $appointment->cl_id = $appointment->cl_id;
        $appointment->cl_first_name = $request->cl_first_name;
        $appointment->cl_last_name = $request->cl_last_name;
        $appointment->cl_dob = $request->cl_dob;
        $appointment->cl_doc_id_number = $request->cl_doc_id_number;
        $appointment->cl_doc_id_state = $request->cl_doc_id_state;
        $appointment->cl_email = $request->cl_email;
        $appointment->cl_mobile_number = $request->cl_mobile_number;
        $appointment->property_address_one = $request->property_address_one;
        $appointment->property_address_two = $request->property_address_two;
        $appointment->property_city = $request->property_city;
        $appointment->property_state = $request->property_state;
        $appointment->property_zip_code = $request->property_zip_code;
        if ($request->background_check == "1") {
            $appointment->bg_status = 'accepted';
        }
        if ($request->has('upload_doc')) {
            $image = $request->file('upload_doc');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/appointmentImage');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $appointment->image;
        }
        $appointment->image = $profile_image;
        $appointment->save();


        $appointmentSchedule->emergencyContacts()->delete();
        $a = 0;
        foreach ($request->name as $name) {
            EmergencyContact::create([
                'realtor_id' => $appointmentSchedule->id,
                'name' => $name,
                'mobile_number' => $request->mobile_number[$a],
            ]);
            $a++;
            // Send SMS to Emergency Contact
            // $this->sendSms($contact['mobile_number'], 'hello');
        }


        // Send SMS to Appointment Person for the user
        // $this->sendSms($request->cl_mobile_number, 'hello');


        //Realtor Notification
        $notification = new Notification();
        $notification->type = 'appointment';
        $notification->data_id = $appointmentSchedule->id;
        $notification->message = 'Your Appointment has been updated Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '';
        $notification->sms_sent = true;
        $notification->type_id = \auth()->user()->realtor->id;
        $notification->save();
        // Send SMS as Well
        // $this->sendSms(\auth()->user()->phone_number, 'hello');


        if ($request->background_check == "1") {

            if (!is_null($request->final_firstname)) {
                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
                if (!is_null($checkRecordBg)) {
                    $backgroundCheck = $checkRecordBg;
                } else {
                    $backgroundCheck = new BackgroundCheck();
                }
                $backgroundCheck->first_name = $request->final_firstname;
                $backgroundCheck->appointment_schedule_id = $appointmentSchedule->id;
                $backgroundCheck->last_name = $request->final_lastname;
                $backgroundCheck->ssn = $request->final_ssn;
                $backgroundCheck->dob = $request->final_dob;
                $backgroundCheck->city = $request->final_city;
                $backgroundCheck->state = $request->final_state;
                $backgroundCheck->save();
            }
        }


        session_start();


        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $dtz = new \DateTimeZone('Asia/Karachi');//Request TimeZone from Frontend
            $time_in_sofia = new \DateTime('now', $dtz);
            $offset = $dtz->getOffset($time_in_sofia) / 3600;
            if ($offset > 9) {
                $timeZone = '00-' . $offset . ':00';
            } else {
                $timeZone = '00-0' . $offset . ':00';
            }
            $startDateTime = date('Y-m-d', strtotime($request->start_date)) . 'T' . date("H:i", strtotime($startTime)) . ':' . $timeZone;
            $endDateTime = date('Y-m-d', strtotime($request->start_date)) . 'T' . date("H:i", strtotime($endTime)) . ':' . $timeZone;

//            dd($startDateTime,$endDateTime);
//            // retrieve the event from the API.
//            $event = $service->events->get('primary', $id); //eventId
//
//
//            $event->setSummary('New Appointment');
//
//            $event->setDescription('Your Appointment has been updated Successfully.Your Appointment Id :: ' . $appointmentSchedule->id . '');
//
//            //start time
//            $start = new Google_Service_Calendar_EventDateTime();
//            $start->setDateTime($startDateTime);
//            $event->setStart($start);
//
//            //end time
//            $end = new Google_Service_Calendar_EventDateTime();
//            $end->setDateTime($endDateTime);
//            $event->setEnd($end);
//
//
//            $updatedEvent = $service->events->update('primary', $event->getId(), $event);
        }


        return redirect(route('realtor.appointments.index'))->with(['flash_status' => "success", 'flash_message' => "Appointment Updated Successfully!"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session_start();
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $this->client->setAccessToken($_SESSION['access_token']);
            $service = new Google_Service_Calendar($this->client);

            $service->events->delete('primary', $id);

        } else {
            return redirect()->route('oauthCallback');
        }
    }

    public function timeIncreament(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $endTime = date("H:i", strtotime('+15 minutes', strtotime($appointmentSchedule->end_time)));
            $appointmentSchedule->end_time = $endTime;
            $appointmentSchedule->save();
            return response()->json(['status' => true, 'message' => 'Appointment Time Updated successfully.', 'data' => AppointmentResoruce::collection(AppointmentSchedule::where('id', $request->appointment_schedule_id)->get())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function checkIn(Request $request)
    {


        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
            'time_zone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $actual_start_time = date('H:i', strtotime(Carbon::parse(date('H:i'))->setTimezone($request->time_zone)));
            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $appointmentSchedule->actual_start_time = $actual_start_time;
            $appointmentSchedule->save();
            return response()->json(['status' => true, 'message' => 'Appointment started successfully.', 'data' => AppointmentResoruce::collection(AppointmentSchedule::where('id', $request->appointment_schedule_id)->get())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }


    }

    public function checkOut(Request $request)
    {

        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required',
            'time_zone' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        try {

            $actual_end_time = date('H:i', strtotime(Carbon::parse(date('H:i'))->setTimezone($request->time_zone)));
            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $appointmentSchedule->actual_end_time = $actual_end_time;
            $appointmentSchedule->save();
            return response()->json(['status' => true, 'message' => 'Appointment ended successfully.', 'data' => AppointmentResoruce::collection(AppointmentSchedule::where('id', $request->appointment_schedule_id)->get())], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }

    }

    public function googleCalenderAppointment()
    {

        // Get the API client and construct the service object.
        $client = $this->getClient();
        $service = new Google_Service_Calendar($client);

// Print the next 10 events on the user's calendar.
        $calendarId = 'primary';
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => date('c'),
        );
        $results = $service->events->listEvents($calendarId, $optParams);
        $events = $results->getItems();

        if (empty($events)) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($events as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }

    }

    public function getClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Google Calendar API PHP Quickstart');
        $client->setScopes(Google_Service_Calendar::CALENDAR_READONLY);
        $client->setAuthConfig('Credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        $tokenPath = 'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                define('STDIN', fopen("php://stdin", "r"));
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new \Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($client->getAccessToken()));
        }
        return $client;
    }


    public function cancelAppointment(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());

        $validator = Validator::make($request->all(), [
            'appointment_schedule_id' => 'required|exists:appointment_schedules,id'
        ], [
            'appointment_schedule_id.required' => "Appointment Schedule is Required.",
            'appointment_schedule_id.exists' => "Invalid Appointment Schedule Selected."
        ]);

        if (!$validator->fails()) {
            $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
            $appointmentSchedule->status = 'cancel';
            $appointmentSchedule->save();

            $response['status'] = 'success';
            $response['message'] = 'Appointment Cancel successfully.';
        } else {
            $response['status'] = 'error';
            $response['message'] = "Validation Errors.";
            $response['data'] = $validator->errors()->toArray();
        }

        return $response;
    }





}
