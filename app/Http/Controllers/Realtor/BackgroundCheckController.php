<?php

namespace App\Http\Controllers\Realtor;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\AppointmentSchedule;
use App\Models\BackgroundCheck;
use App\Models\ShortLink;
use App\Models\State;
use App\Models\User;
use App\Traits\GeneralHelperTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BackgroundCheckController extends Controller
{

    use GeneralHelperTrait;

    public function getCode($code)
    {
        $url = ShortLink::where('code', $code)->first()->link;
        return redirect()->to($url);
    }

    public function appointmentCheck($appointment_schedule_id, $user_id)
    {

        $user = User::where('id', $user_id)->first();
        Auth::login($user);
        $getAppoitment = AppointmentSchedule::with(['appointment', 'emergencyContacts', 'backgroundCheck'])->where('id', $appointment_schedule_id)->first();

        $states = State::orderBy('name', 'asc')->get();
        $cities = DB::table('cities')->where('id', (int)$getAppoitment->appointment->property_city)->get();

        return view('realtor.appointments.appointment')->with([
            'getAppointment' => $getAppoitment,
            'user' => $user,
            'states' => $states,
            'cities' => $cities,

        ]);
    }

    public function storeBackgroundCheck(Request $request, $appointmen_id)
    {
        $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
        $appointment = Appointment::where('id', $appointmen_id)->first();

        if ($request->bg_status == 'rejected') {
            $appointment->bg_status = 'rejected';
        }
        if ($request->bg_status == 'accepted') {
            $appointment->bg_status = 'accepted';
        }
        $appointment->cl_first_name = $request->cl_first_name;
        $appointment->cl_last_name = $request->cl_last_name;
        $appointment->cl_dob = $request->cl_dob;
        $appointment->cl_doc_id_number = $request->cl_doc_id_number;
        $appointment->cl_doc_id_state = $request->cl_doc_id_state;
        $appointment->cl_email = $request->cl_email;
        $appointment->cl_mobile_number = $request->cl_mobile_number;
        $appointment->property_address_one = $request->property_address_one;
        $appointment->property_address_two = $request->property_address_two;
        $appointment->property_city = $request->property_city;
        $appointment->property_state = $request->property_state;
        $appointment->property_zip_code = $request->property_zip_code;
        if ($request->has('upload_doc')) {
            $image = $request->file('upload_doc');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/appointmentImage');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = 'default.png';
        }
        $appointment->image = $profile_image;
        $appointment->save();
        if ($appointmentSchedule->background_check == "1") {

            if (!is_null($request->final_firstname)) {
                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
                if (!is_null($checkRecordBg)) {
                    $backgroundCheck = $checkRecordBg;
                } else {
                    $backgroundCheck = new BackgroundCheck();
                }
                $backgroundCheck->appointment_schedule_id = $appointmentSchedule->id;
                $backgroundCheck->first_name = $request->final_firstname;
                $backgroundCheck->last_name = $request->final_lastname;
                $backgroundCheck->ssn = $request->final_ssn;
                $backgroundCheck->dob = $request->final_dob;
                $backgroundCheck->city = $request->final_city;
                $backgroundCheck->state = $request->final_state;
                $backgroundCheck->save();
            }
        }


        return redirect()->route('index')->with(['flash_status' => "success", 'flash_message' => "Appointment Updated Successfully!"]);

    }
}
