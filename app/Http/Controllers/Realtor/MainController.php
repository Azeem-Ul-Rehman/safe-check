<?php

namespace App\Http\Controllers\Realtor;

use App\Http\Controllers\Controller;
use App\Models\Appointment;
use App\Models\AppointmentSchedule;
use App\Models\BackgroundCheck;
use App\Models\Notification;
use App\Models\Package;
use App\Models\State;
use App\Models\Subscription;
use App\Models\SubscriptionHistory;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:realtor');
    }

    public function index()
    {
        $user = Auth::user();
        $total_appointments = AppointmentSchedule::where('realtor_id', $user->realtor->id)->count();

        $appointments = AppointmentSchedule::where('realtor_id', $user->realtor->id)->orderBy('id', 'desc')->with(['appointment', 'emergencyContacts'])->get();
        return view('realtor.dashboard.index', compact('total_appointments', 'appointments'));
    }

    public function subscribeHistory()
    {
        $user = Auth::user();


        $subscriptionHistories = SubscriptionHistory::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->get();
        $subscriptions = Subscription::orderBy('id', 'DESC')->where('realtor_id', $user->realtor->id)->get();


        return view('realtor.subscriptions.index', compact('subscriptions', 'subscriptionHistories'));
    }

    public function subcribePackage()
    {
        $textPage = 'subscribe';
        $packages = Package::all();
        $user = Auth::user();
        $messageShow = true;
        $checkSubcriptionvalue = Subscription::where('realtor_id', $user->realtor->id)->where('package_id', '!=', 1)
            ->whereDate('expiry_date', '>=', Carbon::now())->first();
        if (isset($checkSubcriptionvalue)) {
            $messageShow = true;
        } else {
            $messageShow = false;
        }

        return view('frontend.pages.subscription', compact('user', 'packages', 'textPage', 'messageShow'));
    }

    public function notifications()
    {
        $user = Auth::user();
        $notifications = Notification::where('type_id', $user->realtor->id)->get();
        return view('realtor.notifications.index', compact('notifications'));

    }

    public function appointmentCheck($appointment_schedule_id, $user_id)
    {
        Auth::loginUsingId($user_id);
        $user = Auth::user();
        $getAppoitment = AppointmentSchedule::with(['appointment', 'emergencyContacts', 'backgroundCheck'])->where('id', $appointment_schedule_id)->first();

        $states = State::orderBy('name', 'asc')->get();
        $cities = DB::table('cities')->where('id', (int)$getAppoitment->appointment->property_city)->get();

        return view('realtor.appointments.appointment')->with([
            'getAppointment' => $getAppoitment,
            'user' => $user,
            'states' => $states,
            'cities' => $cities,

        ]);
    }

    public function storeBackgroundCheck(Request $request, $appointmen_id)
    {
        $appointmentSchedule = AppointmentSchedule::where('id', $request->appointment_schedule_id)->first();
        $appointment = Appointment::where('id', $appointmen_id)->first();

        if($request->bg_status == 'rejected'){
            $appointment->bg_status = 'rejected';
        }
        if($request->bg_status == 'accepted'){
            $appointment->bg_status = 'accepted';
        }
        $appointment->save();
        if ($appointmentSchedule->background_check == "1") {

            if (!is_null($request->final_firstname)) {
                $checkRecordBg = BackgroundCheck::where('ssn', $request->final_ssn)->first();
                if (!is_null($checkRecordBg)) {
                    $backgroundCheck = $checkRecordBg;
                } else {
                    $backgroundCheck = new BackgroundCheck();
                }
                $backgroundCheck->appointment_schedule_id = $appointmentSchedule->id;
                $backgroundCheck->first_name = $request->final_firstname;
                $backgroundCheck->last_name = $request->final_lastname;
                $backgroundCheck->ssn = $request->final_ssn;
                $backgroundCheck->dob = $request->final_dob;
                $backgroundCheck->city = $request->final_city;
                $backgroundCheck->state = $request->final_state;
                $backgroundCheck->save();
            }
        }

        return redirect()->route('index')->with(['flash_status' => "success", 'flash_message' => "Appointment Updated Successfully!"]);

    }
}
