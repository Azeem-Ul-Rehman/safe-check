<?php

namespace App\Http\Controllers\Realtor;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\State;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:realtor');
    }

    public function index()
    {

        $user = auth()->user();
        return view('realtor.profiles.index', compact('user'));
    }

    public function editProfile()
    {

        $user = auth()->user();
        return view('realtor.profiles.edit-profile', compact('user'));
    }

    public function updateProfile(Request $request)
    {

        $user = User::find(auth()->user()->id);
        $this->validate($request, [
            'mls_id'              => 'required',
            'website'             => 'nullable|string',
            'pin_number'          => 'required|max:4',
            'address_one'         => 'required',
            'address_two'         => 'nullable',
            'zip_code'            => 'nullable',
//            'pin_number'              => ['required', 'max:4',
//                Rule::unique('realtors')->where(function($query) use($user) {
//                    $query->where('user_id','!=',$user->id);
//                })],

//            'twitter_id'          => 'nullable',
//            'facebook_url'        => 'nullable',
//            'paypal_id'           => 'nullable',
//            'instagram_id'        => 'nullable',
        ], [
        ]);


        $user->update([
            'first_name'        => $request->first_name,
            'last_name'         => $request->last_name,
            'username'          => $request->username,
            'email'             => $request->get('email'),
            'phone_number'      => $request->get('phone_number'),
            'emergency_number'  => $request->get('home_number'),
        ]);

        $user->realtor->update([
            'mls_id'        => $request->mls_id,
            'website'       => $request->website,
            'pin_number'    => $request->pin_number,
            'address_one'   => $request->address_one,
            'address_two'   => $request->address_two,
            'zip_code'      => $request->zip_code ,
//            'twitter_id'    => $request->twitter_id,
//            'facebook_url'  => $request->facebook_url,
//            'paypal_id'     => $request->paypal_id,
//            'instagram_id'  => $request->instagram_id,
        ]);



        return redirect()->route('realtor.profile.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Profile updated successfully.'
            ]);
    }

    public function editProfileImage(Request $request)
    {
        $response = array('status' => '', 'message' => "", 'data' => array());
        $user = User::where('id', $request->user_id)->first();

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/user_profiles');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image = $name;
        } else {
            $profile_image = $user->profile_pic;
        }
        $user->update([
            'profile_pic' => $profile_image,
        ]);

        $response['status'] = 'success';
        $response['message'] = 'Profile Image updated successfully.';

        return $response;


    }

    public function changePassword()
    {

        $user = auth()->user();

        return view('realtor.profiles.change-password', compact('user'));
    }

    public function changeMobileNumber()
    {

        $user = auth()->user();
        return view('realtor.profiles.change-mobile-number', compact('user'));
    }

    public function updateMobileNumber(Request $request)
    {
        $this->validate($request, [
            'phone_number' => 'required',
        ], [
            'phone_number.required' => 'Mobile Number is required.',

        ]);

        $user = User::find(auth()->user()->id);

        $user->update([
            'phone_number' => $request->get('phone_number'),
        ]);

        return redirect()->route('realtor.profile.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Mobile Number updated successfully.'
            ]);
    }

    public function updatePassword(Request $request)
    {
        if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6',
        ]);

        if (strcmp($request->get('new-password-confirm'), $request->get('new-password')) == 0) {
            //Change Password
            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return redirect()->route('realtor.profile.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Password Changed successfully.'
                ]);

        } else {
            return redirect()->back()->with("error", "New Password must be same as your confirm password.");
        }


    }
}
