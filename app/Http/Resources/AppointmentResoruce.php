<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppointmentResoruce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        if ((!is_null($this->appointment->image))) {
            $profile_pic = $this->appointment->image;
        } else {
            $profile_pic = 'default.png';
        }

        return [
            'appointment_schedule_id'       => $this->id,
            'background_check'              => $this->background_check,
            'cl_id'                         => $this->appointment->cl_id ,
            'cl_first_name'                 => $this->appointment->cl_first_name,
            'cl_last_name'                  => $this->appointment->cl_last_name,
            'cl_doc_id_number'              => $this->appointment->cl_doc_id_number,
            'cl_doc_id_state'               => $this->appointment->cl_doc_id_state,
            'cl_email'                      => $this->appointment->cl_email,
            'cl_dob'                        => $this->appointment->cl_dob ? date('m/d/Y',strtotime($this->appointment->cl_dob)) : '',
            "image"                         => asset("/uploads/appointmentImage/" . $profile_pic),
            'cl_mobile_number'              => $this->appointment->cl_mobile_number,
            'property_address_one'          => $this->appointment->property_address_one,
            'property_address_two'          => $this->appointment->property_address_two,
            'property_city_id'              => $this->appointment->property_city,
            'property_city'                 => !is_null($this->appointment->city) ? $this->appointment->city->name : 'No City Found',
            'property_state_id'             => $this->appointment->property_state,
            'property_state'                => !is_null($this->appointment->state) ? $this->appointment->state->name : 'No State Found',
            'property_zip_code'             => $this->appointment->property_zip_code,
            'pin_number'                    => $this->pin_number,
            'local_police_number'           => $this->local_police_number,
            'start_date'                    => $this->start_date,
            'status'                        => $this->status,
            'bg_status'                     => $this->appointment->bg_status,
            'start_time_timezone'           => \Carbon\Carbon::parse(strtotime($this->start_time))->setTimezone($this->time_zone)->format('h:i A'),
            'end_time_timezone'             => \Carbon\Carbon::parse(strtotime($this->end_time))->setTimezone($this->time_zone)->format('h:i A'),
            'actual_start_time_timezone'    => \Carbon\Carbon::parse(strtotime($this->actual_start_time))->setTimezone($this->time_zone)->format('h:i A'),
            'actual_end_time_timezone'      => \Carbon\Carbon::parse(strtotime($this->actual_end_time))->setTimezone($this->time_zone)->format('h:i A'),
            'start_time'                    => date('h:i A', strtotime($this->start_time)),
            'end_time'                      => date('h:i A', strtotime($this->end_time)),
            'actual_start_time'             => date('h:i A', strtotime($this->actual_start_time)),
            'actual_end_time'               => date('h:i A', strtotime($this->actual_end_time)),
            'time_zone'                     => $this->time_zone,
            'emergency_contacts'            => EmergencyContactsResoruce::collection($this->emergencyContacts),

        ];
    }
}
