<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $table = "appointments";
    protected $guarded = [];


    public function realtors()
    {
        return $this->belongsToMany(Realtor::class, 'appointment_schedules', 'realtor_id', 'appointment_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'property_state', 'id');
    }

    public function city()
    {
        return  $this->belongsTo(City::class, 'property_city', 'id');
    }


}
