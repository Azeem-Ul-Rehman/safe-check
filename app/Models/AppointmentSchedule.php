<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppointmentSchedule extends Model
{
    protected $table = "appointment_schedules";
    protected $guarded = [];


    public function realtor()
    {
        return $this->belongsTo(Realtor::class, 'realtor_id', 'id');
    }

    public function appointment()
    {
        return $this->belongsTo(Appointment::class, 'appointment_id', 'id');
    }

    public function emergencyContacts()
    {
        return $this->hasMany(EmergencyContact::class, 'realtor_id', 'id');
    }

    public function backgroundCheck()
    {
        return $this->hasOne(BackgroundCheck::class, 'appointment_schedule_id', 'id');
    }


}
