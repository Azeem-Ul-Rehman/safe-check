<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackgroundCheck extends Model
{
    protected $table = "background_check_records";
    protected $guarded = [];

    public function appointment()
    {
        return $this->belongsTo(AppointmentSchedule::class, 'appointment_schedule_id', 'id');
    }
}
