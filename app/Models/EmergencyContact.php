<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmergencyContact extends Model
{
    protected $table = "emergency_contacts";
    protected $guarded = [];

    public function appointmentSchedule()
    {
        return $this->belongsTo(AppointmentSchedule::class, 'realtor_id', 'id');
    }
}
