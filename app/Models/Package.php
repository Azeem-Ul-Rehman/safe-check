<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table = "packages";
    protected $guarded = [];

    public function realtors(){
        return $this->belongsToMany(Realtor::class, 'subscriptions', 'realtor_id','package_id');
    }

    public function realtorSubcriptionHistory(){
        return $this->belongsToMany(Realtor::class, 'subscription_histories', 'realtor_id','package_id');
    }
}
