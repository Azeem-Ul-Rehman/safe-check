<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Realtor extends Model
{
    protected $table = 'realtors';
    protected $guarded = [];
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function appointments()
    {
        return $this->belongsToMany(Appointment::class, 'appointment_schedules', 'appointment_id', 'realtor_id');
    }

    public function packages()
    {
        return $this->hasOne(Package::class, 'subscriptions', 'package_id', 'realtor_id');
    }

    public function packageSubcriptionHistory()
    {
        return $this->belongsToMany(Package::class, 'subscription_histories', 'package_id', 'realtor_id');
    }

}
