<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table = 'states';
    protected $guarded = [];

    public function appointment(){
        return $this->belongsToMany(Appointment::class,'property_state','id');
    }

}
