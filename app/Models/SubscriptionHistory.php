<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionHistory extends Model
{
    protected $table = "subscription_histories";
    protected $guarded = [];

    public function realtor()
    {
        return $this->belongsTo(Realtor::class, 'realtor_id', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }
}
