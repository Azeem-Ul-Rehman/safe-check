<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;


trait GeneralHelperTrait
{
    public function orderNumber(): string
    {

        $order_number = DB::table('order_numbers')->first();
        if (!empty($order_number) && !is_null($order_number)) {
            if ($order_number->code == '9999') {

                $last_ch = ord($order_number->alpha);
                $last_ch = $last_ch + 1;
                $new_character = chr($last_ch);

                if (($last_ch >= 65 && $last_ch <= 90) || ($last_ch >= 97 && $last_ch <= 122)) {
                    DB::table('order_numbers')
                        ->insert([
                            'slug' => 'Q-',
                            'alpha' => $new_character,
                            'code' => '0001'
                        ]);
                }

                $new_order_number = 'Q-' . $new_character . '0001';
            } else {
                $existing_order_number = DB::table('order_numbers')
                    ->where('alpha', $order_number->alpha)
                    ->first();

                if ($existing_order_number) {
                    $new_order_number = $existing_order_number->slug . $existing_order_number->alpha . $existing_order_number->code;
                    DB::table('order_numbers')
                        ->update([
                            'slug' => 'Q-',
                            'alpha' => $order_number->alpha,
                            'code' => $this->code($existing_order_number->code)
                        ]);

                } else {
                    DB::table('order_numbers')
                        ->insert([
                            'slug' => 'Q-',
                            'alpha' => $order_number->alpha,
                            'code' => '0001'
                        ]);
                    $new_order_number = 'Q-' . $order_number->alpha . '0001';
                }
            }
        } else {
            DB::table('order_numbers')
                ->insert([
                    'slug' => 'Q-',
                    'alpha' => 'A',
                    'code' => '0001'
                ]);
            $new_order_number = 'Q-' . 'A' . '0001';
        }


        return $new_order_number;
    }

    public function code(string $code)
    {
        $convert_into_int = (int)$code;
        $incremented_int = $convert_into_int + 1;
        $integer_length = (int)log10($incremented_int) + 1;

        if ($integer_length === 1) {
            $new_code = '000' . $incremented_int;
        } elseif ($integer_length === 2) {
            $new_code = '00' . $incremented_int;
        } elseif ($integer_length === 3) {
            $new_code = '0' . $incremented_int;
        } else {
            $new_code = $incremented_int;
        }

        return $new_code;
    }

    public function xmlRequest($firstName, $lastName, $dateOfBirth)
    {
        try {
//            <searchParams>
//                             <firstName>John</firstName>
//                             <lastName>Doe</lastName>
//                             <state></state>
//                             <dob>01/01/1901</dob>
//                         </searchParams>

            $xmlString = '<FDSRequest>
                         <username>safechexxml</username>
                         <password>x1845dx</password>
                         <sType>CRMUG</sType>
                         <detail>1</detail>
                         <testmode>true</testmode>
                         <searchParams>
                             <firstName>' . $firstName . '</firstName>
                             <lastName>' . $lastName . '</lastName>
                             <state></state>
                             <dob>' . $dateOfBirth . '</dob>
                         </searchParams>
                    </FDSRequest>';

            $url = "http://www.nationalpublicdata.com/feeds/FDSFeed.cfm";
            //setting the curl parameters.
            $ch = curl_init();
            $data['xml'] = $xmlString;

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


            $data = curl_exec($ch);

            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);


            if (!curl_errno($ch)) {
                $info = curl_getinfo($ch);

            }

            curl_close($ch);


//            print_r('<pre>');
//            print_r($data);
//            print_r('</pre>');
//            return $data;


            $xmlNode = simplexml_load_string($data);
            $arrayData = $this->xmlToArray($xmlNode);
            $finalData = json_encode($arrayData, true);
            $newData = json_decode($finalData, true);
            return $newData['FDSResponse'];

        } catch (\Exception $e) {
//            dd($e);
            return $e->getMessage();
        }
    }

    public function xmlToArray($xml, $options = array())
    {
        $defaults = array(
            'namespaceSeparator' => ':',//you may want this to be something other than a colon
            'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
            'alwaysArray' => array(),   //array of xml tag names which should always become arrays
            'autoArray' => true,        //only create arrays for tags which appear more than once
            'textContent' => '$',       //key used for the text content of elements
            'autoText' => true,         //skip textContent key if node has no attributes or child nodes
            'keySearch' => false,       //optional search and replace on tag and attribute names
            'keyReplace' => false       //replace values for above search values (as passed to str_replace())
        );
        $options = array_merge($defaults, $options);
        $namespaces = $xml->getDocNamespaces();
        $namespaces[''] = null; //add base (empty) namespace

        //get attributes from all namespaces
        $attributesArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
                //replace characters in attribute name
                if ($options['keySearch']) $attributeName =
                    str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
                $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
                $attributesArray[$attributeKey] = (string)$attribute;
            }
        }

        //get child nodes from all namespaces
        $tagsArray = array();
        foreach ($namespaces as $prefix => $namespace) {
            foreach ($xml->children($namespace) as $childXml) {
                //recurse into child nodes
                $childArray = $this->xmlToArray($childXml, $options);
//                list($childTagName, $childProperties) = each($childArray);

                foreach ($childArray as $childTagName => $childProperties) {
                    //replace characters in tag name
                    if ($options['keySearch']) $childTagName =
                        str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
                    //add namespace prefix, if any
                    if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

                    if (!isset($tagsArray[$childTagName])) {
                        //only entry with this key
                        //test if tags of this type should always be arrays, no matter the element count
                        $tagsArray[$childTagName] =
                            in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                                ? array($childProperties) : $childProperties;
                    } elseif (
                        is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                        === range(0, count($tagsArray[$childTagName]) - 1)
                    ) {
                        //key already exists and is integer indexed array
                        $tagsArray[$childTagName][] = $childProperties;
                    } else {
                        //key exists so convert to integer indexed array with previous value in position 0
                        $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
                    }
                }
            }
        }

        //get text content of node
        $textContentArray = array();
        $plainText = trim((string)$xml);
        if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

        //stick it all together
        $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

        //return node as array
        return array(
            $xml->getName() => $propertiesArray
        );
    }

    public function sendSms($phoneNumber, $message)
    {
        $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        $appSid = config('app.twilio')['TWILIO_APP_SID'];
        $client = new Client($accountSid, $authToken);
        try {
            // Use the client to do fun stuff like send text messages!
            $client->messages->create(
            // the number you'd like to send the message to
                $phoneNumber,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+17069608295',
                    // the body of the text message you'd like to send
                    'body' => $message
                )
            );
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
    }

    public function timeZoneSet($offset)
    {
        // This is just an example. In application this will come from Javascript (via an AJAX or something)
        $timezone_offset_minutes = $offset;  // $_GET['timezone_offset_minutes']

        // Convert minutes to seconds
        $timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes * 60, false);

        // Asia/Kolkata
        return $timezone_name;
    }
}
