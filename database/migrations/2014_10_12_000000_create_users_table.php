<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable(); //required
            $table->string('last_name')->nullable();  //required
            $table->string('email')->unique()->nullable();  //required
            $table->string('username')->nullable(); //required
            $table->string('password')->nullable();  //required
            $table->boolean('activated')->default(false);
            $table->integer('role_id')->nullable(); //required
            $table->string('phone_number')->nullable();  //required
            $table->string('emergency_number')->nullable();
            $table->string('otp_code')->nullable();
            $table->enum('user_type', ['admin', 'realtor'])->nullable();
            $table->enum('status', ['pending', 'suspended', 'verified'])->default('pending');
            $table->enum('otp_status', ['sent','verified'])->default('sent');
            $table->timestamp('email_verified_at')->nullable();
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
