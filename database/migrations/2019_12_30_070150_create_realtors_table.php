<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealtorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realtors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('city_id')->nullable();
            $table->unsignedInteger('area_id')->nullable();
            $table->text('mls_id')->nullable();
            $table->text('website')->nullable();
            $table->string('pin_number')->nullable();
            $table->longText('address_one')->nullable();
            $table->longText('address_two')->nullable();
            $table->longText('zip_code')->nullable();
            $table->longText('twitter_id')->nullable();
            $table->longText('facebook_url')->nullable();
            $table->longText('paypal_id')->nullable();
            $table->longText('instagram_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realtors');
    }
}
