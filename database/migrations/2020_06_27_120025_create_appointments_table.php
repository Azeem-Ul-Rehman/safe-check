<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cl_id')->unique();
            $table->text('cl_first_name')->nullable();
            $table->text('cl_last_name')->nullable();
            $table->text('cl_doc_id_number')->nullable(); //licenese number or id card number
            $table->text('cl_doc_id_state')->nullable();
            $table->text('cl_email')->nullable();
            $table->text('cl_mobile_number')->nullable();
            $table->longText('property_address_one')->nullable();
            $table->longText('property_address_two')->nullable();
            $table->text('property_city')->nullable();
            $table->text('property_state')->nullable();
            $table->text('property_zip_code')->nullable();
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
