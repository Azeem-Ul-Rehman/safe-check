<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', ['appointment', 'realtor']);
            $table->unsignedInteger('type_id');
            $table->unsignedInteger('data_id');
            $table->boolean('sms_sent')->default(false);
            $table->boolean('email_sent')->default(false);
            $table->boolean('reject_sent')->default(false);
            $table->boolean('schedule_sent')->default(false);
            $table->longText('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
