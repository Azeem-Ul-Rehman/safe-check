<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackgroundCheckRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('background_check_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('appointment_schedule_id');
            $table->text('first_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('dob')->nullable();
            $table->text('city')->nullable();
            $table->text('state')->nullable();
            $table->text('ssn')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('background_check_records');
    }
}
