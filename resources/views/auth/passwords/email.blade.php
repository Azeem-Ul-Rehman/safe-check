@extends('frontend.layout.master')
@section('title','Forgot Password')

@push('css')
    <style>
        .input-codes {
            width: 24%;
            height: 37px;
            text-align: center;
            font-weight: 500;
            font-size: 22px;
            border: 2px solid #000;
        }
    </style>
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Reset Password</h3>
            </div>
        </div>
    </div>
    <div class="loginArea registerArea">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-5 col-xs-12 loginImg">
                    <img src="{{ asset('frontend/images/phone.png') }}" alt="">
                </div>
                <div class="col-md-6 col-sm-7 col-xs-12">
                    <h2 class="heading">forgot <span>p</span>assword</h2>
                    <div class="formArea loginFormArea">
                        <form method="POST" action="javascript:void(0)" id="forget-password">
                            @csrf

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" value="{{ old('email') }}" required placeholder="Email*"
                                               autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btnMain">{{ __('Send') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form method="POST" action="javascript:void(0)" id="otp-send" style="display: none">
                            <p id="opt_code_text"></p>
                            <input type="hidden" value="0" name="email" id="email_address">
                            @csrf

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group OTP-Codes">
                                        <input type="text" class="input-codes" id="otp_code1" name="otp_code1"/>
                                        <input type="text" class="input-codes" id="otp_code2" name="otp_code2"/>
                                        <input type="text" class="input-codes" id="otp_code3" name="otp_code3"/>
                                        <input type="text" class="input-codes" id="otp_code4" name="otp_code4"/>
                                        <input id="otp_code" type="hidden"
                                               class="form-control @error('otp_code') is-invalid @enderror"
                                               name="otp_code" value="{{ old('otp_code') }}" required
                                               autocomplete="otp_code" autofocus>
                                        @error('otp_code')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btnMain">{{ __('Verify') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form method="POST" action="javascript:void(0)" id="password-reset" style="display: none">
                            @csrf
                            <div class="form-group ">
                                <input type="hidden" value="" name="email" id="mobile_number_forget">
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="Password*"
                                               autocomplete="new-password">
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control" placeholder="Confirm Password*"
                                               name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btnMain">{{ __('Reset Password') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@push('js')
    <script>

        $(function () {
            'use strict';

            var body = $('body');

            function goToNextInput(e) {
                var key = e.which,
                    t = $(e.target),
                    sib = t.next('input');
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((e.which < 48 || e.which > 57)) {
                    e.preventDefault();
                    return false;
                }

                sib.select().focus();
            }


            function onFocus(e) {
                $(e.target).select();
            }

            body.on('keypress keyup blur', 'input[name=otp_code1]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code2]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code3]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code4]', goToNextInput);
            body.on('click', 'input', onFocus);

        });


        $('#forget-password').submit(function (e) {
            $('#email_address').val($('#email').val());
            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('otp.send') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    toastr['success'](response.message)


                    $('#opt_code_text').text(response.data.otp);


                    $('#forget-password').css('display', 'none');
                    $('#otp-send').css('display', 'block');

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });

        $('#otp-send').submit(function (e) {
            $('#mobile_number_forget').val($('#email_address').val());

            $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('verify.otp') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.status == 'success') {
                        $('#forget-password').css('display', 'none');
                        $('#otp-send').css('display', 'none');
                        $('#password-reset').css('display', 'block');
                    } else {
                        toastr['error'](response.message);
                    }


                },
                error: function (e) {

                    toastr['error'](e.responseJSON.error.otp_code);
                }
            });

        });

        $('#password-reset').submit(function (e) {
            $('#mobile_number_forget').val($('#email_address').val());

            $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('otp.password.reset') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (response.status == 'success') {
                        toastr['success'](response.message);
                        setTimeout(function () {
                            window.location.href = '{{ route('login') }}';
                        }, 2000)
                    } else {
                        toastr[response.status](response.message);
                    }


                },
                error: function (e) {
                    $.each(e.responseJSON.error, function (index, object) {
                        toastr['error'](object);
                    });


                }
            });

        });
    </script>
@endpush
