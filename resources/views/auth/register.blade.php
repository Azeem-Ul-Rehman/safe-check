@extends('frontend.layout.master')
@section('title','Register')

@push('css')
    <style>
        .pb-14 {
            padding-bottom: 14px;
        }

        .width-100 {
            width: 100%;
        }

        .select2-container--default .select2-selection--single {

            width: 100% !important;
            height: 36px !important;
            border: #ff6c2b solid 2px !important;
            background: transparent !important;
        }
    </style>
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Sign Up</h3>
            </div>
        </div>
    </div>
    <div class="loginArea registerArea">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-5 col-xs-12 loginImg">
                    <img src="{{ asset('frontend/images/phone.png') }}" alt="">
                </div>
                <div class="col-md-6 col-sm-7 col-xs-12">
                    <h2 class="heading">create a<span>n a</span>ccount</h2>
                    <div class="formArea loginFormArea">
                        <form action="{{ route('register') }}" method="POST" id="register_user">
                            @csrf

                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="first_name" type="text"
                                               class="form-control @error('first_name') is-invalid @enderror"
                                               name="first_name" value="{{ old('first_name') }}"
                                               autocomplete="first_name" autofocus placeholder="First Name*">

                                        @error('first_name')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="last_name" type="text"
                                               class="form-control @error('last_name') is-invalid @enderror"
                                               name="last_name" value="{{ old('last_name') }}"
                                               autocomplete="last_name" autofocus placeholder="Last Name*">

                                        @error('last_name')
                                        <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="email" type="email"
                                               class="form-control @error('email') is-invalid @enderror"
                                               name="email" placeholder="Email*"
                                               value="{{ old('email') }}" autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="username" type="text"
                                               class="form-control @error('username') is-invalid @enderror"
                                               name="username"
                                               value="{{ old('username') }}" autocomplete="username"
                                               placeholder="Username*">

                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="home_number" type="text"
                                               class="form-control @error('home_number') is-invalid @enderror"
                                               name="home_number" value="{{ old('home_number') }}"
                                               autocomplete="home_number" autofocus placeholder="Home Number"
                                               onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">

                                        @error('home_number')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="phone_number" type="text"
                                               class="form-control @error('phone_number') is-invalid @enderror"
                                               name="phone_number" value="{{ old('phone_number') }}"
                                               autocomplete="phone_number" autofocus placeholder="Enter Mobile Number"
                                               onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">

                                        @error('phone_number')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="password" type="password"
                                               class="form-control @error('password') is-invalid @enderror"
                                               name="password"
                                               placeholder="Enter Password*"
                                               autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" autocomplete="new-password"
                                               placeholder="Confirm Password">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <input id="pin_number" type="text"
                                               class="form-control @error('pin_number') is-invalid @enderror"
                                               name="pin_number" value="{{ old('pin_number') }}"
                                               maxlength="4"
                                               autocomplete="pin_number" autofocus placeholder="Pin Number*"
                                               onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">

                                        @error('pin_number')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <p>Already have an account? <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="checkbox" name="terms-and-conditions"
                                               id="terms-and-conditions"
                                               class=" @error('terms-and-conditions') is-invalid @enderror"
                                               {{ old('terms-and-conditions') ? 'checked' : '' }}  value="{{ old('terms-and-conditions') }}">
                                        &nbsp; I agree to the <a href="{{ route('privacy.policy') }}" target="_blank">Terms
                                            & Conditions and Privacy Policy
                                        </a>
                                        <br>
                                        @error('terms-and-conditions')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btnMain">{{ __('Register') }}</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')

    <script>
        $('#terms-and-conditions').on('change',function (){
            if($(this).is(':checked') == true){
                $(this).val(1);
            }else{
                $(this).val(0);
            }
        })
    </script>
@endpush
