@extends('layouts.master')
@section('title','Subscriptions')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Subscriptions
                        </h3>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Realtor Name</th>
                        <th> Package Name</th>
                        <th> Price</th>
                        <th> Background Checks</th>
                        <th> Payment Status</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($subscriptions))
                        @foreach($subscriptions as $subscription)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$subscription->realtor->user->fullName()}}</td>
                                <td>{{$subscription->package->name}}</td>
                                <td>{{$subscription->price}}</td>
                                <td>{{$subscription->background_checks}}</td>
                                <td>{{ucwords($subscription->status)}}</td>
                                <td nowrap>
                                    <a href="{{route('admin.subscriptions.edit',$subscription->id)}}" class="btn btn-sm btn-info pull-left ">Edit</a>

                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                { orderable: false, targets: [2] }
            ],
        });
    </script>
@endpush
