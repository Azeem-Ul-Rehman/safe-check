@php
    $settings =\App\Models\Setting::all();
@endphp

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <p>© Safechex.com {{date('Y')}}. All Rights Reserved | <a href="{{ route('privacy.policy') }}">Privacy Policy</a> | <a href="{{ route('terms-and-conditions') }}">Terms & Conditions</a></p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 footerSocial">
                <ul class="list-unstyled list-inline">
                    <li>
                        <a href="{{$settings[8]->value}}"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li>
                        <a href="{{$settings[9]->value}}"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li>
                        <a href="{{$settings[10]->value}}"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="{{$settings[11]->value}}"><i class="fab fa-google-plus-g"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

