@php
    $settings =\App\Models\Setting::all();



@endphp
{{--<div class="topbar">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="responsiveLogo">--}}
{{--                <a href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}" alt=""></a>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 col-sm-6 col-xs-7 topbarLeft">--}}
{{--                <ul class="list-unstyled list-inline">--}}
{{--                    <li><a href="mailto:{{$settings[1]->value}}"><i class="fa fa-envelope"></i> {{$settings[1]->value}}--}}
{{--                        </a></li>--}}
{{--                    <li><a href="tel:{{$settings[0]->value}}"><i class="fa fa-phone"></i> {{$settings[0]->value}}</a>--}}
{{--                    </li>--}}
{{--                    <li><a href="{{$settings[8]->value}}" target="_blank"><i class="fab fa-whatsapp"></i></a></li>--}}
{{--                    <li><a href="{{$settings[9]->value}}" target="_blank"><i class="fab fa-instagram"></i></a></li>--}}
{{--                    <li><a href="{{$settings[10]->value}}" target="_blank"><i class="fab fa-facebook"></i></a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--            @guest--}}
{{--                <div class="col-md-6 col-sm-6 col-xs-5 topbarRight  hidden-xs">--}}
{{--                    <a href="{{ route('login') }}" class="btn buttonMain hvr-bounce-to-right">Login</a>--}}
{{--                    <a href="{{ route('register') }}" class="btn buttonMain hvr-bounce-to-right">Register</a>--}}
{{--                </div>--}}
{{--            @endguest--}}

{{--            @auth--}}
{{--                <div class="col-md-6 col-sm-6 col-xs-5 topbarRight  hidden-xs">--}}

{{--                    <!-- Example single danger button -->--}}
{{--                    <div class="btn-group">--}}
{{--                        <button type="button" class="btn buttonMain hvr-bounce-to-right dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                            Welcome {{ auth()->user()->first_name }}    <span class="caret"></span>--}}
{{--                        </button>--}}

{{--                        <div class="dropdown-menu">--}}
{{--                            @if(auth()->user()->hasRole('admin'))--}}
{{--                                <li><a class="dropdown-item" href="{{ route('admin.dashboard.index') }}">Dashboard</a></li>--}}
{{--                            @elseif(auth()->user()->hasRole('realtor'))--}}
{{--                                <li><a class="dropdown-item" href="{{ route('realtor.dashboard.index') }}">Dashboard</a></li>--}}
{{--                            @endif--}}
{{--                            <div class="dropdown-divider"></div>--}}
{{--                                <li><a href="{{ route('logout') }}" class="dropdown-item"--}}
{{--                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>--}}
{{--                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                                {{ csrf_field() }}--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endauth--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="navbar-fixed-top">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}"
                                                                         alt="safechex"></a></div>
            <div class="collapse navbar-collapse" id="defaultNavbar1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#home">Home</a></li>
                    <li><a href="#about">about us </a></li>
                    <li><a href="#works">how it works</a></li>
                    <li><a href="#contact">contact</a></li>
                    @guest
                        <li><a href="{{ route('login') }}" class="signupBtn">Login</a></li>
                        <li><a href="{{ route('register') }}" class="signupBtn">Sign Up</a></li>
                    @endguest
                    @auth
                        <li>
                            <div class="col-md-6 col-sm-6 col-xs-5 topbarRight  hidden-xs">

                                <!-- Example single danger button -->
                                <div class="btn-group">
                                    <button type="button" class="btn buttonMain hvr-bounce-to-right dropdown-toggle"
                                            style="margin-top: 15px;background-color: transparent"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Welcome {{ strtoupper(auth()->user()->fullName()) }} <span class="caret"></span>
                                    </button>


                                    <div class="dropdown-menu">
                                        <ul class="nav navbar-nav">
                                            @if(auth()->user()->hasRole('admin'))
                                                <li><a class="dropdown-item"
                                                       href="{{ route('admin.dashboard.index') }}">Dashboard</a></li>
                                            @elseif(auth()->user()->hasRole('realtor'))
                                                <li><a class="dropdown-item"
                                                       href="{{ route('realtor.dashboard.index') }}">Dashbaord</a></li>
                                            @endif
                                            <div class="dropdown-divider"></div>
                                            <li><a href="{{ route('logout') }}" class="dropdown-item"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                            </li>
                                        </ul>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </li>

                    @endauth
                </ul>
            </div>
        </div>
    </nav>
</div>
