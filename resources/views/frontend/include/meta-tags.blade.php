@if(is_null($meta_information))
    <title>SAFECHEX | @yield('title')</title>
    <meta name="description"
          content="SECURE SOLUTIONS FOR REALTORS">
    <meta name="keywords" content="safechex">
@else
    <title>SAFECHEX | {{$meta_information->title}}</title>
    <meta name="description" content="{{ $meta_information->description }}">
    <meta name="keywords" content="{{ $meta_information->keywords }}">
@endif


