<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header customLogo">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1">
                <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span
                    class="icon-bar"></span><span class="icon-bar"></span></button>
            {{--            <a class="navbar-brand" href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}"--}}
            {{--                                                                     alt=""></a>--}}
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">
            <ul class="nav navbar-nav">

                <li class="{{ (request()->is('/')) ? 'active' : '' }}"><a href="{{ route('index') }}">Home
                    </a>
                </li>

                <li class="{{ (request()->is('about-us')) ? 'active' : '' }}"><a href="{{ route('aboutus.detail') }}">About
                        Us</a>
                </li>
                <li class="{{ (request()->is('about-us')) ? 'active' : '' }}"><a href="{{ route('aboutus.detail') }}">How
                        it Works</a>
                </li>
                <li class="{{ (request()->is('about-us')) ? 'active' : '' }}"><a href="{{ route('aboutus.detail') }}">Real
                        State Professionals</a>
                </li>
                <li class="{{ (request()->is('contacts')) ? 'active' : '' }} hidden-xs"><a
                        href="{{ route('contacts.create') }}">Contact</a>
                </li>

                @auth

                    @if(auth()->user()->hasRole('admin'))
                        <li class="hidden-lg hidden-md hidden-sm">
                            <a href="{{ route('admin.dashboard.index') }}"
                               class="profileImage"
                               title="{{ auth()->user()->fullName() }}">Dashboard
                            </a>
                        </li>
                    @elseif(auth()->user()->hasRole('realtor'))
                        <li class="hidden-lg hidden-md hidden-sm">
                            <a href="{{ route('realtor.dashboard.index') }}"
                               class="profileImage"
                               title="{{ auth()->user()->fullName() }}">Dashboard
                            </a>
                        </li>
                    @endif
                @endauth


                @auth
                    <li class="hidden-lg hidden-md hidden-sm">
                        <a href="{{ route('logout') }}" class="btn buttonMain hvr-bounce-to-right"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                @endauth


                @guest
                    <li class="hidden-lg hidden-md hidden-sm"><a href="{{ route('login') }}"
                                                                 class=" {{ (request()->is('login')) ? 'active' : '' }}">Login</a>
                    </li>
                    <li class="hidden-lg hidden-md hidden-sm"><a href="{{ route('register') }}"
                                                                 class="{{ (request()->is('register')) ? 'active' : '' }}">Register</a>
                    </li>
                @endguest


            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
