
@extends('frontend.layout.master')
@section('title','About US')
@section('description','SECURE SOLUTIONS FOR REALTORS')
@section('keywords', 'SAFECHEX')
@push('css')
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>About Us</h3>
            </div>
        </div>
    </div>

    <div class="loginArea">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formArea loginFormArea">
                    {!! ($aboutus) ? $aboutus->description : '' !!}

                </div>
            </div>
        </div>
    </div>



@endsection




