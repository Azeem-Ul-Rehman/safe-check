
@extends('frontend.layout.master')
@section('title','Terms And Conditions')
@section('description','SECURE SOLUTIONS FOR REALTORS')
@section('keywords', 'SAFECHEX')
@push('css')
@endpush
@section('content')
    <div class="counter innerBanner">
        <div class="container">
            <div class="row">
                <h3>Terms And Conditions</h3>
            </div>
        </div>
    </div>
    <div class="loginArea">
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="formArea loginFormArea">
                    {!! ($terms) ? $terms->description : '' !!}

                </div>
            </div>
        </div>
    </div>





@endsection







