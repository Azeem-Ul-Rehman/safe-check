@php
    $aboutus =\App\Models\Aboutus::first();

@endphp

@push('css')
    <style>
    </style>
@endpush
<div class="aboutUs" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-5 col-xs-12">

            </div>
            <div class="col-md-6 col-sm-7 col-xs-12">
                <h2 class="heading"><span>ab</span>out us</h2>
                <p>{!! $aboutus->description ?? '--' !!}</p>
            </div>
        </div>
    </div>
</div>
