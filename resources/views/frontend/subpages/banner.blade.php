<div class="banner" id="home">
    <div class="container">
        <div class="row">

            {{--            <div class="col-md-6 col-sm-6 col-xs-12" >--}}
            {{--                <div class="video-container">--}}
            {{--                    <video width="100%" autoplay>--}}
            {{--                        <source src="{{ asset('videos/video.mp4') }}" type="video/mp4">--}}
            {{--                        <source src="{{ asset('videos/video.mp4') }}" type="video/ogg">--}}
            {{--                        Your browser does not support HTML5 video.--}}
            {{--                    </video>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="bannerInner">
                    <h2>SECURE <span>SOLUTIONS</span></h2>
                    <p>For Realtors to provide safety in the field</p>
                    <div class="row">
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <a href="https://apps.apple.com/us/app/safechex/id1549614571"><img src="{{ asset('frontend/images/appStore.png') }}" alt=""></a>
                        </div>
                        <div class="col-md-5 col-sm-6 col-xs-6">
                            <a href="https://play.google.com/store/apps/details?id=com.safechex"><img src="{{ asset('frontend/images/playStore.png') }}" alt=""></a>
                        </div>
                        <br>
                        <div class="col-md-10 col-sm-12 col-xs-12 text-center">
                            <a href="javascript:void(0)" onclick="watchNow()" class="btn btnMain" style="background: #fff;
    color: #000; margin-top: 20px">Watch Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 bannerPhone">
                <img src="{{ asset('frontend/images/phone.png') }}" alt="phone" style="width: 100%">
            </div>

        </div>
    </div>
</div>


{{--<style>--}}
{{--    .video-section {--}}
{{--        padding: 70px 0;--}}
{{--        background: #fff;--}}
{{--    }--}}
{{--</style>--}}
