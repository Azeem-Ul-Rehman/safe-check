<div class="contact" id="contact">
    <div class="container">
        <h2 class="heading">SEND US <span>a</span> message</h2>
        <div class="formArea">
            <form method="post" action="{{ route('contacts.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" class="form-control  @error('full_name') is-invalid @enderror"
                                   placeholder="Full Name" name="full_name" id="full_name"
                                   value="{{ old('full_name') }}" autocomplete="full_name">
                            @error('full_name')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <input id="email" type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Email">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select name="nature_of_contact" id="nature_of_contact"
                                    class="form-control @error('nature_of_contact') is-invalid @enderror">

                                <option value="">Select Nature of Contact</option>
                                <option value="report_an_issue">Report an Issue</option>
                                <option value="feedback">Feedback</option>
                            </select>


                            @error('nature_of_contact')
                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <textarea rows="5" id="message"
                                      class="form-control @error('message') is-invalid @enderror"
                                      name="message" placeholder="Message"
                                      autocomplete="message">{{ old('message') }}</textarea>

                            @error('message')
                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btnMain">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

