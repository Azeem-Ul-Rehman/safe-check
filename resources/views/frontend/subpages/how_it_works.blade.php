<div class="howItWorks" id="works">
    <div class="container">
        <h2 class="heading text-center mb-5">How <span>it</span> works</h2>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="video-container">
                    <video width="100%" autoplay muted controls>
                        <source src="{{ asset('videos/video.mp4') }}" type="video/mp4">
                        <source src="{{ asset('videos/video.mp4') }}" type="video/ogg">
                        Your browser does not support HTML5 video.
                    </video>
                </div>
            </div>
            <div class="col-md- col-sm-6 col-xs-12">
                <img src="{{ asset('frontend/images/video-img.jpg') }}" alt="video-img" style="width: 100%;height: 314px;">
            </div>
        </div>
        <div class="row">

            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="row">
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how1.png') }}" alt="how1">
                        <p>1. Register</p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how2.png') }}" alt="how2">
                        <p>2. Set up Payments </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how3.png') }}" alt="how3">
                        <p>3. Download the app
                            Accept permissions
                            Accept disclaimer
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 text-center howImg howDesktop">
                <img src="{{ asset('frontend/images/howImg.png') }}" alt="howImg">
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="row">
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how4.png') }}" alt="how4">
                        <p>4. Set your
                            appointments
                        </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how5.png') }}" alt="how5">
                        <p>5. Check
                            Background </p>
                    </div>
                    <div class="col-md-12 text-center howItWorksText">
                        <img src="{{ asset('frontend/images/how6.png') }}" alt="how6">
                        <p>6. Secure Your
                            Showings
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 text-center howImg howMobile">
                <img src="{{ asset('frontend/images/howImg.png') }}" alt="howImg">

            </div>
        </div>
    </div>
</div>
<style>
    .video-section {
        padding: 70px 0;
        /*background: #fff;*/
    }
</style>
