<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  "
                aria-haspopup="true"><a href="{{ route('index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-home"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Home</span>
											</span></span></a></li>


            <li class="m-menu__item {{ (request()->is('admin/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>



            <li class="m-menu__item {{ (request()->is('admin/users') || request()->is('admin/users/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.users.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-user-add"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Users</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/cities') || request()->is('admin/cities/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.cities.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-location"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Cities</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/states') || request()->is('admin/states/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.states.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-location"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">States</span>
											</span></span></a></li>


            <li class="m-menu__item  {{ (request()->is('admin/packages') || request()->is('admin/packages/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.packages.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-price-tag"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Packages</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/subscriptions') || request()->is('admin/subscriptions/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.subscriptions.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-price-tag"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Subscriptions</span>
											</span></span></a></li>



         <li class="m-menu__item {{ (request()->is('admin/contacts') || request()->is('admin/contacts/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.contacts.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-bell"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Email Messages</span>
											</span></span></a></li>



            <li class="m-menu__item  {{ (request()->is('admin/aboutus') || request()->is('admin/aboutus/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.aboutus.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-doc"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">About Us</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/privacypolicy') || request()->is('admin/privacypolicy/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.privacypolicy.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-book"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Privacy Policy</span>
											</span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/terms-and-conditions') || request()->is('admin/terms-and-conditions/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.terms-and-conditions.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-book"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Terms and Conditions</span>
											</span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('admin/settings') || request()->is('admin/settings/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.settings.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-settings"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Settings</span>
											</span></span></a></li>
{{--            <li class="m-menu__item  {{ (request()->is('admin/meta-tags') || request()->is('admin/meta-tags/*')) ? 'activeMenu' : '' }} "--}}
{{--                aria-haspopup="true"><a href="{{ route('admin.meta-tags.index') }}" class="m-menu__link "><i--}}
{{--                        class="m-menu__link-icon flaticon-information"></i><span--}}
{{--                        class="m-menu__link-title"> <span--}}
{{--                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Meta Tags (SEO)</span>--}}
{{--											</span></span></a></li>--}}


        </ul>

    </div>

    <!-- END: Aside Menu -->
</div>
