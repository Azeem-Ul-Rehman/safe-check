@extends('realtor.main')
@section('title','Apply Background Check')
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">
@push('css')

    <style>
        /* Absolute Center Spinner */
        .loading {
            position: fixed;
            z-index: 999;
            height: 2em;
            width: 2em;
            overflow: show;
            margin: auto;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        /* Transparent Overlay */
        .loading:before {
            content: '';
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));

            background: -webkit-radial-gradient(rgba(20, 20, 20, .8), rgba(0, 0, 0, .8));
        }

        /* :not(:required) hides these rules from IE9 and below */
        .loading:not(:required) {
            /* hide "loading..." text */
            font: 0/0 a;
            color: transparent;
            text-shadow: none;
            background-color: transparent;
            border: 0;
        }

        .loading:not(:required):after {
            content: '';
            display: block;
            font-size: 10px;
            width: 1em;
            height: 1em;
            margin-top: -0.5em;
            -webkit-animation: spinner 150ms infinite linear;
            -moz-animation: spinner 150ms infinite linear;
            -ms-animation: spinner 150ms infinite linear;
            -o-animation: spinner 150ms infinite linear;
            animation: spinner 150ms infinite linear;
            border-radius: 0.5em;
            -webkit-box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
            box-shadow: rgba(255, 255, 255, 0.75) 1.5em 0 0 0, rgba(255, 255, 255, 0.75) 1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) 0 1.5em 0 0, rgba(255, 255, 255, 0.75) -1.1em 1.1em 0 0, rgba(255, 255, 255, 0.75) -1.5em 0 0 0, rgba(255, 255, 255, 0.75) -1.1em -1.1em 0 0, rgba(255, 255, 255, 0.75) 0 -1.5em 0 0, rgba(255, 255, 255, 0.75) 1.1em -1.1em 0 0;
        }

        /* Animation */

        @-webkit-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes spinner {
            0% {
                -webkit-transform: rotate(0deg);
                -moz-transform: rotate(0deg);
                -ms-transform: rotate(0deg);
                -o-transform: rotate(0deg);
                transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
                -moz-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .select2-container {
            width: 100% !important;
        }

        * {
            margin: 0;
            padding: 0
        }

        html {
            height: 100%
        }

        #grad1 {
            /*border: 3px #000 solid;*/
        }

        #msform {
            text-align: center;
            position: relative;
            margin-top: 20px
        }

        #msform fieldset .form-card {
            background: white;
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            padding: 20px 40px 30px 40px;
            box-sizing: border-box;
            width: 94%;
            margin: 0 3% 20px 3%;
            position: relative
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 0.5rem;
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding-bottom: 20px;
            position: relative
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }

        #msform fieldset .form-card {
            text-align: left;
            color: #9E9E9E
        }

        #msform input,
        #msform select,
        #msform textarea {
            padding: 0px 8px 4px 8px;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 25px;
            margin-top: 2px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 16px;
            letter-spacing: 1px
        }

        #msform input:focus,
        #msform textarea:focus {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            border: none;
            border-bottom: 1px solid #911014;
            outline-width: 0
        }


        #msform .action-button {
            border: #911014 solid 2px;
            color: #fff;
            background: #911014;
            padding: 5px 25px;
            box-sizing: border-box;
            transition-duration: 0.3s;
            border-radius: 0;
            font-size: 16px;
            font-weight: 400;
            text-decoration: none;
            cursor: pointer;
            border-radius: 50px !important;
            width: 20%;
        }


        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
            border-radius: 50px !important;
        }

        #msform .action-button-appointment {
            width: 200px;
            background: skyblue;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }


        select.list-dt {
            border: none;
            outline: 0;
            border-bottom: 1px solid #ccc;
            padding: 2px 5px 3px 5px;
            margin: 2px
        }

        select.list-dt:focus {
            border-bottom: 1px solid #911014;
        }

        .card {
            z-index: 0;
            border: none;
            border-radius: 0.5rem;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #911014;
            margin-bottom: 10px;
            font-weight: bold;
            text-align: left;
            margin-bottom: 20px;
        }

        #progressbar {
            width: 101%;

            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #000000
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 33%;
            float: left;
            position: relative
        }

        #progressbar #account:before {

            /*content: "\f023"*/
            content: "\f007"
        }

        #progressbar #personal:before {

            content: "\f007"
        }

        #progressbar #payment:before {

            content: "\f09d"
        }

        #progressbar #confirm:before {

            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 18px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {

        }

        .radio-group {
            position: relative;
            margin-bottom: 25px
        }

        .radio {
            display: inline-block;
            width: 204px;
            height: 104px;
            border-radius: 0;
            background: lightblue;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            box-sizing: border-box;
            cursor: pointer;
            margin: 8px 2px
        }

        .radio:hover {
            box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
        }

        .radio.selected {
            box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        .pl-125 {
            padding-left: 125px;
            color: #000;
        }

        .pl-155 {
            padding-left: 155px;
            color: #000;
        }

        .pl-159 {
            padding-left: 159px;
            color: #000;
        }

        .pl-115 {
            padding-left: 115px;
            color: #000;
        }

        .pl-110 {
            padding-left: 110px;
            color: #000;
        }

        .pl-91 {
            padding-left: 91px;
            color: #000;
        }

        .float-right {
            float: right;
        }

        #msform .review_items {
            width: 200px;
            background: skyblue;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }

        .hideSpan {
            display: none;
        }

        .colorSpan {
            color: red;
        }

        @media only screen and (max-width: 768px) {
            /* For mobile phones: */
            #msform input[type="button"], #msform input[type="submit"] {
                width: 95%;
            }
        }

    </style>
@endpush
@section('content')

    <div class="loading" style="display: none !important;">Loading&#8230;</div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('Apply Background Check') }}
                        </h3>
                    </div>
                </div>
            </div>

            <!-- MultiStep Form -->
            <div class="container-fluid" id="grad1">
                <div class="row justify-content-center mt-0">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center p-0 mt-3 mb-2 appointmentMain">
                        <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                            <h5><strong>Note: You have utilize one background check after clicking on apply
                                    button</strong></h5>
                            <div class="row">
                                <div class="col-md-12 mx-0">
                                    <form class="m-form" method="post"
                                          action="{{ route('realtor.appointments.store.backgroundCheck' , $getAppointment->appointment->id) }}"
                                          id="msform"
                                          onsubmit="return validate(event)"
                                          enctype="multipart/form-data" role="form">
                                        @csrf
                                        <input type="hidden" name="appointment_schedule_id"
                                               value="{{$getAppointment->id}}">
                                        <input type="hidden" name="bg_status" id="status" value="">
                                        <!-- progressbar -->
                                        <h1></h1>
                                        <div>
                                            <input type="text" name="timeZoneOffset" id="timeZoneOffset" value="">
                                            <div id="hidden-fields">
                                                <input type="hidden" name="final_firstname" id="final-firstname"
                                                       value="">
                                                <input type="hidden" name="final_lastname" id="final-lastname" value="">
                                                <input type="hidden" name="final_ssn" id="final-ssn" value="">
                                                <input type="hidden" name="final_dob" id="final-dob" value="">
                                                <input type="hidden" name="final_city" id="final-city" value="">
                                                <input type="hidden" name="final_state" id="final-state" value="">
                                            </div>
                                            <div class="form-card" style="display: none">
                                                <h5 class="fs-title">Location</h5>
                                                <input type="text" name="property_address_one" id="property_address_one"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->property_address_one)) ?$getAppointment->appointment->property_address_one : '' }}"
                                                       placeholder="Enter Street Address"/>
                                                <span id="property_address_one_message" class="hideSpan colorSpan">Enter Street Address</span>
                                                <input type="text" name="property_address_two" id="property_address_two"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->property_address_two)) ?$getAppointment->appointment->property_address_two : '' }}"
                                                       placeholder="Enter Street Address Two"/>
                                                <span id="property_address_two_message" class="hideSpan colorSpan">Enter Street Address</span>
                                                <div class="row">
                                                    <div class="col-12">

                                                        <select name="property_state" id="property_state"
                                                                class="states list-dt-first" style="width: 100%">
                                                            <option value="">Select State</option>

                                                            @if(!empty($states) && count($states) >0)
                                                                @foreach($states as $state)
                                                                    <option
                                                                        value="{{$state->id}}" {{ ((int)$getAppointment->appointment->property_state == $state->id) ? 'selected' : ''  }}> {{ ucfirst($state->name) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <span id="property_state_message"
                                                              class="hideSpan colorSpan">Enter State</span>
                                                    </div>


                                                </div>
                                                <div class="row">

                                                    <div class="col-12">
                                                        <select name="property_city" id="property_city"
                                                                class="cities list-dt-first" style="width: 100%">
                                                            <option value="">Select City</option>
                                                            @if(!empty($cities) && count($cities) >0)
                                                                @foreach($cities as $city)
                                                                    <option
                                                                        value="{{$city->id}}"
                                                                        selected> {{ ucfirst($city->name) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                        <span id="property_city_message" class="hideSpan colorSpan">Enter Property City</span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-card container">
                                                <input type="text" name="cl_first_name" id="cl_first_name"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_first_name)) ?$getAppointment->appointment->cl_first_name : '' }}"
                                                       placeholder="First Name"/>
                                                <span id="cl_first_name_message" class="hideSpan colorSpan">Please Enter First Name</span>
                                                <input type="text" name="cl_last_name" id="cl_last_name"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_last_name)) ?$getAppointment->appointment->cl_last_name : '' }}"
                                                       placeholder="Last Name"/>
                                                <span id="cl_last_name_message"
                                                      class="hideSpan colorSpan">Please Enter Last Name</span>
                                                <input type="date" name="cl_dob" id="cl_dob"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_dob)) ? $getAppointment->appointment->cl_dob : '' }}"
                                                       placeholder="DOB"/>
                                                <span id="cl_dob_message"
                                                      class="hideSpan colorSpan">Please Enter Date of Birth</span>
                                                <input type="text" name="cl_doc_id_number"
                                                       placeholder="Id Card or License Number" id="cl_doc_id_number"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_doc_id_number)) ?$getAppointment->appointment->cl_doc_id_number : '' }}"/>
                                                <span id="cl_doc_id_number_message" class="hideSpan colorSpan">Please Enter Id Card or License Number</span>


                                                <select name="cl_doc_id_state" id="cl_doc_id_state"
                                                        class="list-dt-first" style="width: 100%">
                                                    <option value="">Select License State</option>

                                                    @if(!empty($states) && count($states) >0)
                                                        @foreach($states as $state)
                                                            <option
                                                                value="{{$state->id}}" {{ ((int)$getAppointment->appointment->cl_doc_id_state == $state->id) ? 'selected' : ''  }}> {{ ucfirst($state->name) }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                {{--                                                <input type="text" name="cl_doc_id_state"--}}
                                                {{--                                                       placeholder="Id Card or License State" id="cl_doc_id_state"--}}
                                                {{--                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_doc_id_state)) ?$getAppointment->appointment->cl_doc_id_state : '' }}"/>--}}

                                                <span id="cl_doc_id_state_message" class="hideSpan colorSpan">Please Enter Id Card or License State</span>

                                                <input type="text" name="cl_mobile_number" placeholder="Mobile number"
                                                       id="cl_mobile_number"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_mobile_number)) ?$getAppointment->appointment->cl_mobile_number : '' }}"/>
                                                <span id="cl_mobile_number_message" class="hideSpan colorSpan">Please Enter Mobile Number</span>
                                                <input type="email" name="cl_email" placeholder="Email Address"
                                                       id="cl_email"
                                                       value="{{(isset($getAppointment) && isset($getAppointment->appointment) && isset($getAppointment->appointment->cl_email)) ?$getAppointment->appointment->cl_email : '' }}"/>
                                                <span id="cl_email_message" class="hideSpan colorSpan">Please Enter Email</span>
                                                <br>
                                                <label for="uplod_doc_message"
                                                       style="font-weight: 700 !important;float: left">Upload
                                                    a Picture of Your Stated Issued ID
                                                    Card Or Drivers License</label>

                                                <div class="input-group">
                                                    <input value="{{old('upload_doc')}}" type="file"
                                                           class="form-control @error('upload_doc') is-invalid @enderror propic"
                                                           id="file" name="upload_doc" onchange="readURLImage(this);">
                                                    <label for="file"><img id="upld"
                                                                           src="{{ asset('frontend/images/uploadbtn.png') }}"
                                                                           alt="Upload Image"></label>
                                                    <img width="300" height="200" class="img-thumbnail"
                                                         style="display:{{($getAppointment->appointment->image) ? 'block' : 'none'}};"
                                                         id="img"
                                                         src="{{ asset('/uploads/appointmentImage/'.$getAppointment->appointment->image) }}"
                                                         alt="your image"/>

                                                </div>
                                                <span id="uplod_doc_message" class="hideSpan colorSpan">Please Enter File</span>

                                                <label style="float: left;margin-top: 15px;"><input type="checkbox"
                                                                                                    name="background_check"
                                                                                                    id="background_check"
                                                                                                    value="1"
                                                                                                    checked
                                                                                                    style="width: auto;"><span> Background Check</span></label>

                                            </div>
                                        </div>
                                        <fieldset id="applyFieldset">

                                            <input type="button" id="applyButton" class="action-button-appointment"
                                                   value="Apply"/>
                                        </fieldset>

                                        <fieldset style="display: none;" id="enableFieldset">
                                            <div class="form-card">
                                                <div id="background-check-information"></div>

                                            </div>

                                            <input type="submit" name="reject"
                                                   class="reject action-button-previous"
                                                   data-status="rejected"
                                                   value="Reject"/>

                                            <input type="submit" name="make_payment" class="action-button-appointment"
                                                   data-status="accepted"
                                                   value="Accept"/>
                                        </fieldset>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Background Check!</h3>
                    <h2 id="response_status">PASSED!</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Background Check Informations</b></h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">


                    {{--First Name--}}
                    <label>
                        <b>Actual First Name: </b>
                        <span id="form-first-name"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search First Name: </b>
                        <span id="bg-first-name"></span>
                    </label>
                    <br>

                    {{--Last Name--}}
                    <label>
                        <b>Actual Last Name: </b>
                        <span id="form-last-name"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search Last Name: </b>
                        <span id="bg-last-name"></span>
                    </label>

                    <br>

                    {{--State--}}
                    <label>
                        <b>Actual State: </b>
                        <span id="form-state"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search State:</b>
                        <span id="bg-state"></span>
                    </label>


                    {{--City--}}
                    <br>
                    <label>
                        <b>Actual City: </b>
                        <span id="form-city"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search City: </b>
                        <span id="bg-city"></span>
                    </label>


                    {{--Address--}}
                    <br>
                    <label>
                        <b>Actual Address: </b>
                        <span id="form-address"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search Address: </b>
                        <span id="bg-address"></span>
                    </label>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>

        var filteredRecord;
        var dateToday = new Date();
        $('#start_date').datepicker({startDate: 'today'});

        $(document).ready(function () {
            var timeZoneOffset = moment().utcOffset();
            // backgroundCheck();
            $('#timeZoneOffset').valueOf(timeZoneOffset);


            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var getValue;

            $(".next").click(function () {

                if ($(this).val() === "1") {
                    getValue = steponevalidation();
                } else if ($(this).val() === "2") {
                    getValue = steptwovalidation();
                }


                debugger;
                if (getValue === false) {

                } else {


                    if ($(this).val() === "1") {
                        backgroundCheck();
                    }
                    if ($(this).val() === "2") {
                        $('#first_name_third_step').text($('#cl_first_name').val());
                        $('#last_name_third_step').text($('#cl_last_name').val());
                        $('#property_address_third_step').text($('#property_address_one').val() + ' ' + $('#property_address_two').val());
                        $('#appointment_date_third_step').text($('#start_date').val());
                        // $('#appointment_duration_third_step').text($('#duration').val() + 'hrs');
                        $('#appointment_time_third_step').text($('#start_time_hour').val() + ':' + $('#start_time_min').val() + '' + $('#start_time_period').val() + ' - ' + $('#end_time_hour').val() + ':' + $('#end_time_min').val() + '' + $('#end_time_period').val());
                    }
                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                    //show the next fieldset
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function (now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({'opacity': opacity});
                        },
                        duration: 600
                    });
                }
            });

            $(".previous").click(function () {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function () {
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });

            $(".submit").click(function () {
                return false;
            });


            var count = 1;

            dynamic_field(count);

            function dynamic_field(number) {
                html = '<div id="emergency_contacts' + number + '">';
                html += '<input type="text" name="name[]" placeholder="Name" required/>';
                html += '<input type="text" name="mobile_number[]" placeholder="Phone Number." required/>';
                if (number > 1) {
                    html += '<button type="button" name="remove" id="" class="btn btn-danger remove">Remove EMERGENCY CONTACT</button></div>';
                    $('#all_contacts').append(html);
                } else {
                    html += '<button type="button" name="add" id="add" class="btn btn-success">ADD EMERGENCY CONTACT</button></div>';
                    $('#all_contacts').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);
            });

            $(document).on('click', '.remove', function () {
                var removecount = count;
                count--;
                $('#emergency_contacts' + removecount + '').remove();
            });

        });

        function readURLImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    if (input.files[0].size > 2097152) {
                        $("#file").val('');
                        toastr['error']("Video size less then 2MB!");
                    } else {
                        $('#img').attr('src', e.target.result);
                        $('#img').css("display", "block");
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function steponevalidation() {

            var check = true;
            if ($("#property_address_one").val() == '') {
                $("#property_address_one_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_address_one_message").addClass('hideSpan');
            }
            // if ($("#property_address_two").val() == '') {
            //     $("#property_address_two_message").removeClass('hideSpan');
            //     check = false;
            // } else {
            //     $("#property_address_two_message").addClass('hideSpan');
            // }
            if ($("#property_city").val() == '') {
                $("#property_city_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_city_message").addClass('hideSpan');
            }
            if ($("#property_state").val() == '') {
                $("#property_state_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_state_message").addClass('hideSpan');
            }

            if ($("#cl_first_name").val() == '') {
                $("#cl_first_name_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_first_name_message").addClass('hideSpan');
            }
            if ($("#cl_last_name").val() == '') {
                $("#cl_last_name_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_last_name_message").addClass('hideSpan');
            }
            if ($("#cl_dob").val() == '') {
                $("#cl_dob_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_dob_message").addClass('hideSpan');
            }

            if ($("#cl_doc_id_number").val() == '') {
                $("#cl_doc_id_number_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_doc_id_number_message").addClass('hideSpan');
            }
            if ($("#cl_doc_id_state").val() == '') {
                $("#cl_doc_id_state_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_doc_id_state_message").addClass('hideSpan');
            }
            if ($("#cl_mobile_number").val() == '') {
                $("#cl_mobile_number_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_mobile_number_message").addClass('hideSpan');
            }
            if ($("#cl_email").val() == '') {
                $("#cl_email_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_email_message").addClass('hideSpan');
            }
            if ($("#uplod_doc").val() == '') {
                $("#uplod_doc_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#uplod_doc_message").addClass('hideSpan');
            }
            return check;
        }

        function steptwovalidation() {
            var check = true;
            if ($("#start_date").val() == '') {
                $("#start_date_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_date_message").addClass('hideSpan');
            }
            if ($("#duration").val() == '') {
                $("#duration_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#duration_message").addClass('hideSpan');
            }
            if ($("#start_time_hour").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }
            if ($("#start_time_min").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }

            if ($("#start_time_period").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }
            if ($("#end_time_hour").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }
            if ($("#end_time_hour").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }
            if ($("#end_time_period").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }

            return check;
        }

        function backgroundCheck() {

            $('.loading').show();
            $('#background_check').val(1);
            $.ajax({
                type: "GET",
                url: "{{ route('ajax.backgroundCheck') }}",
                data: {
                    'backgroundCheck': true,
                    'first_name': $('#cl_last_name').val(),
                    'last_name': $('#cl_first_name').val(),
                    'dob': $('#cl_dob').val()
                },
                success: function (response) {
                    if (response.status === "success") {

                        var data = response.data.backgroundCheckResponse.searchResults.CriminalSearch.Result;
                        if (data.length > 0) {
                            // filteredRecord = data.filter(element => element.lastname == $('#cl_last_name').val() && element.firstname == $('#cl_first_name').val())
                            filteredRecord = data.filter(element => element.lastname == $('#cl_last_name').val() && element.firstname == $('#cl_first_name').val() && element.dob == $('#cl_dob').val())
                            // filteredRecord = data.filter(element => element.lastname == $('#cl_last_name').val() && element.firstname == $('#cl_first_name').val())
                            if (filteredRecord.length > 0) {
                                $('#response_status').text('Passed');

                                var html = '';
                                $.each(filteredRecord, function (key, value) {
                                    html += '<label><input style="width: auto" type="radio" value="' + key + '" name="index-value" id="index-value" onclick="checkFunction(' + key + ')"><span> ' + (key + 1) + ' Passed  <span onclick="checkFunction(' + key + ')"> (Click to view)</span></span></label>';
                                    html += '<br>';
                                });
                                $('#background-check-information').html(html);
                                $('.loading').hide();
                                $('#exampleModal').modal('show');
                                $('#applyButton').hide();
                                $('#enableFieldset').show();

                            } else {
                                $('.loading').hide();
                                $('#response_status').text('Record Not Found');
                                $('#exampleModal').modal('show');
                                $('#applyButton').hide();
                                $('#enableFieldset').show();
                            }
                        } else {
                            $('#response_status').text('Record Not Found');
                            $('.loading').hide();
                            $('#exampleModal').modal('show');
                            $('#applyButton').hide();
                            $('#enableFieldset').show();
                        }


                    } else {
                        $('.loading').hide();
                        toastr['error']("No Background Check Available.");
                    }
                },
                error: function () {
                    $('.loading').hide();
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        $('#applyButton').on('click', function () {
            if ($('#background_check').is(':checked') == true) {
                backgroundCheck();
            } else {
                $('#applyButton').hide();
                $('#enableFieldset').show();
            }
        })
        $('#background_check').on('change',function (){
            $('#applyButton').show();
            $('#enableFieldset').hide();
        })

    </script>
    <script>


        $('.states').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var state_id = $(this).val();
            var request = "state_id=" + state_id;

            if (state_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.stateCities') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status === "success") {
                            var html = "";
                            $.each(response.data.cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' >Select City</option>");

                            $('.cities').find('option[value="{{ old('property_city') }}"]').attr('selected', 'selected');
                            $('.cities').trigger('change');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        function checkFunction(index) {

            $('#bg-first-name').text(filteredRecord[index].firstname);
            $('#bg-last-name').text(filteredRecord[index].lastname);
            $('#bg-city').text(filteredRecord[index].city);
            $('#bg-state').text(filteredRecord[index].state);
            $('#bg-address').text(filteredRecord[index].address);
            $('#form-first-name').text($('#cl_first_name').val());
            $('#form-last-name').text($('#cl_last_name').val());
            $('#form-address').text($('#property_address_one').val() + ' ' + $('#property_address_two').val());
            $('#form-city').text($('#property_city option:selected').text());
            $('#form-state').text($('#property_state option:selected').text());


            $('#final-firstname').val(filteredRecord[index].firstname);
            $('#final-lastname').val(filteredRecord[index].lastname);
            $('#final-city').val(filteredRecord[index].city);
            $('#final-state').val(filteredRecord[index].state);
            $('#final-dob').val(filteredRecord[index].dob);
            $('#final-ssn').val($('#cl_doc_id_number').val());

            $('#exampleModal1').modal('show');
        }


        function validate(e) {
            $(':input[type="submit"]').prop('disabled', true);
            $('#loader').show();

            if (e.submitter.dataset.status == 'rejected') {
                $('#status').val('rejected');
            } else if (e.submitter.dataset.status == 'accepted') {
                $('#status').val('accepted');
            }


            if (e.submitter.dataset.status == 'accepted' && $("#background-check-information").html().length > 0 && $('#final-ssn').val() != '') {
                return true;
            } else if (e.submitter.dataset.status == 'accepted' && $("#background-check-information").html().length == 0 && $('#final-ssn').val() == '') {
                return true;
            } else if (e.submitter.dataset.status == 'rejected') {
                return true;
            } else {
                toastr['error']("Please select one option to save record.");
                $(':input[type="submit"]').prop('disabled', false);
                return false;
            }


        }

        $
    </script>
@endpush
