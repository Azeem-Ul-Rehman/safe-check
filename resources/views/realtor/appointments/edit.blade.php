@extends('realtor.main')
@section('title','Edit Appointment')
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/css/bootstrap-datetimepicker.min.css">

@push('css')

    <style>

        .select2-container {
            width: 100% !important;
        }

        * {
            margin: 0;
            padding: 0
        }

        html {
            height: 100%
        }

        #grad1 {
            /*border: 3px #000 solid;*/
        }

        #msform {
            text-align: center;
            position: relative;
            margin-top: 20px
        }

        #msform fieldset .form-card {
            background: white;
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            padding: 20px 40px 30px 40px;
            box-sizing: border-box;
            width: 94%;
            margin: 0 3% 20px 3%;
            position: relative
        }

        #msform fieldset {
            background: white;
            border: 0 none;
            border-radius: 0.5rem;
            box-sizing: border-box;
            width: 100%;
            margin: 0;
            padding-bottom: 20px;
            position: relative
        }

        #msform fieldset:not(:first-of-type) {
            display: none
        }

        #msform fieldset .form-card {
            text-align: left;
            color: #9E9E9E
        }

        #msform input,
        #msform textarea {
            padding: 0px 8px 4px 8px;
            border: none;
            border-bottom: 1px solid #ccc;
            border-radius: 0px;
            margin-bottom: 25px;
            margin-top: 2px;
            width: 100%;
            box-sizing: border-box;
            font-family: montserrat;
            color: #2C3E50;
            font-size: 16px;
            letter-spacing: 1px
        }

        #msform input:focus,
        #msform textarea:focus {
            -moz-box-shadow: none !important;
            -webkit-box-shadow: none !important;
            box-shadow: none !important;
            border: none;
            /* font-weight: bold; */
            border-bottom: 1px solid #911014;
            outline-width: 0
        }

        #msform .action-button {
            border: #911014 solid 2px;
            color: #fff;
            background: #911014;
            padding: 5px 25px;
            box-sizing: border-box;
            transition-duration: 0.3s;
            border-radius: 0;
            font-size: 16px;
            font-weight: 400;
            text-decoration: none;
            cursor: pointer;
            border-radius: 50px !important;
            width: 20%;
        }


        #msform .action-button-previous {
            width: 100px;
            background: #616161;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
            border-radius: 50px !important;
        }

        #msform .action-button-appointment {
            width: 200px;
            background: skyblue;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }


        select.list-dt {
            border: none;
            outline: 0;
            border-bottom: 1px solid #ccc;
            padding: 2px 5px 3px 5px;
            margin: 2px
        }

        select.list-dt:focus {
            border-bottom: 2px solid skyblue
        }

        .card {
            z-index: 0;
            border: none;
            border-radius: 0.5rem;
            position: relative
        }

        .fs-title {
            font-size: 25px;
            color: #911014;
            margin-bottom: 10px;
            font-weight: bold;
            text-align: left;
            margin-bottom: 20px;
        }

        #progressbar {
            width: 101%;

            margin-bottom: 30px;
            overflow: hidden;
            color: lightgrey
        }

        #progressbar .active {
            color: #000000
        }

        #progressbar li {
            list-style-type: none;
            font-size: 15px;
            width: 33%;
            float: left;
            position: relative
        }

        #progressbar #account:before {

            /*content: "\f023"*/
            content: "\f007"
        }

        #progressbar #personal:before {

            content: "\f007"
        }

        #progressbar #payment:before {

            content: "\f09d"
        }

        #progressbar #confirm:before {

            content: "\f00c"
        }

        #progressbar li:before {
            width: 50px;
            height: 50px;
            line-height: 45px;
            display: block;
            font-size: 18px;
            color: #ffffff;
            background: lightgray;
            border-radius: 50%;
            margin: 0 auto 10px auto;
            padding: 2px
        }

        #progressbar li:after {
            content: '';
            width: 100%;
            height: 2px;
            background: lightgray;
            position: absolute;
            left: 0;
            top: 25px;
            z-index: -1
        }

        #progressbar li.active:before,
        #progressbar li.active:after {

        }

        .radio-group {
            position: relative;
            margin-bottom: 25px
        }

        .radio {
            display: inline-block;
            width: 204px;
            height: 104px;
            border-radius: 0;
            background: lightblue;
            box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
            box-sizing: border-box;
            cursor: pointer;
            margin: 8px 2px
        }

        .radio:hover {
            box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
        }

        .radio.selected {
            box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
        }

        .fit-image {
            width: 100%;
            object-fit: cover
        }

        .pl-125 {
            padding-left: 125px;
            color: #000;
        }

        .pl-155 {
            padding-left: 155px;
            color: #000;
        }

        .pl-159 {
            padding-left: 159px;
            color: #000;
        }

        .pl-115 {
            padding-left: 115px;
            color: #000;
        }

        .pl-110 {
            padding-left: 110px;
            color: #000;
        }

        .pl-91 {
            padding-left: 91px;
            color: #000;
        }

        .float-right {
            float: right;
        }

        #msform .review_items {
            width: 200px;
            background: skyblue;
            font-weight: bold;
            color: white;
            border: 0 none;
            border-radius: 0px;
            cursor: pointer;
            padding: 10px 5px;
            margin: 10px 5px;
        }

        .hideSpan {
            display: none;
        }

        .colorSpan {
            color: red;
        }

    </style>
@endpush
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Appointment') }}
                        </h3>
                    </div>
                </div>
            </div>

            <!-- MultiStep Form -->
            <div class="container-fluid" id="grad1">
                <div class="row justify-content-center mt-0">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 text-center p-0 mt-3 mb-2 appointmentMain">
                        <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                            <h2><strong>Appointment Information</strong></h2>
                            <p>Fill all form field to go to next step</p>
                            <div class="row">
                                <div class="col-md-12 mx-0">
                                    <form class="m-form" method="post"
                                          action="{{ route('realtor.appointments.update' , $getAppoitment->appointment->id) }}"
                                          id="msform"
                                          enctype="multipart/form-data" role="form">

                                        @method('patch')
                                        @csrf
                                        <input type="hidden" name="appointment_schedule_id"
                                               value="{{$getAppoitment->id}}">
                                        <!-- progressbar -->
                                        <ul id="progressbar">
                                            <li class="active fa fa-user" id="account"><strong>Personal
                                                    Information</strong></li>
                                            <li class="fa fa-user" id="personal"><strong>Schedule</strong></li>
                                            <li class="fa fa-user" id="confirm"><strong>Finish</strong></li>
                                        </ul> <!-- fieldsets -->
                                        <fieldset>
                                            <input type="text" name="timeZoneOffset" id="timeZoneOffset" value="">
                                            <div id="hidden-fields">
                                                @if(!is_null($backgroundCheck))
                                                    <input type="hidden" name="final_firstname" id="final-firstname"
                                                           value="{{$backgroundCheck->first_name}}">
                                                    <input type="hidden" name="final_lastname" id="final-lastname"
                                                           value="{{$backgroundCheck->last_name}}">
                                                    <input type="hidden" name="final_ssn" id="final-ssn"
                                                           value="{{$backgroundCheck->ssn}}">
                                                    <input type="hidden" name="final_dob" id="final-dob"
                                                           value="{{$backgroundCheck->dob}}">
                                                    <input type="hidden" name="final_city" id="final-city"
                                                           value="{{$backgroundCheck->city}}">
                                                    <input type="hidden" name="final_state" id="final-state"
                                                           value="{{$backgroundCheck->state}}">
                                                @else
                                                    <input type="hidden" name="final_firstname" id="final-firstname"
                                                           value="">
                                                    <input type="hidden" name="final_lastname" id="final-lastname"
                                                           value="">
                                                    <input type="hidden" name="final_ssn" id="final-ssn" value="">
                                                    <input type="hidden" name="final_dob" id="final-dob" value="">
                                                    <input type="hidden" name="final_city" id="final-city" value="">
                                                    <input type="hidden" name="final_state" id="final-state" value="">
                                                @endif
                                            </div>
                                            <div class="form-card">
                                                <h5 class="fs-title">Location</h5>
                                                <input type="text" name="property_address_one" id="property_address_one"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->property_address_one)) ?$getAppoitment->appointment->property_address_one : '' }}"
                                                       placeholder="Enter Street Address"/>
                                                <span id="property_address_one_message" class="hideSpan colorSpan">Enter Street Address</span>
                                                <input type="text" name="property_address_two" id="property_address_two"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->property_address_two)) ?$getAppoitment->appointment->property_address_two : '' }}"
                                                       placeholder="Enter Street Address Two"/>
                                                <span id="property_address_two_message" class="hideSpan colorSpan">Enter Street Address</span>
                                                <div class="row">
                                                    <div class="col-12">

                                                        <select name="property_state" id="property_state"
                                                                class="states list-dt-first" style="width: 100%">
                                                            <option value="">Select State</option>

                                                            @if(!empty($states) && count($states) >0)
                                                                @foreach($states as $state)
                                                                    <option
                                                                        value="{{$state->id}}" {{ ((int)$getAppoitment->appointment->property_state == $state->id) ? 'selected' : ''  }}> {{ ucfirst($state->name) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                        <span id="property_state_message"
                                                              class="hideSpan colorSpan">Enter State</span>
                                                    </div>


                                                </div>
                                                <div class="row">

                                                    <div class="col-12">
                                                        <select name="property_city" id="property_city"
                                                                class="cities list-dt-first" style="width: 100%">
                                                            <option value="">Select City</option>
                                                            @if(!empty($cities) && count($cities) >0)
                                                                @foreach($cities as $city)
                                                                    <option
                                                                        value="{{$city->id}}"
                                                                        selected> {{ ucfirst($city->name) }}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>

                                                        <span id="property_city_message" class="hideSpan colorSpan">Enter Property City</span>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-card">
                                                <h5 class="fs-title">Buyers Information</h5>
                                                <input type="text" name="cl_first_name" id="cl_first_name"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_first_name)) ?$getAppoitment->appointment->cl_first_name : '' }}"
                                                       placeholder="First Name"/>
                                                <span id="cl_first_name_message" class="hideSpan colorSpan">Please Enter First Name</span>
                                                <input type="text" name="cl_last_name" id="cl_last_name"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_last_name)) ?$getAppoitment->appointment->cl_last_name : '' }}"
                                                       placeholder="Last Name"/>
                                                <span id="cl_last_name_message"
                                                      class="hideSpan colorSpan">Please Enter Last Name</span>
                                                <input type="date" name="cl_dob" id="cl_dob"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_dob)) ? $getAppoitment->appointment->cl_dob : '' }}"
                                                       placeholder="DOB"/>
                                                <span id="cl_dob_message"
                                                      class="hideSpan colorSpan">Please Enter Date of Birth</span>
                                                <input type="text" name="cl_doc_id_number"
                                                       placeholder="Id Card or License Number" id="cl_doc_id_number"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_doc_id_number)) ?$getAppoitment->appointment->cl_doc_id_number : '' }}"/>
                                                <span id="cl_doc_id_number_message" class="hideSpan colorSpan">Please Enter Id Card or License Number</span>


                                                <select name="cl_doc_id_state" id="cl_doc_id_state"
                                                        class="list-dt-first" style="width: 100%">
                                                    <option value="">Select License State</option>

                                                    @if(!empty($states) && count($states) >0)
                                                        @foreach($states as $state)
                                                            <option
                                                                value="{{$state->id}}" {{ ((int)$getAppoitment->appointment->cl_doc_id_state == $state->id) ? 'selected' : ''  }}> {{ ucfirst($state->name) }}</option>
                                                        @endforeach
                                                    @endif
                                                </select>

                                                {{--                                                <input type="text" name="cl_doc_id_state"--}}
                                                {{--                                                       placeholder="Id Card or License State" id="cl_doc_id_state"--}}
                                                {{--                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_doc_id_state)) ?$getAppoitment->appointment->cl_doc_id_state : '' }}"/>--}}

                                                <span id="cl_doc_id_state_message" class="hideSpan colorSpan">Please Enter Id Card or License State</span>

                                                <input type="text" name="cl_mobile_number" placeholder="Mobile number"
                                                       id="cl_mobile_number"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_mobile_number)) ?$getAppoitment->appointment->cl_mobile_number : '' }}"/>
                                                <span id="cl_mobile_number_message" class="hideSpan colorSpan">Please Enter Mobile Number</span>
                                                <input type="email" name="cl_email" placeholder="Email Address"
                                                       id="cl_email"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->appointment) && isset($getAppoitment->appointment->cl_email)) ?$getAppoitment->appointment->cl_email : '' }}"/>
                                                <span id="cl_email_message" class="hideSpan colorSpan">Please Enter Email</span>
                                                <br>
                                                <label for="uplod_doc_message" style="font-weight: 700 !important;">Upload
                                                    a Picture of Your Stated Issued ID
                                                    Card Or Drivers License</label>

                                                <div class="input-group">
                                                    <input value="{{old('upload_doc')}}" type="file"
                                                           class="form-control @error('upload_doc') is-invalid @enderror propic"
                                                           id="file" name="upload_doc" onchange="readURLImage(this);">
                                                    <label for="file"><img id="upld"
                                                                           src="{{ asset('frontend/images/uploadbtn.png') }}"
                                                                           alt="Upload Image"></label>
                                                    <img width="300" height="200" class="img-thumbnail"
                                                         style="display:{{($getAppoitment->appointment->image) ? 'block' : 'none'}};"
                                                         id="img"
                                                         src="{{ asset('/uploads/appointmentImage/'.$getAppoitment->appointment->image) }}"
                                                         alt="your image"/>

                                                </div>
                                                <span id="uplod_doc_message" class="hideSpan colorSpan">Please Enter File</span>

                                                <label><input type="checkbox" name="background_check"
                                                              id="background_check"
                                                              value="0"
                                                              style="width: auto"><span> Background Check</span></label>

                                            </div>
                                            <button type="button" name="next" class="next action-button"
                                                    value="1">Next
                                            </button>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-card">
                                                <div id="background-check-information">

                                                    <label><b>Background Check Information:</b></label>

                                                    @if(!is_null($backgroundCheck))
                                                        <p>{{$backgroundCheck->first_name}}</p>
                                                        <p>{{$backgroundCheck->last_name}}</p>
                                                        <p>{{$backgroundCheck->ssn}}</p>
                                                        <p>{{$backgroundCheck->dob}}</p>
                                                        <p>{{$backgroundCheck->city}}</p>
                                                        <p>{{$backgroundCheck->state}}</p>
                                                    @endif

                                                </div>

                                            </div>
                                            <div class="form-card">
                                                <h5 class="fs-title">Set Appointment Date*</h5>
                                                <input type="text" name="start_date" id="start_date"
                                                       placeholder="dd/mm/yyyy"
                                                       value="{{(isset($getAppoitment) && isset($getAppoitment->start_date)) ? $getAppoitment->start_date: '' }}"/>
                                                <span id="start_date_message" class="hideSpan colorSpan">Please Enter Appointment Date</span>
                                                <h5 class="fs-title">Set Appointment time*</h5>
                                                @php
                                                    $splitStartTime= explode(":",$getAppoitment->start_time);
                                                    $splitEndTime= explode(":",$getAppoitment->end_time);;
                                                    $startHour =$splitStartTime[0];
                                                    $endHour =$splitEndTime[0];
                                                    $startMin =$splitStartTime[1];
                                                    $endMin =$splitEndTime[1];

                                                    if((int)$startHour >= 12){
                                                        $startPeriod = 'pm';
                                                    }else{
                                                        $startPeriod = 'am';
                                                    }

                                                    if((int)$endHour >= 12){
                                                        $endPeriod = 'pm';
                                                    }else{
                                                        $endPeriod = 'am';
                                                    }
                                                @endphp
                                                <div class="row">
                                                    <div class="col-3"><label class="start_time_hour">Start
                                                            Time*</label></div>
                                                    <div class="col-9">
                                                        <select class="list-dt" id="start_time_hour"
                                                                name="start_time_hour" style="width: 10% !important;">

                                                            <option
                                                                value="01" {{ $startHour =="01" ? 'selected' : '' }}>1
                                                            </option>
                                                            <option
                                                                value="02" {{ $startHour =="02" ? 'selected' : '' }}>2
                                                            </option>
                                                            <option
                                                                value="03" {{ $startHour =="03" ? 'selected' : '' }}>3
                                                            </option>
                                                            <option
                                                                value="04" {{ $startHour =="04" ? 'selected' : '' }}>4
                                                            </option>
                                                            <option
                                                                value="05" {{ $startHour =="05" ? 'selected' : '' }}>5
                                                            </option>
                                                            <option
                                                                value="06" {{ $startHour =="06" ? 'selected' : '' }}>6
                                                            </option>
                                                            <option
                                                                value="07" {{ $startHour =="07" ? 'selected' : '' }}>7
                                                            </option>
                                                            <option
                                                                value="08" {{ $startHour =="08" ? 'selected' : '' }}>8
                                                            </option>
                                                            <option
                                                                value="09" {{ $startHour =="09" ? 'selected' : '' }}>9
                                                            </option>
                                                            <option
                                                                value="10" {{ $startHour =="10" ? 'selected' : '' }}>10
                                                            </option>
                                                            <option
                                                                value="11" {{ $startHour =="11" ? 'selected' : '' }}>11
                                                            </option>
                                                            <option
                                                                value="12" {{ $startHour =="12" ? 'selected' : '' }}>12
                                                            </option>
                                                            <option
                                                                value="13" {{ $startHour =="13" ? 'selected' : '' }}>13
                                                            </option>
                                                            <option
                                                                value="14" {{ $startHour =="14" ? 'selected' : '' }}>14
                                                            </option>
                                                            <option
                                                                value="15" {{ $startHour =="15" ? 'selected' : '' }}>15
                                                            </option>
                                                            <option
                                                                value="16" {{ $startHour =="16" ? 'selected' : '' }}>16
                                                            </option>
                                                            <option
                                                                value="17" {{ $startHour =="17" ? 'selected' : '' }}>17
                                                            </option>
                                                            <option
                                                                value="18" {{ $startHour =="18" ? 'selected' : '' }}>18
                                                            </option>
                                                            <option
                                                                value="19" {{ $startHour =="19" ? 'selected' : '' }}>19
                                                            </option>
                                                            <option
                                                                value="20" {{ $startHour =="20" ? 'selected' : '' }}>20
                                                            </option>
                                                            <option
                                                                value="21" {{ $startHour =="21" ? 'selected' : '' }}>21
                                                            </option>
                                                            <option
                                                                value="22" {{ $startHour =="22" ? 'selected' : '' }}>22
                                                            </option>
                                                            <option
                                                                value="23" {{ $startHour =="23" ? 'selected' : '' }}>23
                                                            </option>
                                                            <option
                                                                value="24" {{ $startHour =="24" ? 'selected' : '' }}>24
                                                            </option>
                                                        </select>
                                                        <select class="list-dt" id="start_time_min"
                                                                name="start_time_min" style="width: 10% !important;">


                                                            @for($i=0 ;$i<60;$i++)
                                                                @if($i < 10)
                                                                    <option
                                                                        value="0{{$i}}" {{ $startMin =='0'.$i.'' ? 'selected' : '' }}>
                                                                        0{{$i}}</option>
                                                                @else
                                                                    <option
                                                                        value="{{$i}}" {{ $startMin ==$i ? 'selected' : '' }}>{{$i}}</option>
                                                                @endif
                                                            @endfor


                                                            {{--                                                            <option value="00" {{ $startMin =="00" ? 'selected' : '' }}>--}}
                                                            {{--                                                                00--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="15" {{ $startMin =="15" ? 'selected' : '' }}>--}}
                                                            {{--                                                                15--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="30" {{ $startMin =="30" ? 'selected' : '' }}>--}}
                                                            {{--                                                                30--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="45" {{ $startMin =="45" ? 'selected' : '' }}>--}}
                                                            {{--                                                                45--}}
                                                            {{--                                                            </option>--}}

                                                        </select>
                                                        <select class="list-dt" id="start_time_period"
                                                                style="width: 10% !important;"
                                                                name="start_time_period">

                                                            <option
                                                                value="am" {{$startPeriod == "am" ? 'selected' : ''}}>AM
                                                            </option>
                                                            <option
                                                                value="pm" {{$startPeriod == "pm" ? 'selected' : ''}}>PM
                                                            </option>
                                                        </select>
                                                        <span id="start_time_message" class="hideSpan colorSpan">Please Enter Appointment Start Time</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-3"><label class="end_time_hour">End Time*</label>
                                                    </div>
                                                    <div class="col-9">
                                                        <select class="list-dt" id="end_time_hour" name="end_time_hour"
                                                                style="width: 10% !important;">

                                                            <option value="01" {{ $endHour =="01" ? 'selected' : '' }}>
                                                                1
                                                            </option>
                                                            <option value="02" {{ $endHour =="02" ? 'selected' : '' }}>
                                                                2
                                                            </option>
                                                            <option value="03" {{ $endHour =="03" ? 'selected' : '' }}>
                                                                3
                                                            </option>
                                                            <option value="04" {{ $endHour =="04" ? 'selected' : '' }}>
                                                                4
                                                            </option>
                                                            <option value="05" {{ $endHour =="05" ? 'selected' : '' }}>
                                                                5
                                                            </option>
                                                            <option value="06" {{ $endHour =="06" ? 'selected' : '' }}>
                                                                6
                                                            </option>
                                                            <option value="07" {{ $endHour =="07" ? 'selected' : '' }}>
                                                                7
                                                            </option>
                                                            <option value="08" {{ $endHour =="08" ? 'selected' : '' }}>
                                                                8
                                                            </option>
                                                            <option value="09" {{ $endHour =="09" ? 'selected' : '' }}>
                                                                9
                                                            </option>
                                                            <option value="10" {{ $endHour =="10" ? 'selected' : '' }}>
                                                                10
                                                            </option>
                                                            <option value="11" {{ $endHour =="11" ? 'selected' : '' }}>
                                                                11
                                                            </option>
                                                            <option value="12" {{ $endHour =="12" ? 'selected' : '' }}>
                                                                12
                                                            </option>
                                                            <option value="13" {{ $endHour =="13" ? 'selected' : '' }}>
                                                                13
                                                            </option>
                                                            <option value="14" {{ $endHour =="14" ? 'selected' : '' }}>
                                                                14
                                                            </option>
                                                            <option value="15" {{ $endHour =="15" ? 'selected' : '' }}>
                                                                15
                                                            </option>
                                                            <option value="16" {{ $endHour =="16" ? 'selected' : '' }}>
                                                                16
                                                            </option>
                                                            <option value="17" {{ $endHour =="17" ? 'selected' : '' }}>
                                                                17
                                                            </option>
                                                            <option value="18" {{ $endHour =="18" ? 'selected' : '' }}>
                                                                18
                                                            </option>
                                                            <option value="19" {{ $endHour =="19" ? 'selected' : '' }}>
                                                                19
                                                            </option>
                                                            <option value="20" {{ $endHour =="20" ? 'selected' : '' }}>
                                                                20
                                                            </option>
                                                            <option value="21" {{ $endHour =="21" ? 'selected' : '' }}>
                                                                21
                                                            </option>
                                                            <option value="22" {{ $endHour =="22" ? 'selected' : '' }}>
                                                                22
                                                            </option>
                                                            <option value="23" {{ $endHour =="23" ? 'selected' : '' }}>
                                                                23
                                                            </option>
                                                            <option value="24" {{ $endHour =="24" ? 'selected' : '' }}>
                                                                24
                                                            </option>
                                                        </select>
                                                        <select class="list-dt" id="end_time_min" name="end_time_min"
                                                                style="width: 10% !important;">

                                                            {{--                                                            <option value="00" {{ $endMin =="00" ? 'selected' : '' }}>--}}
                                                            {{--                                                                00--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="15" {{ $endMin =="15" ? 'selected' : '' }}>--}}
                                                            {{--                                                                15--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="30" {{ $endMin =="30" ? 'selected' : '' }}>--}}
                                                            {{--                                                                30--}}
                                                            {{--                                                            </option>--}}
                                                            {{--                                                            <option value="45" {{ $endMin =="45" ? 'selected' : '' }}>--}}
                                                            {{--                                                                45--}}
                                                            {{--                                                            </option>--}}

                                                            @for($i=0 ;$i<60;$i++)
                                                                @if($i < 10)
                                                                    <option
                                                                        value="0{{$i}}" {{ $endMin =='0'.$i.'' ? 'selected' : '' }}>
                                                                        0{{$i}}</option>
                                                                @else
                                                                    <option
                                                                        value="{{$i}}" {{ $endMin ==$i ? 'selected' : '' }}>{{$i}}</option>
                                                                @endif
                                                            @endfor

                                                        </select>
                                                        <select class="list-dt" id="end_time_period"
                                                                name="end_time_period" style="width: 10% !important;">

                                                            <option value="am" {{$endPeriod == "am" ? 'selected' : ''}}>
                                                                AM
                                                            </option>
                                                            <option value="pm" {{$endPeriod == "pm" ? 'selected' : ''}}>
                                                                PM
                                                            </option>
                                                        </select>
                                                        <span id="end_time_message" class="hideSpan colorSpan">Please Enter Appointment End Time</span>
                                                    </div>
                                                </div>

                                            </div>
                                            <input type="button" name="previous" class="previous action-button-previous"
                                                   value="Previous"/>
                                            <input type="button" name="reject" id="reject"
                                                   class="reject action-button-previous"
                                                   value="Reject"/>
                                            <button type="button" name="next" class="next action-button"
                                                    value="2">Confirm
                                            </button>
                                            {{--                                            <input type="button" name="review_items" class="review_items    "--}}
                                            {{--                                                   value="Review Items"/>--}}
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-card">
                                                <h5 class="fs-title">Your Clients Info </h5>
                                                <label class="pay">First Name: </label><span
                                                    class="pl-155"
                                                    id="first_name_third_step">{{$getAppoitment->appointment->cl_first_name}}</span><br>
                                                <label class="pay">Last Name: </label><span
                                                    class="pl-159"
                                                    id="last_name_third_step">{{$getAppoitment->appointment->cl_last_name}}</span><br>
                                                <label class="pay">Propert Address: </label><span
                                                    class="pl-125"
                                                    id="property_address_third_step">{{$getAppoitment->appointment->property_address_one}} {{$getAppoitment->appointment->property_address_two}}</span><br>
                                                <label class="pay">Appointment Date: </label><span
                                                    class="pl-115"
                                                    id="appointment_date_third_step">{{$getAppoitment->start_date}}</span><br>
                                                <label class="pay">Appointment Time: </label><span class="pl-110"
                                                                                                   id="appointment_time_third_step"> {{$getAppoitment->start_time}} - {{$getAppoitment->end_time}}</span><br>
                                                {{--                                                <label class="pay">Appointment Duration: </label><span class="pl-91">2hrs</span><br>--}}


                                            </div>
                                            <div class="form-card">
                                                <label for="local_police_number">Local Police Number</label>

                                                <input type="text" class="" name="local_police_number"
                                                       id="local_police_number"
                                                       value="{{$getAppoitment->local_police_number ?? '911'}}">


                                            </div>
                                            <div class="form-card">
                                                <h2 class="fs-title">Emergency Contacts</h2>
                                                <div id="all_contacts">
                                                    @if (isset($getAppoitment->emergencyContacts))
                                                        @foreach ($getAppoitment->emergencyContacts as $key => $emergency_contacts)
                                                            <div id="emergency_contacts{{$key +1}}">
                                                                <input type="text" name="name[]" placeholder="Name"
                                                                       required
                                                                       value="{{$emergency_contacts->name}}"/>
                                                                <input type="text" name="mobile_number[]"
                                                                       placeholder="Phone Number." required
                                                                       value="{{$emergency_contacts->mobile_number}}"/>
                                                                @if($key+1 > 1)
                                                                    <button type="button" name="remove" id=""
                                                                            class="btn btn-danger remove">Remove
                                                                        EMERGENCY
                                                                        CONTACT
                                                                    </button>
                                                                @else
                                                                    <button type="button" name="add" id="add"
                                                                            class="btn btn-success">ADD EMERGENCY
                                                                        CONTACT
                                                                    </button>
                                                                @endif

                                                            </div>
                                                        @endforeach
                                                    @endif

                                                </div>
                                            </div>
                                            <input type="button" name="previous" class="previous action-button-previous"
                                                   value="Previous"/>

                                            <input type="submit" name="make_payment" class="action-button-appointment"
                                                   value="Confirm Appointment"/>
                                        </fieldset>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h3>Background Check!</h3>
                    <h2 id="response_status">PASSED!</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Background Check Informations</b></h4>
                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">

                    {{--First Name--}}
                    <label>
                        <b>Actual First Name: </b>
                        <span id="form-first-name"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search First Name: </b>
                        <span id="bg-first-name"></span>
                    </label>
                    <br>

                    {{--Last Name--}}
                    <label>
                        <b>Actual Last Name: </b>
                        <span id="form-last-name"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search Last Name: </b>
                        <span id="bg-last-name"></span>
                    </label>

                    <br>

                    {{--State--}}
                    <label>
                        <b>Actual State: </b>
                        <span id="form-state"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search State:</b>
                        <span id="bg-state"></span>
                    </label>


                    {{--City--}}
                    <br>
                    <label>
                        <b>Actual City: </b>
                        <span id="form-city"></span>
                    </label>
                    <br>
                    <label>
                        <b>Search City: </b>
                        <span id="bg-city"></span>
                    </label>

                    <br>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/3.1.4/js/bootstrap-datetimepicker.min.js"></script>

    <script>

        var filteredRecord;
        $('#start_date').datepicker({
            format: "yyyy-mm-dd",
        });

        $(document).ready(function () {

            var timeZoneOffset = moment().utcOffset();
            $('#timeZoneOffset').valueOf(timeZoneOffset);
            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var getValue;


            $(".next").click(function () {

                if ($(this).val() === "1") {
                    getValue = steponevalidation();
                } else if ($(this).val() === "2") {
                    getValue = steptwovalidation();
                }

                if (getValue === false) {

                } else {
                    if ($(this).val() === "1") {
                        backgroundCheck();
                    }
                    if ($(this).val() === "2") {
                        $('#first_name_third_step').text($('#cl_first_name').val());
                        $('#last_name_third_step').text($('#cl_last_name').val());
                        $('#property_address_third_step').text($('#property_address_one').val() + ' ' + $('#property_address_two').val());
                        $('#appointment_date_third_step').text($('#start_date').val());
                        // $('#appointment_duration_third_step').text($('#duration').val() + 'hrs');
                        $('#appointment_time_third_step').text($('#start_time_hour').val() + ':' + $('#start_time_min').val() + '' + $('#start_time_period').val() + ' - ' + $('#end_time_hour').val() + ':' + $('#end_time_min').val() + '' + $('#end_time_period').val());
                    }

                    current_fs = $(this).parent();
                    next_fs = $(this).parent().next();

//Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

//show the next fieldset
                    next_fs.show();
//hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function (now) {
// for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({'opacity': opacity});
                        },
                        duration: 600
                    });
                }
            });

            $(".previous").click(function () {

                current_fs = $(this).parent();
                previous_fs = $(this).parent().prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function (now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 600
                });
            });

            $('.radio-group .radio').click(function () {
                $(this).parent().find('.radio').removeClass('selected');
                $(this).addClass('selected');
            });

            $(".submit").click(function () {
                return false;
            });

            var count = 1;
            count = '{{count($getAppoitment->emergencyContacts) > 0 ? count($getAppoitment->emergencyContacts) : 1 }}';

            function dynamic_field(number) {
                html = '<div id="emergency_contacts' + number + '">';
                html += '<input type="text" name="name[]" placeholder="Name" required/>';
                html += '<input type="text" name="mobile_number[]" placeholder="Phone Number." required/>';
                if (number > 1) {
                    html += '<button type="button" name="remove" id="" class="btn btn-danger remove">Remove EMERGENCY CONTACT</button></div>';
                    $('#all_contacts').append(html);
                } else {
                    html += '<button type="button" name="add" id="add" class="btn btn-success">ADD EMERGENCY CONTACT</button></div>';
                    $('#all_contacts').html(html);
                }
            }

            $(document).on('click', '#add', function () {
                count++;
                dynamic_field(count);
            });

            $(document).on('click', '.remove', function () {
                var removecount = count;
                count--;
                $('#emergency_contacts' + removecount + '').remove();
            });

        });

        function readURLImage(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {

                    if (input.files[0].size > 2097152) {
                        $("#file").val('');
                        toastr['error']("Video size less then 2MB!");
                    } else {
                        $('#img').attr('src', e.target.result);
                        $('#img').css("display", "block");
                    }
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function steponevalidation() {
            var check = true;
            if ($("#property_address_one").val() == '') {
                $("#property_address_one_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_address_one_message").addClass('hideSpan');
            }
            // if ($("#property_address_two").val() == '') {
            //     $("#property_address_two_message").removeClass('hideSpan');
            //     check = false;
            // } else {
            //     $("#property_address_two_message").addClass('hideSpan');
            // }
            if ($("#property_city").val() == '') {
                $("#property_city_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_city_message").addClass('hideSpan');
            }
            if ($("#property_state").val() == '') {
                $("#property_state_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#property_state_message").addClass('hideSpan');
            }

            if ($("#cl_first_name").val() == '') {
                $("#cl_first_name_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_first_name_message").addClass('hideSpan');
            }
            if ($("#cl_last_name").val() == '') {
                $("#cl_last_name_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_last_name_message").addClass('hideSpan');
            }
            if ($("#cl_dob").val() == '') {
                $("#cl_dob_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_dob_message").addClass('hideSpan');
            }
            if ($("#cl_doc_id_number").val() == '') {
                $("#cl_doc_id_number_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_doc_id_number_message").addClass('hideSpan');
            }
            if ($("#cl_doc_id_state").val() == '') {
                $("#cl_doc_id_state_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_doc_id_state_message").addClass('hideSpan');
            }
            if ($("#cl_mobile_number").val() == '') {
                $("#cl_mobile_number_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_mobile_number_message").addClass('hideSpan');
            }
            if ($("#cl_email").val() == '') {
                $("#cl_email_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#cl_email_message").addClass('hideSpan');
            }
            if ($("#uplod_doc").val() == '') {
                $("#uplod_doc_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#uplod_doc_message").addClass('hideSpan');
            }
            return check;
        }

        function steptwovalidation() {
            var check = true;
            if ($("#start_date").val() == '') {
                $("#start_date_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_date_message").addClass('hideSpan');
            }
            if ($("#duration").val() == '') {
                $("#duration_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#duration_message").addClass('hideSpan');
            }
            if ($("#start_time_hour").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }
            if ($("#start_time_min").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }

            if ($("#start_time_period").val() == '') {
                $("#start_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#start_time_message").addClass('hideSpan');
            }
            if ($("#end_time_hour").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }
            if ($("#end_time_hour").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }
            if ($("#end_time_period").val() == '') {
                $("#end_time_message").removeClass('hideSpan');
                check = false;
            } else {
                $("#end_time_message").addClass('hideSpan');
            }

            return check;
        }

        function backgroundCheck() {
            if ($('#background_check').is(':checked') === true) {

                $('#background_check').val(1);
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.backgroundCheck') }}",
                    data: {
                        'backgroundCheck': true,
                        'first_name': $('#cl_last_name').val(),
                        'last_name': $('#cl_first_name').val(),
                        'dob': $('#cl_dob').val()
                    },
                    success: function (response) {
                        debugger;
                        if (response.status === "success") {

                            var data = response.data.backgroundCheckResponse.searchResults.CriminalSearch.Result;
                            if (data.length > 0) {
                                filteredRecord = data.filter(element => element.lastname == $('#cl_last_name').val() && element.firstname == $('#cl_first_name').val() && element.dob ==$('#cl_dob').val())

                                if (filteredRecord.length > 0) {
                                    $('#response_status').text('Passed');

                                    var html = '';
                                    $.each(filteredRecord, function (key, value) {
                                        html += '<label><input style="width: auto" type="radio" value="' + key + '" name="index-value" id="index-value" onclick="checkFunction(' + key + ')"><span> ' + (key + 1) + ' Passed </span></label>';
                                        html += '<br>';
                                    });
                                    $('#background-check-information').html(html);
                                    $('#exampleModal').modal('show');
                                } else {
                                    $('#response_status').text('Record Not Found');
                                    $('#exampleModal').modal('show');
                                }
                            } else {
                                $('#response_status').text('Record Not Found');
                                $('#exampleModal').modal('show');
                            }


                        } else {
                            toastr['error']("No Background Check Available.");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                alert('checked false');
            }
        }


    </script>
    <script>
        $('.states').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.cities';
            var state_id = $(this).val();
            var request = "state_id=" + state_id;

            if (state_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.stateCities') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status === "success") {
                            var html = "";
                            $.each(response.data.cities, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select City</option>");

                            $('.cities').find('option[value="{{ old('property_city') }}"]').attr('selected', 'selected');
                            $('.cities').trigger('change');
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select City</option>");
            }
        });

        {{--$('#background_check').on('click', function () {--}}
        {{--    if ($(this).is(':checked') === true) {--}}

        {{--        $('#background_check').val(1);--}}
        {{--        $.ajax({--}}
        {{--            type: "GET",--}}
        {{--            url: "{{ route('ajax.backgroundCheck') }}",--}}
        {{--            data: {'backgroundCheck': true},--}}
        {{--            success: function (response) {--}}
        {{--                debugger;--}}
        {{--                if (response.status === "success") {--}}

        {{--                    var data = response.data.backgroundCheckResponse.searchResults.CriminalSearch.Result;--}}
        {{--                    if (data.length > 0) {--}}
        {{--                        filteredRecord = data.filter(element => element.lastname == $('#cl_last_name').val() && element.firstname == $('#cl_first_name').val() && element.state == "FL" && element.city == "SPRING HILL")--}}

        {{--                        if (filteredRecord.length > 0) {--}}
        {{--                            $('#response_status').text('Passed');--}}

        {{--                            var html = '';--}}

        {{--                            $.each(filteredRecord, function (key, value) {--}}
        {{--                                html += '<label><input type="radio" value="' + key + '" name="index-value" style="width: auto" id="index-value" onclick="checkFunction(' + key + ')"><span> ' + (key + 1) + ' Passed </span></label>';--}}
        {{--                                html += '<br>';--}}
        {{--                            });--}}

        {{--                            $('#background-check-information').html(html);--}}


        {{--                            $('#exampleModal').modal('show');--}}
        {{--                        } else {--}}
        {{--                            $('#response_status').text('Record Not Found');--}}
        {{--                            $('#exampleModal').modal('show');--}}
        {{--                        }--}}
        {{--                    } else {--}}
        {{--                        $('#response_status').text('Record Not Found');--}}
        {{--                        $('#exampleModal').modal('show');--}}
        {{--                    }--}}


        {{--                } else {--}}
        {{--                    toastr['error']("No Background Check Available.");--}}
        {{--                }--}}
        {{--            },--}}
        {{--            error: function () {--}}
        {{--                toastr['error']("Something Went Wrong.");--}}
        {{--            }--}}
        {{--        });--}}
        {{--    } else {--}}
        {{--        alert('checked false');--}}
        {{--    }--}}
        {{--});--}}

        function checkFunction(index) {

            $('#bg-first-name').text(filteredRecord[index].firstname);
            $('#bg-last-name').text(filteredRecord[index].lastname);
            $('#bg-city').text(filteredRecord[index].city);
            $('#bg-state').text(filteredRecord[index].state);
            $('#form-first-name').text($('#cl_first_name').val());
            $('#form-last-name').text($('#cl_last_name').val());
            $('#form-city').text($('#property_city option:selected').text());
            $('#form-state').text($('#property_state option:selected').text());


            $('#final-firstname').val(filteredRecord[index].firstname);
            $('#final-lastname').val(filteredRecord[index].lastname);
            $('#final-city').val(filteredRecord[index].city);
            $('#final-state').val(filteredRecord[index].state);
            $('#final-dob').val(filteredRecord[index].dob);
            $('#final-ssn').val($('#cl_doc_id_number').val());

            $('#exampleModal1').modal('show');
        }

        $('#reject').on('click', function () {
            if (confirm("Are you sure You want to reject this appointment ?")) {
                window.location.href = '{{route('realtor.appointments.index')}}';
            }


        });

    </script>
@endpush
