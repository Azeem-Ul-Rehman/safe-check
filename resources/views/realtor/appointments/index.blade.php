@extends('realtor.main')
@section('title','Appointments')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Appointments
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('realtor.appointments.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Appointment</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Full Name</th>
                        <th> Address</th>
                        <th> Appointment Date</th>
                        <th> Appointment Time</th>
                        <th> Status</th>
                        <th> BG Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($appointments))
                        @foreach($appointments as $appointment)

                            @php
                                $start_time     =  date('h:i A', strtotime($appointment->start_time));
                                $end_time       =  date('h:i A', strtotime($appointment->end_time));
                                $start_date     =  date('l j F, Y', strtotime($appointment->start_date));

                            @endphp
                            <tr style="background:@if($appointment->appointment->bg_status == 'not-applied') yellow;  @elseif($appointment->appointment->bg_status == 'accepted') lightgreen; @elseif($appointment->appointment->bg_status == 'rejected') red; @endif color: @if($appointment->appointment->bg_status == 'rejected') #fff @else #000; @endif">

                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($appointment->appointment->cl_first_name)}} {{ucfirst($appointment->appointment->cl_last_name)}}</td>
                                <td>{{$appointment->appointment->property_address_one}} {{$appointment->appointment->property_address_two}}
                                    , {{ (!is_null($appointment->appointment->city)) ? $appointment->appointment->city->name :' ' }}
                                    , {{(!is_null($appointment->appointment->state)) ? $appointment->appointment->state->name : '' }}</td>
                                <td>{{$start_date}}</td>
                                <td>{{$start_time}} - {{$end_time}}</td>
                                <td>{{ucfirst($appointment->status)}}</td>
                                <td>
                                    @if($appointment->appointment->bg_status == 'not-applied')
                                        Background Check not performed
                                    @elseif($appointment->appointment->bg_status == 'accepted')
                                        Background Check is passed
                                    @elseif($appointment->appointment->bg_status == 'rejected')
                                        Background Check is Failed
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('realtor.appointments.edit', $appointment->id) }}">
                                        <button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Edit">Edit<i
                                                class="fa fa-pencil"></i></button>
                                    </a>
                                    @if($appointment->status =='upcoming')
                                        <a href="javascript:void(0)">
                                            <button class="btn btn-xs btn-primary cancelAppointment" style="margin-top: 3px;"
                                                    id="cancelAppointment" data-id="{{$appointment->id}}"
                                                    data-toggle="tooltip" title="Cancel">Cancel<i
                                                    class="fa fa-cancel"></i></button>
                                        </a>
                                    @endif
                                </td>



                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [5]}
            ],
        });
        $(document).on('click', '.cancelAppointment', function () {

            var appointment_schedule_id = $(this).data('id');
            var request = {"appointment_schedule_id": appointment_schedule_id};
            if (appointment_schedule_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('realtor.cancel.appointment.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status === "success") {
                            toastr[response.status](response.message);
                            window.location.href = '{{ route('realtor.appointments.index') }}'
                        }
                    },
                    error: function () {
                        toastr['error']('Something went wrong.');
                    }
                });
            } else {
                toastr['error']('Appointment Schedule not found.');

            }

        });


    </script>
@endpush
