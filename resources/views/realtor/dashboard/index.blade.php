@extends('realtor.main')
@section('title','Dashboard')
<style>
    .dashboard-stat.blue {
        background-color: #aa2e32 !important;
    }
</style>
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <a class="dashboard-stat dashboard-stat-v2 blue"
                           href="{{ route('realtor.appointments.index') }}">
                            <div class="visual">
                                <i class="fa fa-comments"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span data-counter="counterup"
                                          data-value="{{ $total_appointments }}">{{ $total_appointments }}</span>
                                </div>
                                <div class="desc">Total Appointments</div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                            <thead>
                            <tr>

                                <th> Sr No.</th>
                                <th> Full Name</th>
                                <th> Address</th>
                                <th> Appointment Date</th>
                                <th> Appointment Time</th>
                                <th> Status</th>

                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($appointments))
                                @foreach($appointments as $appointment)

                                    @php
                                        $start_time     =  date('h:i A', strtotime($appointment->start_time));
                                        $end_time       =  date('h:i A', strtotime($appointment->end_time));
                                        $start_date     =  date('l j F, Y', strtotime($appointment->start_date));

                                    @endphp
                                    <tr>

                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ucfirst($appointment->appointment->cl_first_name)}} {{ucfirst($appointment->appointment->cl_last_name)}}</td>
                                        <td>{{$appointment->appointment->property_address_one}} {{$appointment->appointment->property_address_two}}
                                            , {{ (!is_null($appointment->appointment->city)) ? $appointment->appointment->city->name :' ' }}
                                            , {{(!is_null($appointment->appointment->state)) ? $appointment->appointment->state->name : '' }}</td>
                                        <td>{{$start_date}}</td>
                                        <td>{{$start_time}} - {{$end_time}}</td>
                                        <td>{{ucfirst($appointment->status)}}</td>



                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
        });
    </script>
@endpush
