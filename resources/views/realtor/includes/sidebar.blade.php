<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark"
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            <li class="m-menu__item "
                aria-haspopup="true"><a href="{{ route('index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-home"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Home</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('realtor/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('realtor.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('realtor/profile') || request()->is('realtor/edit') || request()->is('realtor/edit/*') || request()->is('realtor/update/profile/*') || request()->is('realtor/update') || request()->is('realtor/update/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('realtor.profile.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-profile-1"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Profile</span>
											</span></span></a></li>


            <li class="m-menu__item {{ (request()->is('realtor/subscribe-history')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('realtor.subscribe.history.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-price-tag"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Subscription</span>
											</span></span></a></li>


            <li class="m-menu__item {{ (request()->is('realtor/appointments')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('realtor.appointments.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-price-tag"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Appointments</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('realtor/notifications')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('realtor.notifications.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-price-tag"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Notifications</span>
											</span></span></a></li>


        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
