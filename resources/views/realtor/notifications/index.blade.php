@extends('realtor.main')
@section('title','Notifications')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Notifications
                        </h3>
                    </div>
                </div>


            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Type.</th>
                        <th> Message</th>
                        <th> Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($notifications))
                        @foreach($notifications as $notification)

                            <tr>

                                <td>{{$loop->iteration}}</td>
                                <td>{{ $notification->type == 'realtor' ? 'User Register' : ucfirst($notification->type) }}</td>
                                <td>{{ ucfirst($notification->message) }}</td>
                                <td>{{ date('d-m-Y',strtotime($notification->updated_at)) }}</td>

                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "ordering": false,
            "order": [[0, "desc"]],
        });


    </script>
@endpush
