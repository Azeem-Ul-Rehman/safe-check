@extends('realtor.main')
@section('title','Profile')
@push('css')
    <style>
        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: #fcefe1;
            opacity: 1;
        }
    </style>
@endpush
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="">
                <div class="">
                    <div class="m-portlet">
                        <div class="container">
                            <div class="m-portlet__body">
                                <div class="serviceBoxMain">
                                    <div class="serverInnerDetails">
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8 col-xs-12 bannerFields">
                                                <div class="profileSettings">
                                                    <h4>Edit Profile</h4>
                                                    <form action="{{ route('realtor.update.user.profile') }}"
                                                          method="post"
                                                          enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="first_name">First Name</label>
                                                                    <input id="first_name" type="text"
                                                                           class="form-control @error('first_name') is-invalid @enderror"
                                                                           name="first_name"
                                                                           value="{{ old('first_name',$user->first_name) }}"
                                                                           autocomplete="first_name" autofocus
                                                                           placeholder="First Name*">

                                                                    @error('first_name')
                                                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="last_name">Last Name</label>
                                                                    <input id="last_name" type="text"
                                                                           class="form-control @error('last_name') is-invalid @enderror"
                                                                           name="last_name"
                                                                           value="{{ old('last_name',$user->last_name) }}"
                                                                           autocomplete="last_name" autofocus
                                                                           placeholder="Last Name*">

                                                                    @error('last_name')
                                                                    <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="mls_id">MLS ID</label>
                                                                    <input type="text" id="mls_id" name="mls_id"
                                                                           class="form-control @error('mls_id') is-invalid @enderror"
                                                                           value="{{ $user->realtor->mls_id ?? '' }}">
                                                                    @error('mls_id')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="website">Website</label>
                                                                    <input type="text"
                                                                           class="form-control @error('website') is-invalid @enderror"
                                                                           name="website" id="website"
                                                                           value="{{ $user->realtor->website ?? '' }}">
                                                                    @error('website')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="username">Username</label>
                                                                    <input id="username" type="text"
                                                                           class="form-control @error('username') is-invalid @enderror"
                                                                           name="username"
                                                                           value="{{ old('username',$user->username) }}" autocomplete="username" placeholder="Username*">

                                                                    @error('username')
                                                                    <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="email">Email</label>
                                                                    <input type="email"
                                                                           class="form-control @error('email') is-invalid @enderror"
                                                                           id="email"
                                                                           name="email" value="{{ $user->email }}">
                                                                    @error('email')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="phone_number">Mobile Number</label>
                                                                    <input id="phone_number" type="text"
                                                                           class="form-control @error('phone_number') is-invalid @enderror"
                                                                           name="phone_number"
                                                                           value="{{ $user->phone_number }}"
                                                                           autocomplete="phone_number" autofocus
                                                                           placeholder="Enter Mobile Number"
                                                                           onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                                                    @error('phone_number')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="home_number">Home Number</label>
                                                                    <input id="home_number" type="text"
                                                                           class="form-control @error('home_number') is-invalid @enderror"
                                                                           name="home_number"
                                                                           value="{{ $user->home_number }}"
                                                                           autocomplete="home_number" autofocus
                                                                           placeholder="Enter Home Number"
                                                                           onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                                                    @error('home_number')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="email">Pin Number</label>

                                                                    <input id="pin_number" type="text"
                                                                           class="form-control @error('pin_number') is-invalid @enderror"
                                                                           name="pin_number"
                                                                           value="{{ $user->realtor->pin_number ?? '' }}"
                                                                           maxlength="4"
                                                                           autocomplete="pin_number" autofocus
                                                                           placeholder="Enter Pin Number"
                                                                           onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')">
                                                                    @error('pin_number')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="address_one">Address One</label>
                                                                    <input type="text"
                                                                           class="form-control  @error('address_one') is-invalid @enderror"
                                                                           name="address_one" id="address_one"
                                                                           value="{{ $user->realtor->address_one ?? '' }}">
                                                                    @error('address_one')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="address_two">Address Two</label>
                                                                    <input type="text"
                                                                           class="form-control  @error('address_two') is-invalid @enderror"
                                                                           name="address_two" id="address_two"
                                                                           value="{{ $user->realtor->address_two ?? '' }}">
                                                                    @error('address_two')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label for="zip_code">Zip code</label>
                                                                    <input type="text" class="form-control"
                                                                           name="zip_code" id="zip_code"
                                                                           value="{{ $user->realtor->zip_code ?? '' }}">
                                                                    @error('zip_code')
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $message }}</strong>
                                                                    </span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--                                                    <div class="row">--}}
                                                        {{--                                                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
                                                        {{--                                                            <div class="form-group">--}}
                                                        {{--                                                                <label for="twitter_id">Twitter ID</label>--}}
                                                        {{--                                                                <input type="text" class="form-control"--}}
                                                        {{--                                                                       name="twitter_id" id="twitter_id"--}}
                                                        {{--                                                                       value="{{ $user->realtor->twitter_id ?? '' }}">--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}

                                                        {{--                                                    <div class="row">--}}
                                                        {{--                                                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
                                                        {{--                                                            <div class="form-group">--}}
                                                        {{--                                                                <label for="facebook_url">Facebook URL</label>--}}
                                                        {{--                                                                <input type="text" class="form-control"--}}
                                                        {{--                                                                       name="facebook_url" id="facebook_url"--}}
                                                        {{--                                                                       value="{{ $user->realtor->facebook_url ?? '' }}">--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}

                                                        {{--                                                    <div class="row">--}}
                                                        {{--                                                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
                                                        {{--                                                            <div class="form-group">--}}
                                                        {{--                                                                <label for="paypal_id">Payapl ID</label>--}}
                                                        {{--                                                                <input type="text" class="form-control"--}}
                                                        {{--                                                                       name="paypal_id" id="paypal_id"--}}
                                                        {{--                                                                       value="{{ $user->realtor->paypal_id ?? '' }}">--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}

                                                        {{--                                                    <div class="row">--}}
                                                        {{--                                                        <div class="col-md-12 col-sm-12 col-xs-12">--}}
                                                        {{--                                                            <div class="form-group">--}}
                                                        {{--                                                                <label for="instagram_id">Instagram ID</label>--}}
                                                        {{--                                                                <input type="text" class="form-control"--}}
                                                        {{--                                                                       name="instagram_id" id="instagram_id"--}}
                                                        {{--                                                                       value="{{ $user->realtor->instagram_id ?? '' }}">--}}
                                                        {{--                                                            </div>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                    </div>--}}

                                                        <div class="row">
                                                            <div
                                                                class="col-md-12 col-sm-12 col-xs-12 text-right moreBtns">
                                                                <a href="{{ route('realtor.profile.index') }}"
                                                                   class="btn buttonMain hvr-bounce-to-right">
                                                                    Cancel
                                                                </a>
                                                                <button type="submit"
                                                                        class="btn buttonMain hvr-bounce-to-right">Save
                                                                    Changes
                                                                </button>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')


@endpush
