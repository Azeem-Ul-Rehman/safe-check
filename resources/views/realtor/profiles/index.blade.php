@extends('realtor.main')
@section('title','Profile')
@push('css')
    <style>
        .profile-image {
            width: 85% !important;
            /*height: 120px;*/
            object-fit: cover;
        }
    </style>
@endpush
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="">
                <div class="">
                    <div class="m-portlet">
                        <div class="container">
                            <div class="m-portlet__body">
                                <div class="serviceBoxMain">
                                    <div class="serverInnerDetails">
                                        <div class="row ">
                                            <div class="col-md-8 col-sm-8 col-xs-12 bannerFields">                                              
                                                <div class="profileSettings">
                                                    <h4>Account Profile</h4>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <img class="profile-image"
                                                                     src="{{asset("/uploads/user_profiles/".$user->profile_pic)}}"
                                                                     alt="{{$user->profile_pic}}">
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <form method="post" enctype="multipart/form-data" id="submit-image"
                                                            action="javascript:void(0)">
                                                            <p>
                                                              @csrf
                                                              <input type="hidden" name="user_id" id="user_id"
                                                                     value="{{ $user->id}}">
      
                                                              <label for="image">
                                                                  <img src="{{ asset('frontend/images/upload.png') }}"
                                                                       style="width: 40px;cursor: pointer;"/>
                                                              </label>
      
                                                              <input type="file" name="image" id="image" style="display: none"
                                                                     onchange="readURL(this);"
                                                                     accept=".png, .jpg, .jpeg"/>
      
                                                                     Change Picture </p>
                                                      </form>
                                                      <p><a href="{{ route('realtor.profile.edit') }}" 
                                                        class=""><i class="fa fa-pencil-alt"></i> &nbsp; Edit Profile</a> </p>
                                                        <table width="100%">
                                                            <tr>
                                                                <td>Name: </td>
                                                                <td>  @if(!is_null($user->membershipDiscount))
                                                                    <span
                                                                        style="color: red">{{ ucfirst($user->membershipDiscount->name) }}</span>
    
                                                                @endif {{ $user->fullName() }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Email</td>
                                                                <td>{{ $user->email ?? '--' }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Phone Number</td>
                                                                <td>{{ $user->phone_number ?? '--' }}</td>
                                                            </tr>
                                                        </table>
                                                            
                                                          

                                                            <span></span>
                                                            <a href="{{ route('realtor.update.password') }}"
                                                            class="btn buttonMain hvr-bounce-to-right">Reset
                                                             Password</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                        </td>
                                                        <td width="30%">
                                                          
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <br>
                                                
                                                <table cellpadding="0" cellspacing="0" width="100%">
                                                    <tbody>

                                                    <tr>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        function readURL(input, id) {

            if (input.files && input.files[0]) {


                var formData = new FormData($('#submit-image')[0]);
                $.ajax({
                    type: "POST",
                    url: "{{ route('realtor.profile.edit.image') }}",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        toastr[response.status](response.message);
                        window.location.reload();
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });

                // reader.readAsDataURL(input.files[0]);
                // }
            }
        }
    </script>
@endpush
