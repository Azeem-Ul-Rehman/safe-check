@extends('realtor.main')
@section('title','Subscriptions')
@push('header')



    <!-- link to the SqPaymentForm library -->
    <script type="text/javascript" src="https://js.squareupsandbox.com/v2/paymentform"></script>

    <!-- link to the local SqPaymentForm initialization -->
    <script type="text/javascript" src="{{ asset('js/sqpaymentform.js') }}">
    </script>

    <!-- link to the custom styles for SqPaymentForm -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/sqpaymentform-basic.css') }}">
    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            if (SqPaymentForm.isSupportedBrowser()) {
                paymentForm.build();
                // paymentForm.recalculateSize();
            }
        });
    </script>
    <style>
        div.dataTables_wrapper div.dataTables_filter {

            display: none !important;
        }
    </style>

@endpush
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Subscriptions
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('realtor.subscribe.package') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Subscription</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="m-portlet__body">
                <div class="portlet light bordered">

                    <div class="portlet-body">
                        {{--                        <ul class="nav nav-pills">--}}
                        {{--                            <li class="active btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10" style="margin-right: 11px;">--}}
                        {{--                                <a href="#tab_2_1" data-toggle="tab" aria-expanded="true" class="text-white">--}}
                        {{--                                    Current Package </a>--}}
                        {{--                            </li>--}}
                        {{--                            <li class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10">--}}
                        {{--                                <a href="#tab_2_2" data-toggle="tab" aria-expanded="false" class="text-white">--}}
                        {{--                                    Subscription History </a>--}}
                        {{--                            </li>--}}

                        {{--                        </ul>--}}
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_2_1">
                                <table
                                    class="table table-striped- table-bordered table-hover table-checkable table-responsive"
                                    id="m_table_1">
                                    <thead>
                                    <tr>

                                        <th> Sr No</th>
                                        <th> Package Name</th>
                                        <th> Price</th>
                                        <th> Background Checks</th>
                                        <th> Addon Checks</th>
                                        <th> Total Checks</th>
                                        <th> Remaining Checks</th>
                                        <th> Start Date</th>
                                        <th> Expiry Date</th>
                                        <th> Current Package</th>
                                        <th> Payment Status</th>
                                        <th> Action</th>
                                        {{--                        <th> Actions</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($subscriptions))
                                        @foreach($subscriptions as $subscription)

                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$subscription->package->name}}</td>
                                                <td>${{$subscription->price}}</td>
                                                <td>{{$subscription->background_checks}}</td>
                                                <td>{{$subscription->addon_checks}}</td>
                                                <td>{{$subscription->background_checks +$subscription->addon_checks}}</td>
                                                <td>{{$subscription->remaining_checks }}</td>
                                                <td>{{ (!is_null($subscription->start_date)) ? date('d-m-Y' , strtotime($subscription->start_date)) : 'N/A'}}</td>
                                                <td>{{ (!is_null($subscription->expiry_date)) ? date('d-m-Y' , strtotime($subscription->expiry_date)) : 'N/A'}}</td>
                                                <td>
                                                    @if($subscription->package->id !=1 && strtolower($subscription->status) == 'paid' )
                                                        @if($subscription->expiry_date < date('Y-m-d'))
                                                            Expired
                                                        @else
                                                            Active
                                                        @endif
                                                    @else
                                                        N/A
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(strtolower($subscription->status) == 'unpaid')
                                                        <a href="{{route('payment')}}">Pay Now</a>
                                                    @else
                                                        {{ucwords($subscription->status)}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($subscription->package->id !=1 && strtolower($subscription->status) == 'paid' )
                                                        @php
                                                            if($subscription->expiry_date < date('Y-m-d')){
                                                                $disabled = 'disabled';
                                                            }else{
                                                                $disabled = '';
                                                            }
                                                        @endphp
                                                        <button type="button"
                                                                class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                                                                data-toggle="modal"
                                                                data-target="#addMoreChecks"
                                                                {{ $disabled }}
                                                                onclick="addMoreChecks('{{$subscription->id}}','{{$subscription->package->addon_price}}')">
                                                            Add More Checks
                                                        </button>
                                                    @endif

                                                </td>


                                                {{--                                <td nowrap>--}}
                                                {{--                                    <a href="{{route('admin.subscriptions.edit',$subscription->id)}}"--}}
                                                {{--                                       class="btn btn-sm btn-info pull-left ">Edit</a>--}}

                                                {{--                                </td>--}}
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            {{--                            <div class="tab-pane" id="tab_2_2">--}}
                            {{--                                <table class="table table-striped- table-bordered table-hover table-checkable"--}}
                            {{--                                       id="m_table_2">--}}
                            {{--                                    <thead>--}}
                            {{--                                    <tr>--}}

                            {{--                                        <th> Sr NO.</th>--}}
                            {{--                                        <th> Realtor Name</th>--}}
                            {{--                                        <th> Package Name</th>--}}
                            {{--                                        <th> Price</th>--}}
                            {{--                                        <th> Background Checks</th>--}}
                            {{--                                        <th> Payment Status</th>--}}
                            {{--                                        --}}{{--                        <th> Actions</th>--}}
                            {{--                                    </tr>--}}
                            {{--                                    </thead>--}}
                            {{--                                    <tbody>--}}
                            {{--                                    @if(!empty($subscriptionHistories))--}}
                            {{--                                        @foreach($subscriptionHistories as $subscription)--}}
                            {{--                                            <tr>--}}
                            {{--                                                <td>{{$loop->iteration}}</td>--}}
                            {{--                                                <td>{{$subscription->realtor->user->fullName()}}</td>--}}
                            {{--                                                <td>{{$subscription->package->name}}</td>--}}
                            {{--                                                <td>{{$subscription->price}}</td>--}}
                            {{--                                                <td>{{$subscription->background_checks}}</td>--}}
                            {{--                                                <td>{{ucwords($subscription->status)}}</td>--}}
                            {{--                                                --}}{{--                                <td nowrap>--}}
                            {{--                                                --}}{{--                                    <a href="{{route('admin.subscriptions.edit',$subscription->id)}}"--}}
                            {{--                                                --}}{{--                                       class="btn btn-sm btn-info pull-left ">Edit</a>--}}

                            {{--                                                --}}{{--                                </td>--}}
                            {{--                                            </tr>--}}
                            {{--                                        @endforeach--}}
                            {{--                                    @endif--}}
                            {{--                                    </tbody>--}}
                            {{--                                </table>--}}
                            {{--                            </div>--}}
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="addMoreChecks" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">ADD More Checks</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-12 col-xs-12 col-sm-12">

                            <div class="form-group">
                                <form id="nonce-form" novalidate
                                      action="{{ route('customer.charge') }}" method="post">
                                    @csrf
                                    <input type="hidden" name="subscription_id" id="subscription_id" value="">
                                    <div class="col-md-12 col-md-offset-2 col-sm-7 col-xs-12">


                                        <div class="paymentForm">
                                            <div id="form-container">
                                                <label for="add_checks">Add Checks $<span
                                                        id="perCheck"></span>/Check</label>
                                                <br>
                                                <input name="add_checks" id="add_checks" value="" type="number" min="1"
                                                       required>
                                                <h3 style="margin-top: 10px"><span>En</span>ter Payment details</h3>
                                                <div id="sq-ccbox">
                                                    <!--
                                                      Be sure to replace the action attribute of the form with the path of
                                                      the Transaction API charge endpoint URL you want to POST the nonce to
                                                      (for example, "/process-card")
                                                    -->

                                                    <!--
                             After a nonce is generated it will be assigned to this hidden input field.
                         -->
                                                    <input type="hidden" id="amount" name="amount" value="">
                                                    <input type="hidden" id="type" name="type" value="add_more_checks">
                                                    <input type="hidden" id="card-nonce" name="nonce">
                                                    <fieldset>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group">
                                                                    <span class="label">Card Number</span>
                                                                    <div id="sq-card-number"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <span class="label">Expiration</span>
                                                                    <div id="sq-expiration-date"></div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <span class="label">CVV</span>
                                                                    <div id="sq-cvv"></div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">

                                                                    <span class="label">Postal</span>
                                                                    <div id="sq-postal-code"></div>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </fieldset>

                                                    <button id="sq-creditcard" class="btn btnMain"
                                                            onclick="requestCardNonce(event)">
                                                        Pay
                                                        $<span id="buttonPrice"></span>
                                                    </button>

                                                    <div id="error"></div>

                                                </div> <!-- end #sq-ccbox -->

                                            </div> <!-- end #form-container -->
                                        </div>
                                    </div>
                                </form>
                            </div>


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "columnDefs": [
                // {orderable: false, targets: [2]}
            ],
        });
        $("#m_table_2").dataTable({
            "columnDefs": [
                // {orderable: false, targets: [2]}
            ],
        });

        function addMoreChecks(id, price) {

            $('#subscription_id').val(id);
            $('#perCheck').text(price);

        }

        $('#add_checks').on('change', function () {

            var addChecks = parseInt($(this).val());
            var pricePerCheck = parseFloat($('#perCheck').text());
            var totalPrice = addChecks * pricePerCheck;
            $('#amount').val(totalPrice);
            $('#buttonPrice').text(totalPrice);
        });
    </script>
@endpush

@push('css')
    <style>
        .packagePayment input {
            border-radius: 50px;
            padding: 10px 20px;
            height: auto;
        }

        .paymentForm button {
            margin: 0 !important;
        }

        .paymentForm #form-container .sq-input {
            border-radius: 50px;
            padding: 0 20px;
            height: 40px;
        }

        #form-container {
            width: 100% !important;
        }

    </style>
@endpush
