<!DOCTYPE html>
<html lang="en">
<head>

    @php
        $meta_tags =isset($meta_information) ? $meta_information->where('route',Route::currentRouteName())->first() : null ;
    @endphp
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">--}}
    <title>{{ $meta_tags->title ?? 'SAFECHEX' }}</title>
    <meta name="description" content="{{ $meta_tags->description ?? 'SAFECHEX' }}">
    <meta name="keywords" content="{{ $meta_tags->keywords ?? 'SAFECHEX' }}">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('frontend/images/favicons/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('frontend/images/favicons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('frontend/images/favicons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('frontend/images/favicons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('frontend/images/favicons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('frontend/images/favicons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('frontend/images/favicons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('frontend/images/favicons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('frontend/images/favicons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"
          href="{{asset('frontend/images/favicons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('frontend/images/favicons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('frontend/images/favicons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('frontend/images/favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{asset('frontend/images/favicons/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('frontend/images/favicons/ms-icon-144x144.png') }}">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap -->
    <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
    <link
        href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500&family=Raleway:wght@400;500;700&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">
    @stack('css')
</head>
<body>

@include('frontend.include.header')
<div class="mainContent">
    @include('frontend.subpages.banner')
    @include('frontend.subpages.about')
    @include('frontend.subpages.how_it_works')
    @include('frontend.subpages.counter')
    @include('frontend.subpages.contact-us')
    @include('frontend.include.footer')
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #911014;color:#fff;height: 45px">
                    <h5 class="modal-title" id="exampleModalLabel">Acknowledgement </h5>
                </div>
                <div class="modal-body" style="padding: 35px;">
                    &nbsp; I agree to the <a href="{{ route('privacy.policy') }}" target="_blank">Terms & Conditions and
                        Privacy Policy</a>
                </div>
                <div class="modal-footer">
                    <button type="button" class="signupBtn" onclick="acceptTermsAndCondition()">Accept</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background: #911014;color:#fff;height: 45px">
                    <h5 class="modal-title" id="exampleModalLabel">Watch Now </h5>
                </div>
                <div class="modal-body">
                    <div class="video-section">
                        <video width="100%" height="320" autoplay muted controls id="videoPlay">
                            <source src="{{ asset('videos/videoold.mp4') }}" type="video/mp4">
                            <source src="{{ asset('videos/videoold.mp4') }}" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="signupBtn" onclick="closeModal()">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>

<script src="{{ asset('frontend/js/custom.js') }}"></script>
@stack('js')

<script>
    @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

    function acceptTermsAndCondition() {
        localStorage.setItem("terms", true);
        $('#myModal').modal('toggle');
        $('#videoModal').modal({backdrop: 'static', keyboard: false});
        $('#videoPlay').get(0).play();
    }

    function watchNow() {
        $('#videoModal').modal({backdrop: 'static', keyboard: false});
        $('#videoPlay').get(0).play();
    }

    function openModal() {
        // now works as you would expect
        $('#myModal').modal({backdrop: 'static', keyboard: false});
    }

    function closeModal() {
        $('#videoModal').modal('toggle');
        $('#videoPlay').get(0).pause();
        $('#videoPlay').get(0).currentTime = 0
    }

    $(document).ready(function () {
        if (localStorage.getItem("terms") === null) {
            openModal();
        } else {

            $('#videoModal').modal({backdrop: 'static', keyboard: false});
            $('#videoPlay').get(0).play();
            setTimeout(function () {
                $('#videoPlay').get(0).unmute;
            }, 2000);
        }
    });
</script>
<style>
    @media (min-width: 768px) {
        #videoModal .modal-dialog {
            width: 710px;
            margin: 30px auto;
        }
    }
</style>
<script type="text/javascript">
    if (window.location.protocol == "http:") {
        var restOfUrl = window.location.href.substr(5);
        window.location = "https:" + restOfUrl;
    }
</script>
<script>
</script>

</body>
</html>
