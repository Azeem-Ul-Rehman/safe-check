<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});


Route::middleware(['auth'])->group(function () {
    Route::prefix('realtor')->name('realtor.')->group(function () {
        Route::resource('/appointments', 'Realtor\AppointmentController');
        Route::post('appointment-time-increment', 'Realtor\AppointmentController@timeIncreament');
        Route::post('appointment-checkin', 'Realtor\AppointmentController@checkIn');
        Route::post('appointment-checkout', 'Realtor\AppointmentController@checkOut');
        Route::post('notifications ', 'Realtor\AppointmentController@notifications');
    });
});

//Route::resource('cal', 'gCalendarController');
//Route::get('oauth', 'gCalendarController@oauth')->name('oauthCallback');


Route::resource('/appointments', 'Realtor\AppointmentController');
Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'Realtor\AppointmentController@oauth']);

Route::post('otp', 'Api\AuthController@otp');
Route::post('verify/otp', 'Api\AuthController@verifyOTP');
Route::post('forgot-password-request', 'Api\AuthController@forgotPasswordRequest');
Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

Route::get('states', 'Api\GeneralController@states');
Route::get('packages', 'Api\GeneralController@packages');
Route::get('cities/{state_id}', 'Api\GeneralController@cities');
Route::get('array-to-json', 'Api\GeneralController@arratTojson');

Route::get('settings', 'Api\GeneralController@settings');
Route::get('genders', 'Api\GeneralController@genders');

//Route::resource('gcalendar', 'gCalendarController');
//Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'gCalendarController@oauth']);


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('update-profile', 'Api\GeneralController@updateProfile');
    Route::post('update-password', 'Api\GeneralController@updatePassword');
    Route::post('update-profile-image', 'Api\GeneralController@updateProfileImage');


    Route::post('logout', 'Api\AuthController@logout');
    Route::get('user', 'Api\AuthController@getUser');

    Route::post('get-lat-lng', 'Api\GeneralController@getLatLong');
    Route::post('store-lat-lng', 'Api\GeneralController@saveLatLong');

    Route::prefix('realtor')->group(function () {

        Route::post('create-appointment', 'Api\RealtorController@createAppointment');
        Route::get('get-appointments', 'Api\RealtorController@getAppointments');
        Route::get('get-appointments-history', 'Api\RealtorController@appointmentHistory');
        Route::get('get-appointments-detail/{appointment_schedule_id}', 'Api\RealtorController@getAppointmentDetail');
        Route::post('update-appointments', 'Api\RealtorController@updateAppointment');
        Route::post('panic', 'Api\RealtorController@panicButton');
        Route::post('background-check', 'Api\RealtorController@backgroundCheck');
        Route::get('notifications ', 'Api\RealtorController@notifications');
        Route::get('delete/notification/{id}', 'Api\RealtorController@deleteNotification');
        Route::get('cancel/appointment/{appointment_schedule_id}', 'Api\RealtorController@cancelAppointment');
        Route::post('package/subscribe','Api\RealtorController@packageSubscribe');

    });

    Route::prefix('appointment')->group(function () {

        Route::post('appointment-time-increment', 'Api\RealtorController@timeIncreament');
        Route::post('appointment-checkin', 'Api\RealtorController@checkIn');
        Route::post('appointment-checkout', 'Api\RealtorController@checkOut');


    });

});

Route::fallback(function () {
    return response()->json(['message' => 'URL Not Found'], 404);
});
