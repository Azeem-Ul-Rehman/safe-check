<?php
if (env('APP_ENV') === 'local') {
    URL::forceScheme('https');
}
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Spatie\GoogleCalendar\Event;

//Route::get('/', function () {
//    $event = new \Spatie\GoogleCalendar\Event();
//    $event->name = "test App event";
//    $event->startDateTime = Carbon\Carbon::now();
//    $event->endDateTime = Carbon\Carbon::now()->addHour();
//    $event->addAttendee([
//        'email' => 'azeemulrehman71@gmail.com',
//        'name' => 'John Doe',
//        'comment' => 'Lorum ipsum',
//    ]);
//    $event->save();
//    $e = Event::get();
//    dd($e);
//});

Route::get('/', function () {
    return view('welcome');
})->name('index');
//
//Route::resource('cal', 'gCalendarController');
//Route::get('oauth', 'Realtor\AppointmentController@oauth')->name('oauthCallback');


//Route::resource('cal', 'gCalendarController');
//Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'gCalendarController@oauth']);

Auth::routes(['verify' => true]);
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
//    \Illuminate\Support\Facades\Artisan::call('storage:link');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');

//    \Illuminate\Support\Facades\Artisan::call('passport:install');
//    \Illuminate\Support\Facades\Artisan::call('migrate');

    return 'Commands run successfully Cleared.';
});


// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/register/{referral_code?}', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');


//Forget Password
Route::get('/user/verify/{token}', 'Auth\LoginController@activation');


Route::post('otp', 'Auth\ForgotPasswordController@otp')->name('otp.send');
Route::post('verify/otp', 'Auth\ForgotPasswordController@verifyOTP')->name('verify.otp');
Route::post('forgot-password-request', 'Auth\ForgotPasswordController@ForgotPasswordRequest')->name('otp.password.reset');

Route::get('/home', 'HomeController@index')->name('home');

//Landing Page Routes

Route::get('about-us', 'Frontend\AboutUsController@index')->name('aboutus.detail');
Route::get('how-it-works', 'Frontend\AboutUsController@howItWorks')->name('how.its.works.detail');

Route::post('customer/package/subscribe', 'ChargeController@subscribe')->name('package.subscribe');
Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/dashboard', 'Backend\DashboardController');
        Route::resource('/meta-tags', 'Backend\MetaTagController', [
            'only' => ['index', 'create', 'store', 'edit', 'update']
        ]);
        Route::resource('/cities', 'Backend\CityController');
        Route::resource('/states', 'Backend\StateController');
        Route::resource('/packages', 'Backend\PackageController');
        Route::resource('/subscriptions', 'Backend\SubscriptionController');

        Route::post('/states/import/file', 'Backend\StateController@importFile')->name('states.import.excel');
        Route::resource('/users', 'Backend\UserController');
        Route::post('/users/location', 'Backend\UserController@updateLocation')->name('user.location');


        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'edit', 'update']
        ]);
        Route::resource('/contacts', 'Backend\ContactUsController', [
            'only' => ['index', 'destroy']
        ]);
        Route::resource('/aboutus', 'Backend\AboutUsController', [
            'only' => ['index', 'store']
        ]);

        Route::resource('/privacypolicy', 'Backend\PrivacyController', [
            'only' => ['index', 'store']
        ]);
        Route::resource('/terms-and-conditions', 'Backend\TermsAndConditionsController', [
            'only' => ['index', 'store']
        ]);

        //User Profiles
        Route::get('profile', 'Backend\ProfileController@index')->name('profile.index');

        Route::get('edit/profile', 'Backend\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Backend\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Backend\ProfileController@updateProfile')->name('update.user.profile');

        Route::get('update/password/', 'Backend\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Backend\ProfileController@updatePassword')->name('update.user.password');

        Route::get('update/phone', 'Backend\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Backend\ProfileController@updateMobileNumber')->name('update.user.phone');


    });

    Route::get('/sendsms/{phone}/{message}', 'Backend\SendReferralCodeController@sendCodeWithPhone');


    //Customer Routes
    Route::prefix('realtor')->name('realtor.')->group(function () {
        Route::get('/dashboard', 'Realtor\MainController@index')->name('dashboard.index');
        //User Profiles
        Route::get('profile', 'Realtor\ProfileController@index')->name('profile.index');
        Route::get('subscribe/package', 'Realtor\MainController@subcribePackage')->name('subscribe.package');
        Route::get('subscribe-history', 'Realtor\MainController@subscribeHistory')->name('subscribe.history.index');

        Route::get('notifications', 'Realtor\MainController@notifications')->name('notifications.index');

        Route::get('edit/profile', 'Realtor\ProfileController@editProfile')->name('profile.edit');
        Route::post('edit/profile/image', 'Realtor\ProfileController@editProfileImage')->name('profile.edit.image');
        Route::post('update/profile', 'Realtor\ProfileController@updateProfile')->name('update.user.profile');

        Route::get('update/password/', 'Realtor\ProfileController@changePassword')->name('update.password');
        Route::post('update/user/password/', 'Realtor\ProfileController@updatePassword')->name('update.user.password');

        Route::get('update/phone', 'Realtor\ProfileController@changeMobileNumber')->name('update.phone');
        Route::post('update/phone', 'Realtor\ProfileController@updateMobileNumber')->name('update.user.phone');


        Route::resource('/appointments', 'Realtor\AppointmentController');

        Route::get('oauth', ['as' => 'oauthCallback', 'uses' => 'Realtor\AppointmentController@oauth']);
        Route::post('appointment-time-increment', 'Realtor\AppointmentController@timeIncreament');
        Route::post('appointment-checkin', 'Realtor\AppointmentController@checkIn');
        Route::post('appointment-checkout', 'Realtor\AppointmentController@checkOut');
        Route::get('appointment-calender-google', 'Realtor\AppointmentController@googleCalenderAppointment');


        Route::get('cancel/appointment', 'Realtor\AppointmentController@cancelAppointment')->name('cancel.appointment.status');


    });

    Route::get('payment', 'ChargeController@finalPayment')->name('payment');


});

Route::get('code/{code}', 'Realtor\BackgroundCheckController@getCode');
Route::get('realtor/appointment/background/check/{appointment_id}/{user_id}', 'Realtor\BackgroundCheckController@appointmentCheck');
Route::post('realtor/appointment/background/store/{appointment_id}', 'Realtor\BackgroundCheckController@storeBackgroundCheck')->name('realtor.appointments.store.backgroundCheck');


Route::get('privacy-policy', function () {
    $privacyPolicy = \App\Models\PrivacyPolicy::first();
    return view('frontend.pages.privacy-policy', compact('privacyPolicy'));
})->name('privacy.policy');

Route::get('terms-and-conditions', function () {
    $terms = \App\Models\TermsAndCondition::first();
    return view('frontend.pages.terms-and-conditions', compact('terms'));
})->name('terms-and-conditions');


//Ajax Routes
Route::prefix('ajax')->name('ajax.')->group(function () {

    Route::post('update/addon/checks', 'Backend\AjaxController@updateAddOnChecks')->name('update.addon.checks');
    Route::get('state-cities', 'Backend\AjaxController@stateCities')->name('stateCities');
    Route::get('backgroundCheck', 'Backend\AjaxController@xmlRequestBackgroundCheck')->name('backgroundCheck');

});


Route::resource('/contacts', 'Backend\ContactUsController', [
    'only' => ['create', 'store']
]);


Route::get('/contacts', 'Backend\ContactUsController@create')->name('contacts.create');
Route::post('/contacts/store', 'Backend\ContactUsController@store')->name('contacts.store');


//Basic Charge
Route::post('/charge', 'ChargeController@charge')->name('charge');
Route::get('/send/sms/user', 'SMSController@sendSms')->name('send.sms.user');


Route::post('/customer/charge', 'ChargeController@chargeWithMerchantAndCustomer')->name('customer.charge');

Route::group(['prefix' => 'merchant'], function () {

    //Charge with merchant
    Route::get('/{merchant}/charge', 'ChargeController@chargeWithMerchant');

    //Charge with merchant and customer
    Route::get('/{merchant}/customer/{customer}/charge', 'ChargeController@chargeWithMerchantAndCustomer');
});


Route::group(['prefix' => 'customer'], function () {

    //create customer
    Route::get('/create', 'ChargeController@createCustomer');

    //Charge with customer
    Route::get('/{customer}/charge', 'ChargeController@chargeWithCustomer');
});




